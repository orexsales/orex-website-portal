@extends('auth.layouts.app')
@section('title',trans('app.login'))
@section('content')
    <!-- Login & Signup -->
    <div class="container mt--8 pb-5">
        <div class="row justify-content-center">
            <div class="col-lg-5 col-md-7">
                <div class="card bg-secondary shadow border-0">
{{--                    <div class="card-header bg-transparent pb-5">--}}
{{--                        <div class="text-muted text-center mt-2 mb-3"><small>{{trans('app.sign_in_with')}}</small></div>--}}
{{--                        <div class="btn-wrapper text-center">--}}
{{--                            <a href="{{route('oAuth',['facebook'])}}" class="btn btn-neutral btn-icon">--}}
{{--                                <span class="btn-inner--icon"><i class="fa fa-facebook-f"></i></span>--}}
{{--                                <span class="btn-inner--text">Facebook</span>--}}
{{--                            </a>--}}
{{--                            <a href="{{route('oAuth',['google'])}}" class="btn btn-neutral btn-icon">--}}
{{--                                <span class="btn-inner--icon"><i class="fa fa-google-plus"></i></span>--}}
{{--                                <span class="btn-inner--text">Google</span>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <div class="card-body px-lg-5 py-lg-5">
{{--                        <div class="text-center text-muted mb-4">--}}
{{--                            <small>{{trans('app.or_sign_in_with_credentials')}}</small>--}}
{{--                        </div>--}}
                        @if($errors)
                            <ul>

                                @foreach($errors->all() as $error)
                                    <span class="help-block text-danger">
                                            <strong>{{ $error }}</strong>
                                        </span>
                                @endforeach
                            </ul>
                        @endif
                        <form role="form" action="{{route('login')}}" method="post">
                            {{csrf_field()}}
                            <div class="form-group mb-3">
                                <div class="input-group input-group-alternative">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                    </div>
                                    <input class="form-control" placeholder="{{trans('app.email')}}" name="email" type="email">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group input-group-alternative">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                    </div>
                                    <input class="form-control" placeholder="{{trans('app.password')}}" name="password" type="password">
                                </div>
                            </div>
                            <div class="custom-control custom-control-alternative custom-checkbox">
                                <input class="custom-control-input" name="remember" id="customCheckLogin" type="checkbox">
                                <label class="custom-control-label" for="customCheckLogin">
                                    <span class="text-muted">{{trans('app.remember_me')}}</span>
                                </label>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary my-4">{{trans('app.login')}}</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- End Login & Signup -->

@endsection
