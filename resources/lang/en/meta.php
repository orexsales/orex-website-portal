<?php

return [

    'home_title' =>'Buy Solar Panels | Solar Power Products online in UAE - OREX',
    'home_description' =>'Let solar products powered your life. A one-stop shop to buy solar panel products including Solar UPS, Solar Inverters, Solar Batteries,  Solar Controllers etc.',

    'calculation_tittle'=>'Solar Panel Calculator to Esitmate Total Savings',
    'calculation_description'=>'Estimate total savings with this solar panel calculator, here you can analyze how many panels, inverters, and batteries required on based on roof size, electric bill & other factors.',

    'about_title'=>'About Orex - A Wholesale Supplier of Solar Products',
    'about_description'=>'We at OREX supply unique and reliable, high quality solar energy products and services which include all of the stages involved in manufacturing, quality assurance and supplying. OREX was formed in 2011 identifying your needs and provide you with efficient low cost solar energy systems. We are confident that by using our solar energy systems you will begin saving money, time and energy from the beginning.',

    'blog_title'=>'Solar Panel Calculator to Esitmate Total Savings',
    'blog_description'=>'Solar Panel Calculator to Esitmate Total Savings',

    'solar-panels-title'=>'Wholesale Supplier of Solar Panel Products  | Solar Energy',
    'solar-panels-descp'=>'Check the inventory of solar panel products, we\'ve variety of solar products from Monocrystalline to Polycrysralline. Distributors can get special discounts.',

    'inverters-title'=>'Solar Power Inverters Collection | PV Powered Inverter',
    'inverters-descp'=>'Find the best collection of solar power or photovolatic inverters at Orex, a wholesaler of UAE offering best deals to every distributors. Request a quote today!',

    'led-bulb-title'=>'Solar LED Bulbs Wholesale Supplier | LED Bulb',
    'led-bulb-descp'=>'Best collection of solar LED bulb avail for distributors at the best price from Orex, a wholesale supplier of UAE. See solar LED bulbs today!',

    'batteries-title'=>'Solar Batteries Wholesale Supplier | Best Deal Avail on Solar Battery',
    'batteries-descp'=>'Being a wholesale supplier of Solar Batteries, distributors are always getting best deals at Orex. Contact us to make a trade enquiry of solar battery today.',

    'charge-controllers-title'=>'Buy Solar Charge Controller from a Wholesaler | Solar Controller',
    'charge-controllers-descp'=>'As a wholesale supplier, distributors will always get the best deal on solar charge controllers at Orex. Your dream solar charge controller is just a click away!',



    'solar-panel-poly-title'=>'Solar Panel Poly Supplier | Polycrystalline Solar Panels',
    'solar-panel-poly-descp'=>'Find an outstanding collection of poly solar panel from Orex in best prices. Here polycrystalline solar panels are available from 5w to 300 watts range. Visit today!',


    'solar-panel-mono-title'=>'Solar Panel Mono Supplier | Monocrystalline Solar Panels',
    'solar-panel-mono-descp'=>'Buy long-lasting mono solar panels from UAE\'s best supplier & save. Monocrystalline Solar Panels are avail in different sizes from 100w to 300w.',

    'red-inverter-title'=>'Buy Red Inverter Online at Best Prices | Red Inverter Battery',
    'red-inverter-descp'=>'Orex offers best red inverters which is undoubtedly a leading player in power backup. Here red inverter batteries are available from 300w to 3000W.',

    'solar-water-pump-inverter-title'=>'Solar Water Pump Inverter for Solar Pump Solution',
    'solar-water-pump-inverter-descp'=>'Buy high-quality solar water pump inverter at competitive prices at Orex. 3 phase solar water pump inverters are available in different KW from 5.5 to 22.',

    'home-ups-title'=>'Home UPS | Power Inverters Online - OREX',
    'home-ups-descp'=>'Protect against power interruptions with home UPS. Many benefits come along with this helpful piece of home UPS power inverters. Click here for more info!',

    'solar-home-ups-title'=>'Buy Solar Home UPS online at Best Price in UAE | Solar UPS',
    'solar-home-ups-descp'=>'Find the best collection of solar home UPS at Orex & get great deals on every purchase. Click here to check the collection of solar power UPS for home today.',


    'agm-batteries-title'=>'Choose AGM Batteries for Higher Performance | Wholesale Shop',
    'agm-batteries-descp'=>'AGM Battery, known as maintenance-free battery designed for a solar electric system from UAE\'s wholesaler at best price. Read more about AGM batteries today!',

    'solar-kit-title'=>'Solar Kit for Home Needs | Wholesale Supplier',
    'solar-kit-descp'=>'Being a wholesale supplier of Solar kit, distributors are always getting best deals at Orex. Contact us to make a trade enquiry of solar kits.',

    'led-bulb-12v-title'=>'Stunning LED Bulb 12V at Best Price | Led Solar Bulb',
    'led-bulb-12v-descp'=>'Save more with LED bulb at Orex. Here distributors always get the best deals. Write us today for trade enquiry of LED solar bulb which are available 3w, 5w, 7w.',

    'hybrid-inverter-with-mppt-title'=>'Wholesale Supplier of Hybrid Inverter With MPPT | Solar Unit',
    'hybrid-inverter-with-mppt-descp'=>'Outstanding collection of Hybrid inverter with Mppt for sale. Distributors can increase their trading profit with Orex. Get a free quote today!',

    'led-bulb-220v-title'=>'Wholesale Supplier of LED Bulbs 220v | Buy Solar Bulb online',
    'led-bulb-220v-descp'=>'Find an outstanding collection of LED 220v bulbs from a wholesale supplier of UAE. Exclusive deals avail for distributors; contact for trade enquiry of LED bulb',


    'tall-tubular-batteries-title'=>'Outstanding Tall Tubular Batteries - Orex, a wholesale shop',
    'tall-tubular-batteries-descp'=>'Orex Tubular Batteries are helping users to stay up for a long time. Being a wholesale supplier, distributors can get amazing deals for trade. Request a quote!',

    'high-capacity-inverter-title'=>'Best High Capacity Inverters & Batteries Collection at Orex',
    'high-capacity-inverter-descp'=>'Orex offers high capacity inverters to dealers at the best prices. If you\'re a distributor & have any query regarding high capacity inverters, then click here.',


    'project-accessories-spare-parts-title'=>'Wholesale Supplier of Solar Project Accessories & Spare Parts',
    'project-accessories-spare-parts-descp'=>'Orex, a known wholesale supplier of UAE offering best deals to distributors for all the solar projects including accessories and spare parts. ',


    'pwm-inverter-title'=>'PWM Inverter Wholesale Supplier | Best Deal Avail on Inverter',
    'pwm-inverter-descp'=>'As a wholesale supplier, distributors will always get the best deal on PWM inverters at Orex. Your dream Pulse Width Modulation inverter is just a click away!',

    'dc-fan-title'=>'Best DC Ceiling Fans | DC Stand Fan - Orex',
    'dc-fan-descp'=>'Orex offers DC fan configured to work with a solar panel. Being a wholesale supplier, distributors can get amazing deals for trade. Request a quote!',

    'solar-kit-title'=>'Solar Kit for Home Needs | Wholesale Supplier',
    'solar-kit-descp'=>'Being a wholesale supplier of Solar kit, distributors are always getting best deals at Orex. Contact us to make a trade enquiry of solar kits.',


    'solar-water-heater-title'=>'Wholesale Supplier of Solar Water Heater  | Solar Heaters',
    'solar-water-heater-descp'=>'As a wholesale supplier, distributors will always get the best deals on Solar water heaters at OREX. Click here to get more details about Solar Heaters today!',

];
