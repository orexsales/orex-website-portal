<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use App\Models\Blog;
use App\Models\Category;
use App\Models\Product;
use Nexmo\Client;
use Nexmo\Client\Credentials\Basic;

Route::group(['middleware' => 'web', 'prefix' => LaravelLocalization::setLocale()], function () {
    /*Auth*/
    Auth::routes();
    
    Route::get('sign-in', 'Auth\LoginController@showLoginForm')->name('sign_in');
    Route::get('/', function (){
        return view('auth.login');
    })->middleware('guest');
});

Route::get('auth/{social}', 'Auth\OAuthController@redirectToProvider')->name('oAuth');
Route::get('auth/{social}/callback', 'Auth\OAuthController@handleProviderCallback');

