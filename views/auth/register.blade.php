@extends('layouts.app')
@section('title',trans('app.login'))
@section('content')
    <!-- Login & Signup -->
    <section class="u-bg-overlay g-bg-pos-top-center g-bg-img-hero g-bg-black-opacity-0_3--after g-py-100"
             style="background-image: url(../../../images/slide.jpeg);">
        <div class="container u-bg-overlay__inner">
            <div class="row justify-content-center text-center mb-5">
                <div class="col-lg-8">
                    <!-- Heading -->
                    <h1 class="g-color-white text-uppercase mb-4">{{trans('app.login_or_register_an_account')}}</h1>
                    <div class="d-inline-block g-width-35 g-height-2 g-bg-white mb-4"></div>
                {{--<p class="lead g-color-white">The time has come to bring those ideas and plans to life. This is where we really begin to visualize your napkin sketches and make them into beautiful pixels.</p>--}}
                <!-- End Heading -->
                </div>
            </div>

            <div class="row justify-content-center align-items-center no-gutters">
                <div class="col-lg-5 g-bg-teal g-rounded-left-5--lg-up">
                    <div class="g-pa-50">
                        <!-- Form -->
                        <form class="g-py-15" action="{{route('login')}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <h2 class="h3 g-color-white mb-4">{{trans('app.login')}}</h2>
                            @if($errors)
                                <ul>

                                    @foreach($errors->all() as $error)
                                        <span class="help-block text-danger">
                                            <strong>{{ $error }}</strong>
                                        </span>
                                    @endforeach
                                </ul>
                            @endif
                            <div class="mb-4">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text g-width-45 g-brd-right-none g-brd-white g-color-white"><i
                                                    class="icon-finance-067 u-line-icon-pro"></i></span>
                                    </div>
                                    <input class="form-control g-color-black g-brd-left-none g-brd-white g-bg-transparent g-color-white g-placeholder-white g-pl-0 g-pr-15 g-py-13"
                                           name="email" type="email" placeholder="{{trans('app.email')}}">
                                </div>
                            </div>

                            <div class="g-mb-40">
                                <div class="input-group rounded">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text g-width-45 g-brd-right-none g-brd-white g-color-white"><i
                                                    class="icon-communication-062 u-line-icon-pro"></i></span>
                                    </div>
                                    <input class="form-control g-color-black g-brd-left-none g-brd-white g-bg-transparent g-color-white g-placeholder-white g-pl-0 g-pr-15 g-py-13"
                                           name="password" type="password" placeholder="{{trans('app.password')}}">
                                </div>
                            </div>
                            <div class="mb-1">
                                <label class="form-check-inline u-check g-color-white g-font-size-12 g-pl-25 mb-2">
                                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="remember"
                                           type="checkbox">
                                    <div class="u-check-icon-checkbox-v6 g-absolute-centered--y g-left-0">
                                        <i class="fa" data-check-icon="&#xf00c"></i>
                                    </div>
                                    {{trans('app.remember_me')}}
                                </label>
                            </div>
                            <div class="g-mb-60">
                                <button class="btn btn-md btn-block u-btn-primary rounded text-uppercase g-py-13"
                                        type="submit">{{trans('app.login')}}
                                </button>
                            </div>

                            {{--<div class="text-center g-pos-rel pb-2 g-mb-60">--}}
                            {{--<div class="d-inline-block w-100 g-height-1 g-bg-white"></div>--}}
                            {{--<span class="u-icon-v2 u-icon-size--lg g-brd-white g-color-white g-bg-teal g-font-size-default rounded-circle text-uppercase g-absolute-centered g-pa-24">OR</span>--}}
                            {{--</div>--}}

                            {{--<button class="btn btn-block u-btn-facebook rounded text-uppercase g-py-13 g-mb-15" type="button">--}}
                            {{--<i class="mr-3 fa fa-facebook"></i>--}}
                            {{--<span class="g-hidden-xs-down">Login with</span> Facebook--}}
                            {{--</button>--}}
                            {{--<button class="btn btn-block u-btn-twitter rounded text-uppercase g-py-13" type="button">--}}
                            {{--<i class="mr-3 fa fa-twitter"></i>--}}
                            {{--<span class="g-hidden-xs-down">Login with</span> Twitter--}}
                            {{--</button>--}}
                        </form>
                        <!-- End Form -->
                    </div>
                </div>

                <div class="col-lg-5 g-bg-white g-rounded-right-5--lg-up">
                    <div class="g-pa-50">
                        <!-- Form -->
                        <form class="g-py-15" method="post" action="{{route('register')}}"
                              enctype="multipart/form-data">
                            <h2 class="h3 g-color-black mb-4">{{trans('app.new_sign_up')}}</h2>
                            {{--<p class="mb-4">Profitable contracts, invoices &amp; payments for the best cases!</p>--}}
                            {{csrf_field()}}

                            <div class="mb-4">
                                <div class="input-group rounded">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text g-width-45 g-brd-right-none g-brd-gray-light-v4 g-color-gray-dark-v5"><i
                                                    class="icon-finance-067 u-line-icon-pro"></i></span>
                                    </div>
                                    <input class="form-control g-color-black g-brd-left-none g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-pl-0 g-pr-15 g-py-13"
                                           type="text" name="name" required placeholder="{{trans('app.username')}}">
                                </div>
                                @if ($errors->has('name'))
                                    <span class="help-block text-danger">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                @endif
                            </div>

                            <div class="mb-4">
                                <div class="input-group rounded">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text g-width-45 g-brd-right-none g-brd-gray-light-v4 g-color-gray-dark-v5"><i
                                                    class="icon-communication-062 u-line-icon-pro"></i></span>
                                    </div>
                                    <input class="form-control g-color-black g-brd-left-none g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-pl-0 g-pr-15 g-py-13"
                                           name="email" type="email" required placeholder="{{trans('app.email')}}">
                                </div>
                                @if ($errors->has('email'))
                                    <span class="help-block text-danger">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                @endif
                            </div>

                            <div class="mb-4">
                                <div class="input-group rounded">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text g-width-45 g-brd-right-none g-brd-gray-light-v4 g-color-gray-dark-v5"><i
                                                    class="icon-media-094 u-line-icon-pro"></i></span>
                                    </div>
                                    <input class="form-control g-color-black g-brd-left-none g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-pl-0 g-pr-15 g-py-13"
                                           type="password" name="password" required
                                           placeholder="{{trans('app.password')}}">
                                </div>
                                @if ($errors->has('password'))
                                    <span class="help-block text-danger">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                @endif
                            </div>

                            <div class="mb-4">
                                <div class="input-group rounded">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text g-width-45 g-brd-right-none g-brd-gray-light-v4 g-color-gray-dark-v5"><i
                                                    class="icon-media-094 u-line-icon-pro"></i></span>
                                    </div>
                                    <input class="form-control g-color-black g-brd-left-none g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-pl-0 g-pr-15 g-py-13"
                                           type="password" name="password_confirmation" required
                                           placeholder="{{trans('app.confirm_password')}}">
                                </div>
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block text-danger">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                @endif
                            </div>

                            <div class="mb-1">
                                <label class="form-check-inline u-check g-color-gray-dark-v5 g-font-size-12 g-pl-25 mb-2">
                                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" required type="checkbox">
                                    <div class="u-check-icon-checkbox-v6 g-absolute-centered--y g-left-0">
                                        <i class="fa" data-check-icon="&#xf00c"></i>
                                    </div>
                                    {!! trans('app.agree_terms') !!}
                                </label>
                            </div>


                            <button class="btn btn-md btn-block u-btn-primary rounded text-uppercase g-py-13"
                                    type="submit">{{trans('app.register')}}
                            </button>
                        </form>
                        <!-- End Form -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Login & Signup -->

@endsection
