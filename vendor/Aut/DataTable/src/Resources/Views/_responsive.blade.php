<div class="row">
    <div data-dt-row={!! autDatatableEval('col.rowIndex') !!} data-dt-column={!! autDatatableEval('col.columnIndex') !!}>
        <div class="col-lg-12  col-md-3 col-xs-12">{!! autDatatableEval('col.title') !!} <span>:</span></div>
        <div class="col-lg-4  col-md-4 col-xs-12"><b>{!! autDatatableEval('col.data') !!}</b></div>
    </div>
</div>