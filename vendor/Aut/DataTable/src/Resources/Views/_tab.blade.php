<div class="tab-container tab-container--green hidden-sm hidden-xs">
    <ul class="nav nav-tabs nav-fill" role="tablist">
        @foreach($tabs as $index => $tab)
            <li class="nav-item {{ $index == 1 ? 'active' : '' }}">
                <a {{--data-tab="{{$id}}_{{$index}}_tab"--}} data-toggle="tab" href="#tab-{{$index}}" role="tab"
                   class="nav-link text-center {{ $index == 1 ? 'active show' : '' }}">
                    <h4 class="{{ $tab['icon'] }}"></h4><br/>{{ $tab['title'] }}
                </a>
            </li>
        @endforeach
    </ul>

    <div class="tab-content">
        @foreach($tabs as $index => $tab)
            <div {{--data-tab="{{$id}}_{{$index}}_tab"--}} id="tab-{{$index}}" role="tabpanel"
                 class="tab-pane fade {{ $index == 0 ? 'active show animated zoomInUp' : '' }}">
                {!! $tab['content'] !!}
            </div>
        @endforeach
    </div>
</div>