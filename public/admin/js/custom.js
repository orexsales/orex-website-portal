$(function () {
    $('.search__text').keyup(function () {
        var text = $(this).val();
        _searchHtml(text)
    });
});

function _scroll() {
    $('body').niceScroll({
        cursorcolor: "#FF2558",
        cursorwidth: "5px",
        zindex: 9999999,
        background: "rgba(20,20,20,0.7)",
        cursorborder: "1px solid #FF2558",
        cursorborderradius: 10
    });
}

function _initBreadCrumbs() {
    var breadcrumbs = $('#admin-bread');
    var ele = $('.navigation__active');
    breadcrumbs.append('<li class="breadcrumb-item"><a href="#">' + ele.parent().parent().children('a').text() + '</a></li>');
    breadcrumbs.append('<li class="breadcrumb-item">' + ele.children('a').text() + '</li>')
}

function _searchHtml($text) {

    var theText = $('#test');

    var theString = $text;

    var numCharacters = theString.length;

    var newHTML = "";

    for (i = 0; i <= numCharacters; i++) {

        var newHTML = newHTML + "" + theString[i] + "";

    }

    theText.html(newHTML);
    console.log(":contains('" + $text + "')");
    var selectors = ['label', 'a', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'];
    var searchText = [];
    searchText.push($text);
    console.log(searchText);
    if ($text) {

        for (var $i = 0; $i <= selectors.length; $i++) {
            if ($(selectors[$i] + ":contains('" + $text + "')")) {
                $(selectors[$i] + ":contains('" + $text + "')")
                    .css('background-color', '');
            }
            if ($(selectors[$i] + ":contains('" + $text + "')")) {
                $(selectors[$i] + ":contains('" + $text + "')")
                    .css('background-color', '#DD1144');
            }
        }
    }
}

function _maskInput() {
    $('.input-mask').each(function () {
        var type = $(this).data('type');
        var $this = $(this);
        switch (type) {
            case 'phone':
                $this.attr('placeholder', 'eg: (+963) 000 000 000');
                $this.mask('(+963) 000 000 000');
                break;
            case 'percent':
                $this.attr('placeholder', 'eg: 0,00%');
                $this.mask('##0,00%', {reverse: true});
                break;
            case 'money':
                $this.attr('placeholder', 'eg: 000.000,00');
                $this.mask('000.000.000.000.000,00', {reverse: true});
                break;
            default:
                break;
        }
    })
}

function aut_filemanager_handleImageUpload(event, baseURL, Lang, pareEditor) {
    var ParentEditor = pareEditor;
    var editor = event.editor;
    var dialogDefinition = event.data.definition;
    var dialogName = event.data.name;
    var tabCount = dialogDefinition.contents.length;
    var iframe;
    for (var i = 0; i < tabCount; i++) {
        if (typeof(dialogDefinition.contents[i]) !== typeof(undefined)) {
            var browseButton = dialogDefinition.contents[i].get('browse');
            if (browseButton !== null) {
                browseButton.hidden = false;
                browseButton.onClick = function (dialog, i) {

                    CKFinder.setupCKEditor(ParentEditor, {skin: 'core'});
                    CKFinder.modal( {
                        chooseFiles: true,
                        width: 800,
                        height: 600,
                        "z-index":100000
                        // z-index:
                        // onInit: function( finder ) {
                        //     finder.on( 'files:choose', function( evt ) {
                        //         var file = evt.data.files.first();
                        //         var output = document.getElementById( elementId );
                        //         output.value = file.getUrl();
                        //     } );
                        //
                        //     finder.on( 'file:choose:resizedImage', function( evt ) {
                        //         var output = document.getElementById( elementId );
                        //         output.value = evt.data.resizedUrl;
                        //     } );
                        // }
                    });

                    };
                }
            }

    }


}

function loadedFrame(frame) {
    var body = $(frame[0]).find('body');
    var elements = document.getElementById('fm-iframe').contentWindow.document.getElementsByClassName('ckf-file-item');
    var testDivs = Array.prototype.filter.call(elements, function(element){
        return element;
    });
    for (var i = 0; i < elements.length; i++) {
        console.log(elements[i])
        elements[i].onClick(function (e) {
            e.preventDefault();
            console.log('click')
        })
        $('#fm-iframe').contents().find('li.ckf-file-item').each(function () {
            console.log('enter')
            $(this).on('dbclick', function (e) {
                e.preventDefault();
                console.log('db')
            })
            $(this).on('click', function (e) {
                e.preventDefault();
                console.log('click')
            })
        })
    }
    // $(frame).find('li.ckf-file-item').each( function () {
    //     $(this).on('dbclick',function (e) {
    //         e.preventDefault();
    //         console.log('db')
    //     })
    //     $(this).on('click',function (e) {
    //         e.preventDefault();
    //         console.log('click')
    //     })
    // })
}