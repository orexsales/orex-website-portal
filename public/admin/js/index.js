/**
 * Created by Mohammad on 11/18/2017.
 */
// $(function () {
//     _loadEvents();
//     $(window).load(function () {
//         $('.loader').hide('slow');
//     });
// });
$(document).ready(function ($) {
    $(window).load(function () {
        $('.loader').fadeOut('slow', function () {
            $(this).remove();
        });
    });
    $('.navigation__active').parent().parent().addClass('navigation__sub--active');
    if ($('.navigation__active').find('a').text() === '' || $('.navigation__active').find('a').text() === undefined)
        $('.breadcrumb').css('display', 'none');
    $('.breadcrumb').find('#child').text($('.navigation__active').find('a').text());
    $('.breadcrumb').find('#parent').text($('.navigation__active').parent().parent().find('a').first().text());
    $('button.dropdown-toggle').each(function () {
        $(this).on('click', function () {
            $(this).parent().find('div.dropdown-menu.open').toggleClass('show')
        })
    })
});
$(document).ajaxStop(function () {
});

function _loadEvents($container, callback) {
    if (null == $container) $container = $('.main');
    _validate($container, callback); // Dynamic ajax validation
    _editable($container); // Dynamic editable
    _imageUpload($container);  // Dynamic image Uploads
    _imageCropUpload($container);
    _docUpload($container);// Dynamic documents Uploads
    _textEditor($container);    //CK Editor
    _ajaxreqs($('body')); // Dynamic ajax requests
    _select2($container); // Dynamic select2
    _datePicker($container);
    _dateTimePicker($container);
    _timePicker($container);
    _bar_chart();
    _moment_ago();
    _selectPicker();
    // _lineChart();
}

function _selectPicker() {
    if (typeof $.fn.selectpicker !== typeof undefined) {
        $('.selectpicker').selectpicker();
    }
}

function _select() {
    if (typeof $.fn.select2 !== typeof undefined) {
        $('select.select2').select2();
    }
}

function _accept($this) {
    var data = {
        id: $($this).data('id'),
        not_id: $($this).data('not'),
        _token: _csrf
    };
    $.ajax({
        type: 'POST',
        url: baseUrl + 'notification-read',
        data: data,
        success: function (data) {
            window.location = data.url;
        }
    });
}

function _moment_ago() {
    $('time[data-date]').each(function () {
        var date = $(this).data('date');
        moment.locale(lang);
        var new_date = moment(date).fromNow();
        $(this).text(new_date)
    })
}

function _validate($cont, inCallback, inMethod, resetForm, event) {
    var ee = event ? event.preventDefault() : "";
    if (typeof ($.fn.validate) != 'undefined') {
        $cont = $($cont);
        $cont.find('form.ajax-form').each(function () {
            var url = $(this).attr('action');
            var method = inMethod || $(this).attr('method') || 'post';
            var callback = inCallback || $(this).attr('data-callback');//support callback function (defiend by user - client side)
            var rules = [];
            $(this).validate().destroy();
            var validator = $(this).validate({
                    submitHandler: function (form, e) {
                        // __loaderStart(form);
                        $.ajax({
                            type: method,
                            url: url,
                            data: $(form).serialize(),
                            success: function (data) {
                                if (callback != null) {// Support callback function (defined by user - client side)
                                    fn = window[callback];
                                    if (typeof fn === "function")
                                        fn(data);
                                }
                                // __loaderEnd(form);
                                if (null != data.msg) {// Support (server side) message
                                    SEMICOLON.widget.notifications($('<div id="" data-notify-position="top-right" data-notify-type="info" data-notify-msg="<i class=\'icon-info-sign\'></i>  ' + data.msg + '"></div>'));
                                }
                                if (null != data.redirect) { //support (server side) redirect
                                    window.location = data.redirect;
                                }
                                if ($(form).parents('.modal:first').length > 0 && !$(form).parents('.modal:first').data('no-dismiss')) {
                                    $(form).parents('.modal:first').modal('hide');
                                }
                                $('.modal').modal('hide');
                            },
                            error: function (data) {
                                // __loaderEnd(form);
                                var errorsObj = data.responseJSON;
                                /*if (null != data.msg) {// Support (server side) message
                                 SEMICOLON.widget.notifications($('<div id="" data-notify-position="top-right" data-notify-type="' + ((data.success) ? "info" : "error") + '" data-notify-msg="<i class=\'icon-info-sign\'></i>  ' + data.msg + '"></div>'));
                                 }*/
                                if (null != errorsObj) {// Support (server side) validation errors
                                    var scrolled = false; // helper to Support scroll to first error field
                                    for (i in errorsObj) { //handle each field error
                                        var $errorField = $(form).find('[name="' + i + '"]:first');
                                        if (errorsObj[i].length > 1) {//handle  field multiple errors
                                            errorsStr = "";
                                            for (var j = 0; j < errorsObj[i].length; j++) {
                                                errorsStr += "<li>" + errorsObj[i][j] + "</li>";
                                            }
                                        } else {
                                            var errorsStr = errorsObj[i][0];
                                        }
                                        $(form).find('#' + $errorField.attr('id') + '-error').html(errorsStr).show();// show errors
                                        if (!scrolled) { //scroll to first field error field
                                            $('html, body').animate({
                                                scrollTop: ($errorField.offset().top - 100)
                                            }, 1000);
                                            scrolled = true;
                                        }
                                        //Handle Opening Tab
                                        if ($errorField.parents('.tab-content:first').length > 0) {
                                            var tabId = $errorField.parents('.tab-content:first').attr('Id');
                                            $('a[href="#' + tabId + '"]').click();
                                        }
                                    }
                                }
                            },
                            dataType: "json"
                        });
                        return false;
                    },
                    ignore: '.not-required :input',
                    errorPlacement: function ($error, $element) {
                        var id = $element.attr("id");
                        $error.addClass('f' + right);
                        $("#error-" + id).after($error);

                    },
                    /*
                     errorClass: 'error f' + right+' validate-error validate-error-help-block validate-error-style  ',
                     */
                    highlight: function (e, errorClass, validClass) {
                        var elem = jQuery(e);
                        elem.addClass(errorClass);
                        /*elem.parent('.form-group').removeClass('has-error').addClass('has-error');
                         elem.closest('.help-block').remove();*/
                        //Handle Opening Tab
                        if (elem.parents('.tab-content:first').length > 0) {
                            var tabId = elem.parents('.tab-content:first').attr('Id');
                            $('a[href="#' + tabId + '"]').click();
                        }
                    },
                    /* unhighlight: function(e, errorClass, validClass) {
                     var elem = jQuery(e);
                     elem.parent('.form-group').removeClass('has-error');
                     },*//*
                     success: function(e) {
                     var elem = jQuery(e);
                     elem.parent('.form-group').removeClass('has-error');
                     elem.closest('.help-block').remove();
                     }
                     */
                }
            );
            if (resetForm)
                validator.resetForm();

            $(this).find('.has-error').removeClass('has-error');
            if ($(this).find('.autocomplete').length) {
                $(this).bind('invalid-form.validate', function () {
                    $('select.autocomplete.required').each(function () {
                        if ($(this).val() == null)
                            $(this).parent().addClass('has-error');
                        else
                            $(this).parent().removeClass('has-error');
                    });
                });
            }
        });
    }
    /*else console.log('warning: validate is not defined');*/

}

function _resetValidatorForm($cont, callback) {
    $cont.find('.autocomplete').html('');
    $cont.find('.image-crop-upload').attr('src', '');
    if (typeof (CKEDITOR) != 'undefined') {
        for (name in CKEDITOR.instances) {
            if ($cont.find('textarea[name="' + name + '"]').length)
                CKEDITOR.instances[name].setData('');
        }
    }
    _select2($cont);
    _validate($cont, callback, null, true);
}

function _editable($cont) {
    if (typeof ($.fn.editable) !== 'undefined') {
        $cont = $($cont);
        $cont.find('.editable').each(function () {
            var $this = $(this);
            var url = $this.data('ajaxUrl');
            var method = $this.data('data-method') || 'put';
            $this.editable({
                url: function (params) {
                    var data = {};
                    data[params.name] = params.value;
                    if (method !== "post") data._method = method;
                    $.post(url, data, function (data) {
                        SEMICOLON.widget.notifications($('<div id="" data-notify-position="top-right" data-notify-type="' + (typeof (data[name]) == typeof (undefined) ? "info" : "error") + '" data-notify-msg="<i class=\'icon-info-sign   \'></i> <span class=\'\'> ' + data.msg + '</span>"></div>'));
                    });
                },
                select2: {
                    multiple: true,
                    minimumInputLength: 3,
                },
                width: 350,
                success: function (data, config) {
                    if (data && data.id) {  //record created, response like {"id": 2}
                        //set pk
                        $(this).editable('option', 'pk', data.id);
                        //remove unsaved class
                        $(this).removeClass('editable-unsaved');
                        //show messages
                        var msg = 'New user created! Now editables submit individually.';
                        $('#msg').addClass('alert-success').removeClass('alert-error').html(msg).show();
                        $('#save-btn').hide();
                        $(this).off('save.newuser');
                    } else if (data && data.errors) {
                        //server-side validation error, response like {"errors": {"username": "username already exist"} }
                        config.error.call(this, data.errors);
                    }

                },
                error: function (errors) {
                    var msg = '';
                    if (errors && errors.responseText) { //ajax error, errors = xhr object
                        msg = errors.responseText;
                    } else { //validation error (client-side or server-side)
                        $.each(errors, function (k, v) {
                            msg += k + ": " + v + "<br>";
                        });
                    }
                    $('#msg').removeClass('alert-success').addClass('alert-error').html(msg).show();
                }
            });
        });
    }
    /*else console.log('warning: editable is not defined');*/
}

function _imageUpload($cont, $name, $table, files, configs) {
    // Standard input:
    if (typeof ($.fn.fileinput) !== 'undefined') {
        $cont = $($cont);
        $cont.find(".image-uploade:not('.temp')").fileinput('destroy');
        $cont.find(".image-uploade:not('.temp')").each(function () {
            var $this = $(this);
            $this.attr('data-height'),
                $this.attr('data-size')
            var data = $this.attr('data-url') || null;
            var title = $this.attr('data-title') || '';
            var initialPreview = [];
            var type = $this.attr('data-type').split(',') || ['image'];
            var maxFileSize = type[0] === "image" ? 10000 : 100000;
            var pickImageStr = type[0] === "image" ? pickimage : pick;
            var previewFileType = type[0] === "image" ? "image" : "file";

            var allfiles = [];
            var initialPreviewConfig = [];
            if (files === undefined || files === '')
                console.log('failed');
            else {
                $.each(files, function (i, v) {
                    allfiles.push(v);
                });
                for (var i = 0; i < allfiles.length; i++) {
                    initialPreviewConfig[i] =
                        {
                            type: "image",
                            size: configs[i].size,
                            caption: configs[i].caption,
                            key: configs[i].key,
                            url: configs[i].url,
                            downloadUrl: configs[i].downloadUrl
                        };
                }

            }

            var params = {
                language: lang,
                maxFileCount: 10,
                showCaption: false,
                mainClass: "",
                showUpload: true,
                elErrorContainer: "#errorBlock",
                showRemove: true,
                fileActionSettings: {
                    downloadIcon: '<i class="fa fa-download"></i>',
                    indicatorNew: '',
                    showRemove: true,
                    showDrag: true,
                    showUpload: !0,
                    showDownload: !0,
                    showZoom: !0,
                    removeIcon: '<i class="fa fa-trash"></i>',
                    removeClass: "btn btn-kv btn-default btn-outline-secondary text-danger",
                    removeErrorClass: "btn btn-kv btn-danger",
                    removeTitle: "Remove file",
                    uploadIcon: '<i class="glyphicon glyphicon-upload"></i>',
                    uploadClass: "btn btn-kv btn-default btn-outline-secondary text-success",
                    uploadTitle: "Upload file",
                    uploadRetryIcon: '<i class="fa fa-repeat"></i>',
                    uploadRetryTitle: "Retry upload",
                    downloadClass: "btn btn-kv btn-default btn-outline-secondary text-success",
                    downloadTitle: "Download file",
                    zoomIcon: '<i class="fa fa-search-plus"></i>',
                    zoomClass: "btn btn-kv btn-default btn-outline-secondary text-info",
                    zoomTitle: "View Details",
                },
                theme: 'fa',
                downloadIcon: '<i class="fa fa-download"></i>',
                uploadUrl: baseUrl + "admin/multi-upload-image",
                // deleteUrl: baseUrl + 'admin/delete-file/{key}',
                uploadExtraData: {name: $name, table: $table},
                browseClass: "btn btn-success btn--icon-text waves-effect",
                browseLabel: pickImageStr,
                browseIcon: "<i class=\"icon-picture\"></i> ",
                uploadClass: "btn btn-info hide btn--icon-text waves-effect",
                removeClass: "btn btn-danger btn--icon-text waves-effect",
                removeLabel: del,
                removeIcon: "<i class=\"icon-trash\"></i> ",
                maxFileSize: maxFileSize,
                // /*resizePreference: 'width',*/
                allowedFileExtensions: ['jpg', 'png', 'jpeg', 'mp4', 'avi', 'flv', 'bmp'],
                allowedPreviewTypes: type,
                // purifyHtml: true,
                uploadAsync: false,
                minFileCount: 2,
                // maxFileCount: 5,
                overwriteInitial: false,
                previewFileType: 'image',
                initialPreviewAsData: false, // allows you to set a raw markup
                initialPreviewFileType: 'image',
                initialPreviewShowDelete: true,
                initialPreview: allfiles,
                initialPreviewConfig: initialPreviewConfig,
                maxImageWidth: $this.attr('data-width') ? $this.attr('data-width') : null,
                maxImageHeight: $this.attr('data-height') ? $this.attr('data-height') : null,
                resizeImage: $this.attr('data-size') === 'true' ? true : false,
                // autoReplace: true
            };
            if ($this.data('width'))
                params.minImageWidth = $this.data('width');
            if ($this.data('height'))
                params.minImageHeight = $this.data('height');
            $(this).fileinput(params).on(type[0] !== 'image' ? 'fileloaded' : 'fileimagesloaded', function (event, file, previewId, index, reader) {
                var $parent = $(event.currentTarget).parents('.file-input:first');
                // if ($parent.find('.file-preview-frame').length > 1) $('.file-preview-frame:first .kv-file-remove').click();
                if (!$parent.find('.kv-file-upload').prop('disabled')) $parent.find('.kv-file-upload').click();
            }).on('fileuploaded', function (event, data, previewId, index) {
                var $fileInput = $(event.currentTarget);
                $fileInput.parents('.file-input:first').prev().prev().val(data.response.filename);
            }).on('fileclear', function (event) {
                $(event.currentTarget).parents('.file-input:first').prev().prev().val('');
            });
        });
        $cont.find(".image-upload").change(function () {
            $(this).parents('.form-group').find('label.error').remove();
        });
    }
    /*else console.log('warning: fileinput is not defined');*/
}

function _videoUpload($cont, $name, $table, urls, configs) {
    // Standard input:
    if (typeof ($.fn.fileinput) !== 'undefined') {
        $cont = $($cont);
        $cont.find(".image-upload:not('.temp')").fileinput('destroy');
        $cont.find(".image-upload:not('.temp')").each(function () {
            var $this = $(this);
            var data = $this.attr('data-url') || null;
            var title = $this.attr('data-title') || '';
            var initialPreview = [];
            var type = $this.attr('data-type').split(',') || ['image'];
            var maxFileSize = type[0] === "image" ? 10000 : 100000;
            var pickImageStr = type[0] === "image" ? pickimage : pick;
            var previewFileType = type[0] === "image" ? "image" : "file";

            var allfiles = [];
            var initialPreviewConfig = [];
            // if (files === undefined || files === '')
            //     console.log('failed');
            // else {
            //     $.each(files, function (i, v) {
            //         allfiles.push(v);
            //     });
            //     for (var i = 0; i < allfiles.length; i++) {
            //         initialPreviewConfig[i] =
            //             {
            //                 type: "image",
            //                 size: configs[i].size,
            //                 caption: configs[i].caption,
            //                 key: Math.floor(Math.random() * 1000 + 1),
            //                 url: configs[i].url,
            //                 downloadUrl: configs[i].downloadUrl
            //             };
            //     }
            //
            // }

            if (data != null) {
                if (type[0] === "image") initialPreview = ["<img src='" + data + "' class='file-preview-image' >"];
                else initialPreview = [/*'<div class="file-preview-frame file-preview-success" id="uploaded-1482243555677" data-fileindex="-1" title="'+title+'" style="width:160px;height:160px;">'+*/
                    '<object class="file-object" data="' + data + '" type="video/mp4" width="160px" height="160px" internalinstanceid="15">' +
                    '<param name="movie" value="' + title + '">' +
                    '<param name="controller" value="true">' +
                    '<param name="allowFullScreen" value="true">' +
                    '<param name="allowScriptAccess" value="always">' +
                    '<param name="autoPlay" value="false">' +
                    '<param name="autoStart" value="false">' +
                    '<param name="quality" value="high">' +
                    '<div class="file-preview-other">' +
                    '<span class="file-icon-4x"><i class="icon-file-alt"></i></span>' +
                    '</div>' +
                    '</object>'];
                initialPreviewConfig = [{
                    url: data,
                    key: 100
                }];
            } else {
                if (urls) {

                    initialPreview = '<video width="200" height="150" controls>\n' +
                        '  <source src=' + urls + ' type="video/mp4">\n' +
                        'Your browser does not support the video tag.\n' +
                        '</video>';
                    initialPreviewConfig = [{
                        type: "video",
                        size: configs.size,
                        caption: configs.caption,
                        key: configs.key,
                        url: configs.url,
                        downloadUrl: configs.downloadUrl
                    }];
                }

            }
            var params = {
                language: lang,
                maxFileCount: 10,
                showCaption: false,
                mainClass: " ",
                showUpload: true,
                showRemove: true,
                elErrorContainer: "#errorBlock",
                fileActionSettings: {
                    downloadIcon: '<i class="fa fa-download"></i>',
                    indicatorNew: '',
                    showRemove: true,
                    showDrag: true,
                    showUpload: !0,
                    showDownload: !0,
                    showZoom: !0,
                    removeIcon: '<i class="fa fa-trash"></i>',
                    removeClass: "btn btn-kv btn-default btn-outline-secondary text-danger",
                    removeErrorClass: "btn btn-kv btn-danger",
                    removeTitle: "Remove file",
                    uploadIcon: '<i class="glyphicon glyphicon-upload"></i>',
                    uploadClass: "btn btn-kv btn-default btn-outline-secondary text-success",
                    uploadTitle: "Upload file",
                    uploadRetryIcon: '<i class="glyphicon glyphicon-repeat"></i>',
                    uploadRetryTitle: "Retry upload",
                    downloadClass: "btn btn-kv btn-default btn-outline-secondary text-success",
                    downloadTitle: "Download file",
                    zoomIcon: '<i class="fa fa-search-plus"></i>',
                    zoomClass: "btn btn-kv btn-default btn-outline-secondary text-info",
                    zoomTitle: "View Details",
                },
                'theme': 'explorer-fa',
                downloadIcon: '<i class="fa fa-download"></i>',
                uploadUrl: baseUrl + "admin/upload-image",
                // deleteUrl: baseUrl + 'admin/delete-file/{key}',
                uploadExtraData: {name: $name, table: $table},
                browseClass: "btn btn-success btn--icon-text waves-effect",
                browseLabel: pickImageStr,
                browseIcon: "<i class=\"icon-picture\"></i> ",
                uploadClass: "btn btn-info hide btn--icon-text waves-effect",
                removeClass: "btn btn-danger btn--icon-text waves-effect",
                removeLabel: del,
                removeIcon: "<i class=\"icon-trash\"></i> ",
                resizeImage: false,
                maxFileSize: maxFileSize,
                // /*resizePreference: 'width',*/
                allowedFileExtensions: ['jpg', 'png', 'jpeg', 'mp4', 'avi', 'flv', 'bmp'],
                allowedPreviewTypes: type,
                // purifyHtml: true,
                uploadAsync: false,
                minFileCount: 2,
                // maxFileCount: 5,
                overwriteInitial: false,
                previewFileType: 'video',
                initialPreviewAsData: false, // allows you to set a raw markup
                initialPreviewFileType: 'video',
                // fileTypeSettings:'video',
                initialPreviewShowDelete: true,
                initialPreview: initialPreview,
                initialPreviewConfig: initialPreviewConfig,
                // autoReplace: true
            };
            if ($this.data('width'))
                params.minImageWidth = $this.data('width');
            if ($this.data('height'))
                params.minImageHeight = $this.data('height');
            $(this).fileinput(params).on(type[0] !== 'image' ? 'fileloaded' : 'fileimagesloaded', function (event, file, previewId, index, reader) {
                var $parent = $(event.currentTarget).parents('.file-input:first');
                // if ($parent.find('.file-preview-frame').length > 1) $('.file-preview-frame:first .kv-file-remove').click();
                if (!$parent.find('.kv-file-upload').prop('disabled')) $parent.find('.kv-file-upload').click();
            }).on('fileuploaded', function (event, data, previewId, index) {
                var $fileInput = $(event.currentTarget);
                $fileInput.parents('.file-input:first').prev().prev().val(data.response.filename);
            }).on('fileclear', function (event) {
                $(event.currentTarget).parents('.file-input:first').prev().prev().val('');
            });
        });
        $cont.find(".image-upload").change(function () {
            $(this).parents('.form-group').find('label.error').remove();
        });
    }
    /*else console.log('warning: fileinput is not defined');*/
}

function _imageCropUpload($cont, $name, $resize = null) {
    /*if ($fileInput.data('width')) {
     var $corpModal = $fileInput.parents('.image-upload-cont:first').next();
     $('body').append($corpModal);
     $corpModal.find("#target-corp").attr('src',data.response.fileurl);
     //$corpModal.find("#target-corp").css({'src',data.response.fileurl});
     $corpModal.find('.modal-dialog').css({'min-width':$fileInput.data('width')+50,'min-height':$fileInput.data('height')+50});
     $corpModal.modal('show');
     $corpModal.find("#target-corp").Jcrop({
     //aspectRatio: $fileInput.data('width') / $fileInput.data('height'),
     minSize: [$fileInput.data('width') , $fileInput.data('height') ],
     maxSize: [$fileInput.data('width') , $fileInput.data('height') ],
     }, function () {
     this.animateTo([0, 0, $fileInput.data('width'), $fileInput.data('height')]);
     });
     }*/
    if (typeof ($.fn.cropper) !== 'undefined') {
        $cont = $($cont);
        //$cont.find(".image-crop-upload:not('.temp')").fileinput('destroy');
        $cont.find(".image-crop-upload:not('.temp')").each(function () {
            var $this = $(this);
            // console.log($this);
            var data = $this.data('url') || null;
            var title = $this.data('title') || '';
            var width = $this.data('width') || 0;
            var height = $this.data('height') || 1;
            var size = $this.data('size') || 0;
            var options = {
                viewMode: 1,
                aspectRatio: size == 1 ? width / height : NaN,
                zoom: function (e) {
                    // console.log(e.type, e.detail);
                    // console.log(e.type, e.detail.ratio);
                },
                zoomable: true,
                zoomOnWheel: true
            };
            //onload Crop
            $this.cropper('destroy');
            //hide the crop action
            $this.prev().prev().addClass('hide');
            //Handle Choose Image Operation
            _imageCropUpload_handleChooseImage($this, options);
            //Handle Crop Operation
            _imageCropUpload_handleCropImage($this, $name, $resize);

            /*i*/
        });
    }
    /*else console.log('warning: fileinput is not defined');*/
}

function _imageCropUploadPng($cont, $name) {
    /*if ($fileInput.data('width')) {
     var $corpModal = $fileInput.parents('.image-upload-cont:first').next();
     $('body').append($corpModal);
     $corpModal.find("#target-corp").attr('src',data.response.fileurl);
     //$corpModal.find("#target-corp").css({'src',data.response.fileurl});
     $corpModal.find('.modal-dialog').css({'min-width':$fileInput.data('width')+50,'min-height':$fileInput.data('height')+50});
     $corpModal.modal('show');
     $corpModal.find("#target-corp").Jcrop({
     //aspectRatio: $fileInput.data('width') / $fileInput.data('height'),
     minSize: [$fileInput.data('width') , $fileInput.data('height') ],
     maxSize: [$fileInput.data('width') , $fileInput.data('height') ],
     }, function () {
     this.animateTo([0, 0, $fileInput.data('width'), $fileInput.data('height')]);
     });
     }*/
    if (typeof ($.fn.cropper) !== 'undefined') {
        $cont = $($cont);
        //$cont.find(".image-crop-upload:not('.temp')").fileinput('destroy');
        $cont.find(".image-crop-upload:not('.temp')").each(function () {
            var $this = $(this);
            // console.log($this);
            var data = $this.data('url') || null;
            var title = $this.data('title') || '';
            var width = $this.data('width') || 0;
            var height = $this.data('height') || 1;
            var options = {
                viewMode: 1,
                aspectRatio: NaN,
                zoom: function (e) {
                    // console.log(e.type, e.detail);
                    // console.log(e.type, e.detail.ratio);
                },
                zoomable: true,
                zoomOnWheel: true
            };
            //onload Crop
            $this.cropper('destroy');
            //hide the crop action
            $this.prev().prev().addClass('hide');
            
            //Handle Choose Image Operation
            _imageCropUpload_handleChooseImage($this, options);
            //Handle Crop Operation
            _imageCropUpload_handleCropImagePng($this, $name);

            /*i*/
        });
    }
    /*else console.log('warning: fileinput is not defined');*/
}

function _imageCropUpload_handleChooseImage($image, options) {
    var $inputFile = $($image).prev().prev().prev().prev();
    var $chooseAction = $($image).prev().prev().prev();
    $chooseAction.unbind('click').click(function () {
        $inputFile.click();
    });
    $inputFile.change(function () {
        var files = this.files;
        var file;
        if (files && files.length) {
            file = files[0];
            if (/^image\/\w+$/.test(file.type)) {
                var uploadedImageURL = URL.createObjectURL(file);
                $($image).prev().prev().removeClass('hide');
                $image.parents('form:first').find('[type="submit"]').addClass('hide');
                $image.attr('src', uploadedImageURL);
                $image.cropper(options);
            } else {
                $($image).prev().prev().addClass('hide');
                window.alert('Please choose an image file.');
            }
        }
    });
}

function _imageCropUpload_handleCropImage($image, $name, $resize) {
    var $this = $image;
    var $cropImageAction =  $this.prev().prev();
    $cropImageAction.unbind('click').click(function (event) {
        event.preventDefault();
        var $cont = $this.parents('.image-upload-cont:first');
        $cont.hide().wrap('<div class="preloader2" style="min-height:300px"></div>');
        $(this).addClass('hide');
        var val = $('#image').val();
        var extension = val.substr(val.indexOf(".") + 1);
        var image = $this.cropper('getCroppedCanvas').toDataURL('image/jpeg');
        /*.toBlob(function (blob) {*/
        // $this.cropper('getCroppedCanvas').toBlob(function (blob) {
        var formData = new FormData();
        formData.append('image_url', image);
        formData.append('name', $name);
        $.ajax(`${baseUrl}admin/upload-image${$resize ? '/image' : ''}`, {
            method: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function (res) {
                var $image = $('.image-crop-upload.cropper-hidden');
                var $form = $image.removeClass('hide').parents('form:first');
                $image.attr('src', res.fileurl);
                $image.cropper('destroy');
                $form.find('.image-upload-name').val(res.filename);
                // $form.find('#image-name').val(res.filename);
                $form.find('input[type="file"]').val("");
                $form.find('.image-upload-cont:first').unwrap('<div class="preloader2"  style="min-height:300px"></div>').show();
                $form.find('[type="submit"]').removeClass('hide');
            }
            // });
        });
    });
}

function _imageCropUpload_handleCropImagePng($image, $name) {
    var $this = $image;
    var $cropImageAction = $this.prev().prev();
    $cropImageAction.unbind('click').click(function (event) {
        event.preventDefault();
        var $cont = $this.parents('.image-upload-cont:first');
        $cont.hide().wrap('<div class="preloader2" style="min-height:300px"></div>');
        $(this).addClass('hide');
        var val = $('#image').val();
        var extension = val.substr(val.indexOf(".") + 1);
        var image = $this.cropper('getCroppedCanvas').toDataURL('image/png');
        /*.toBlob(function (blob) {*/
        // $this.cropper('getCroppedCanvas').toBlob(function (blob) {
        var formData = new FormData();
        formData.append('image_url', image);
        formData.append('name', $name);

        $.ajax(baseUrl + "admin/upload-image", {
            method: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function (res) {
                var $image = $('.image-crop-upload.cropper-hidden');
                var $form = $image.removeClass('hide').parents('form:first');
                $image.attr('src', res.fileurl);
                $image.cropper('destroy');
                $form.find('.image-upload-name').val(res.filename);
                // $form.find('#image-name').val(res.filename);
                $form.find('input[type="file"]').val("");
                $form.find('.image-upload-cont:first').unwrap('<div class="preloader2"  style="min-height:300px"></div>').show();
                $form.find('[type="submit"]').removeClass('hide');
            }
            // });
        });
    });
}

function _docUpload($cont, id, files, configs) {
    if (typeof ($.fn.fileinput) !== 'undefined') {
        $cont = $($cont);
        var allfiles = [];
        var initialPreviewConfig = [];
        if (files === undefined || files === '')
            console.log('failed');
        else {
            $.each(files, function (i, v) {
                allfiles.push(v);
            });
            for (var i = 0; i < allfiles.length; i++) {
                initialPreviewConfig[i] = [
                    {
                        type: "pdf",
                        size: configs[i].size,
                        caption: configs[i].caption,
                        key: configs[i].key,
                        url: configs[i].url,
                        downloadUrl: configs[i].downloadUrl
                    }
                ];
            }
        }

        $cont.find(".doc-upload").fileinput('destroy');
        $cont.find(".doc-upload").each(function () {

            var $this = $(this),
                param = $this.attr('data-param') || '';
            var uploadExtraData = {};

            // if (param !== '')
            //     $.each(param.split('&'), function (v, i) {
            //         var extra = JSON.parse('{"' + v.split('=')[0] + '" : "' + v.split('=')[1] + '"}');
            //         $.extend(uploadExtraData, extra);
            //     });

            $(this).fileinput({
                theme: "fa",
                showCaption: false,
                fileActionSettings: {
                    downloadIcon: '<i class="fa fa-download"></i>',
                    dragIcon: '',
                    indicatorNew: ''
                },
                showCancel: false,
                browseLabel: pickdoc,
                browseIcon: "<i class=\"icon-book\"></i> ",
                removeLabel: del,
                removeIcon: "<i class=\"icon-trash\"></i> ",
                uploadUrl: baseUrl + 'admin/upload-doc/' + id,
                uploadExtraData: uploadExtraData,
                allowedFileExtensions: ["pdf"],//"txt", "md", "doc", "text", "docx", "xls",
                allowedFileType: ['image', 'html', 'text', 'video', 'audio', 'flash', 'object'],
                uploadAsync: false,
                maxFileCount: 1,
                overwriteInitial: false,
                initialPreview: allfiles,
                initialPreviewAsData: true, // defaults markup
                previewFileType: 'any',
                initialPreviewFileType: 'pdf', // image is the default and can be overridden in config below
                initialPreviewConfig: configs,
                autoReplace: true,
                showUpload: false,
                 previewFileIcon: '<i class="fa fa-file"></i>',
                // preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
                // previewFileIconSettings: { // configure your icon file extensions
                //     'doc': '<i class="fa fa-file-word-o text-primary"></i>',
                //     'xls': '<i class="fa fa-file-excel-o text-success"></i>',
                //     'ppt': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                //     'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                //     'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
                //     'htm': '<i class="fa fa-file-code-o text-info"></i>',
                //     'txt': '<i class="fa fa-file-text-o text-info"></i>',
                //     'mov': '<i class="fa fa-file-movie-o text-warning"></i>',
                //     'mp3': '<i class="fa fa-file-audio-o text-warning"></i>',
                //     // note for these file types below no extension determination logic
                //     // has been configured (the keys itself will be used as extensions)
                //     'jpg': '<i class="fa fa-file-photo-o text-danger"></i>',
                //     'gif': '<i class="fa fa-file-photo-o text-warning"></i>',
                //     'png': '<i class="fa fa-file-photo-o text-primary"></i>'
                // },
                // previewFileExtSettings: { // configure the logic for determining icon file extensions
                //     'doc': function(ext) {
                //         return ext.match(/(doc|docx)$/i);
                //     },
                //     'xls': function(ext) {
                //         return ext.match(/(xls|xlsx)$/i);
                //     },
                //     'ppt': function(ext) {
                //         return ext.match(/(ppt|pptx)$/i);
                //     },
                //     'zip': function(ext) {
                //         return ext.match(/(zip|rar|tar|gzip|gz|7z)$/i);
                //     },
                //     'htm': function(ext) {
                //         return ext.match(/(htm|html)$/i);
                //     },
                //     'txt': function(ext) {
                //         return ext.match(/(txt|ini|csv|java|php|js|css)$/i);
                //     },
                //     'mov': function(ext) {
                //         return ext.match(/(avi|mpg|mkv|mov|mp4|3gp|webm|wmv)$/i);
                //     },
                //     'mp3': function(ext) {
                //         return ext.match(/(mp3|wav)$/i);
                //     },
                // }
            });
            // if(type=='input'){

            // console.log('ssss');

            $(this).on('fileuploaded', function (event, data, previewId, index) {
                $('#upload_pdf_name').val(data.response.OriginalFilename);
                $('#upload_pdf').val(data.response.filename);

            });

            $(this).on('filedeleted', function (event, data, previewId, index) {
                $('#upload_pdf_name').val('');
                $('#upload_pdf').val('');
            });
            // }
        })
    }
}


// function _docUpload($cont ,data) {
//     if (typeof($.fn.fileinput) != 'undefined') {
//         $cont = $($cont);
//         $cont.find(".doc-upload:not('.temp')").fileinput('destroy');
//         $cont.find(".doc-upload:not('.temp')").each(function () {
//             var data = $(this).attr('data-url') || '';
//             var title = $(this).attr('data-title') || '';
//             var initialPreview = [];
//             var initialPreviewConfig = [];
//             if (data != '') {
//                     initialPreview = [/*'<div class="file-preview-frame file-preview-success" id="uploaded-1482243555677" data-fileindex="-1" title="'+title+'" style="width:160px;height:160px;">'+*/
//                         '<object class="file-object" data="' + data + '" type="application/pdf" width="160px" height="160px" internalinstanceid="15">' +
//                         '<param name="movie" value="' + title + '">' +
//                         '<param name="controller" value="true">' +
//                         '<param name="allowFullScreen" value="true">' +
//                         '<param name="allowScriptAccess" value="always">' +
//                         '<param name="autoPlay" value="false">' +
//                         '<param name="autoStart" value="false">' +
//                         '<param name="quality" value="high">' +
//                         '<div class="file-preview-other">' +
//                         '<span class="file-icon-4x"><i class="icon-file-alt"></i></span>' +
//                         ' </div>' +
//                         ' </object>'];
//                 initialPreviewConfig = [{
//                     url: data,
//                     key: 100,
//                 }];
//             }
//             $(this).fileinput({
//                 maxFileCount: 1,
//                 showCaption: false,
//                 mainClass: " ",
//                 /*showUpload: false,*/
//                 uploadUrl: baseUrl + "upload-doc",
//                 previewFileType: "any",
//                 allowedFileExtensions: ["pdf"],//"txt", "md", "doc", "text", "docx", "xls",
//                 browseClass: "btn button button-3d button-mini button-rounded button-green ",
//                 browseLabel: pickdoc,
//                 browseIcon: "<i class=\"icon-picture\"></i> ",
//                 uploadClass: "btn button button-3d hide button-mini button-rounded button-blue ",
//                 removeClass: "btn button button-3d button-mini button-rounded button-red",
//                 removeLabel: del,
//                 removeIcon: "<i class=\"icon-trash\"></i> ",
//                 maxFileSize: 30000,
//                 initialPreview: initialPreview,
//                 initialPreviewConfig: initialPreviewConfig,
//                 initialPreviewShowDelete: false,
//                 autoReplace: true
//             }).on('fileloaded', function (event, file, previewId, index, reader) {
//                 if ($(event.currentTarget).parents('.file-input:first').find('.file-preview-frame').length > 1) $('.file-preview-frame:first .kv-file-remove').click();
//                 $('.fileinput-upload').click();
//             }).on('fileuploaded', function (event, data, previewId, index) {
//                 //$('.doc-upload-name').val(data.response.filename);
//                 $(event.currentTarget).parents('.file-input:first').prev().prev().val(data.response.filename);
//             }).on('fileclear', function (event) {
//                 //$('.doc-upload-name').val('');
//                 $(event.currentTarget).parents('.file-input:first').prev().prev().val('');
//             });
//         });
//         $cont.find(".doc-upload").change(function () {
//             $(this).parents('.form-group').find('label.error').remove();
//         });
//     }
//     /*else console.log('warning: fileinput is not defined');*/
// }

function _textEditor($cont) {
    $cont = $($cont);
    if (typeof ($.fn.ckeditor) !== 'undefined') {
        $cont.find('textarea.text-editor').each(function () {
            var params = {
                filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
                filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                contentsLangDirection: $(this).hasClass('ar') ? 'rtl' : $(this).hasClass('en') ? 'ltr' : '',
                language: lang
            };
            var $textarea = $(this).ckeditor(params);
            $textarea.editor.on('fileUploadRequest', function (evt) {
                evt.stop();
            });
            // CKFinder.setupCKEditor($textarea.editor);
            CKEDITOR.on('dialogDefinition', function (event) {
                filemanager_handleImageUpload(event, baseUrl, lang, $textarea.editor) // dialogDefinition
                //     // var dialog =event.data.name;
                //     // if (dialog.getName() === "embedBase" || dialog.getName() === "lightbox")
                //     //     dialog.setValueOf('info', 'url', a);
                //     // else{
                //     //     dialog.setValueOf('info', 'srcset', a,'imageresponsive');
                //     //     dialog.setValueOf('info', 'undefined', a);
                //     // }

            });

        });
    }
}

function filemanager_handleImageUpload(event, baseURL, Lang, pareEditor) {
    var ParentEditor = pareEditor;
    var editor = event.editor;
    var dialogDefinition = event.data.definition;
    var dialogName = event.data.name;
    var tabCount = dialogDefinition.contents.length;
    var iframe;
    for (var i = 0; i < tabCount; i++) {
        if (typeof (dialogDefinition.contents[i]) !== typeof (undefined)) {
            var browseButton = dialogDefinition.contents[i].get('browse');
            if (browseButton !== null) {
                browseButton.hidden = false;
                browseButton.onClick = function (dialog, i) {

                    CKFinder.setupCKEditor(ParentEditor, {skin: 'core'});
                    CKFinder.modal({
                        chooseFiles: true,
                        width: 800,
                        height: 600,
                        "z-index": 100000
                        // z-index:
                        // onInit: function( finder ) {
                        //     finder.on( 'files:choose', function( evt ) {
                        //         var file = evt.data.files.first();
                        //         var output = document.getElementById( elementId );
                        //         output.value = file.getUrl();
                        //     } );
                        //
                        //     finder.on( 'file:choose:resizedImage', function( evt ) {
                        //         var output = document.getElementById( elementId );
                        //         output.value = evt.data.resizedUrl;
                        //     } );
                        // }
                    });

                };
            }
        }

    }


}

function _ajaxreqs($cont, inCallback) {
    $cont = $($cont);
    $cont.find('.ajax-req:not("override")').each(function () {
        var url = $(this).attr('data-href');
        //$(this).attr('href','javascript:void(0)');
        var method = $(this).attr('data-method') || 'post';
        var reload = $(this).attr('data-reload') || false;
        var req = $(this).attr('data-data') || '';
        var callback = inCallback || $(this).attr('data-callback') || '';
        if (method === "delete" || method === "put") {
            req += "&_method=" + method;
            method = "post";
        }
        $(this).unbind('click').click(function (e) {
            $.ajax({
                type: method,
                url: url,
                data: /*"_token="+_csrf+"&"+*/req,
                success: function (data) {
                    if (callback != null) {
                        var fn = window[callback];
                        if (typeof fn === "function")
                            fn(data, req);
                    }
                    if (null != data.msg) {
                        SEMICOLON.widget.notifications($('<div id="" data-notify-position="top-right" data-notify-type="' + ((data.success) ? "info" : "error") + '" data-notify-msg="<i class=\'icon-info-sign   \'></i> <span class=\'\'> ' + data.msg + '</span>"></div>'));
                    }
                    if (null != data.redirect) {
                        window.location = data.redirect;
                    }
                    if (reload) {
                        window.location.reload();
                    }
                },
                /*headers: {
                 'X-CSRF-Token': _csrf
                 },*/
                dataType: "json"
            });
        });
        $(this).removeAttr('data-href');
    });
}

function _select2($cont, onSelectFunc) {
    if (typeof ($.fn.select2) != 'undefined') {
        $cont = $($cont);
        $.fn.select2.defaults.set("theme", "bootstrap");
        $cont.find(".autocomplete:not('.temp')").each(function () {
            __initAutocomplete(this, onSelectFunc, '')
        });
    }
    /*else console.log('warning: select2 is not defined');*/
}

function __initAutocomplete(obj, onSelectFunc, options) {
    var $this = $(obj);
    var data = options || {};
    if ($this.find('option:selected').length == 1 && !$this.prop('multiple'))
        data = [{id: $this.find('option:selected').val(), name: $this.find('option:selected').text()}];
    else

        $this.find('option:selected').each(function (i) {
            var $this = $(this);
            data[i] = {id: $this.val(), name: $this.text()};
        });
    var url = $this.data('remote');
    var required = (typeof $this.data('required') !== typeof undefined) ? $this.data('required') : null;
    var placeholder = $this.data('placeholder') ? $this.data('placeholder') : '';
    var letters = (typeof $this.data('letter') !== typeof undefined) ? $this.data('letter') : 3;
    var linkWith = $this.attr('data-param') || '';
    if (linkWith.charAt(0) == '#') {
        $(linkWith).change(function () {
            $this.val('').change();
        });
    }
    if ($this.hasClass('select2-hidden-accessible')) $this.select2("destroy");
    $this.select2({
        ajax: {
            url: url,
            dataType: 'json',
            delay: 400,
            data: function (params) {
                var param = (typeof $this.attr('data-param') !== typeof undefined) ? $this.attr('data-param') : null;
                if (param && param.charAt(0) === '#') {
                    var name = $(param).attr('name') || $(param).attr('id');
                    param = JSON.parse('{"' + name + '":"' + $(param).val() + '"}');
                } else if (param)
                    param = JSON.parse('{"' + param.replaceAll("&", "\",\"").replaceAll("=", "\":\"") + '"}');
                /*if(param && param.charAt(0) === '.') {

                 }*/
                var $data = {q: params.term, page: params.page};
                if (param) {
                    $data = $.extend($data, param);
                }
                return $data;
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: true
        },
        // escapeMarkup: function (markup) {
        //     return markup;
        // },
        dir: dir,
        minimumInputLength: letters,
        placeholder: placeholder,
        allowClear: true,
        // templateResult: __Select2_formatRepo,
        // templateSelection: __Select2_formatRepoSelection,
        dropdownParent: $this.parent(),
        data: data,
        // tags:true
    }).on("select2:select", function (e) {
        $(this).parent().removeClass('has-error');
        $(this).parent().find('label.error').remove();
        if (onSelectFunc != null) {
            var fn = window[onSelectFunc];
            if (typeof fn === "function")
                fn(this);
        }
    }).on("select2:unselect", function (e) {
        if ($(this).hasClass('required')) $(this).parent().addClass('has-error');
        $(this).append('<option value="null" selected>null</option>')
    });

}

//Select2 helpers
var __Select2_formatRepo = function (repo) {
    if (repo.loading)
        return repo.text;
    var markup = '<div class="clearfix">' + '<div class="col-sm-1">' + '</div>'
        + '<div clas="col-sm-10">' + '<div class="clearfix">' + repo.text
        + '</div>';
    markup += '</div></div>';
    return markup;
};

var __Select2_formatRepoSelection = function (repo) {

    var repoText = repo.text || repo.name;
    var $option = $(repo.element);
    for (var key in repo) {
        if (key.startsWith('data-')) {
            $option.attr(key, repo[key]);
            //$option.data('type')
        }
    }
    return repoText;

};

function _datePicker($cont) {
    if (typeof ($.fn.datetimepicker) != 'undefined') {
        $cont = $($cont);
        $cont.find(".datepicker").each(function () {
            $(this).datetimepicker({locale: lang, format: 'YYYY-MM-DD', useCurrent: false});
            var linkWith = $(this).attr('min-date') || '';
            if (linkWith.charAt(0) == '#') {
                $(linkWith).on("dp.change", function (e) {
                    $(this).data("DateTimePicker").minDate(e.date);
                });
            }
        });
    }
    /*else console.log('warning: datetimeepicker is not defined');*/
}

function _dateTimePicker($cont) {
    if (typeof ($.fn.datetimepicker) != 'undefined') {
        $cont = $($cont);
        $cont.find(".datetime").each(function () {
            $(this).datetimepicker({locale: lang, format: 'DD/MM/YYYY HH:mm', useCurrent: false});
            var linkWith = $(this).attr('min-date') || '';
            if (linkWith.charAt(0) == '#') {
                $(linkWith).on("dp.change", function (e) {
                    $(this).data("DateTimePicker").minDate(e.date);
                });
            }
        });
    }
    /*else console.log('warning: datetimeepicker is not defined');*/
}

function _timePicker($cont) {
    if (typeof ($.fn.datetimepicker) !== 'undefined') {
        $cont = $($cont);
        $cont.find(".time").each(function () {
            $(this).datetimepicker({
                locale: lang, format: 'HH:mm', useCurrent: false, icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                }
            });
            var linkWith = $(this).attr('min-date') || '';
            if (linkWith.charAt(0) === '#') {
                $(linkWith).on("dp.change", function (e) {
                    $(this).data("DateTimePicker").minDate(e.date);
                });
            }
        });
    }
    /*else console.log('warning: datetimeepicker is not defined');*/
}

String.prototype.replaceAll = function (search, replaceAllment) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replaceAllment);
};

function _bar_chart(labels, data, color, name) {
    var ctx = document.getElementById("bar-chart");
    var myLineChart;

    if (ctx === null || ctx === '' || ctx === undefined) {
        console.log('no canvas here');
    } else {
        myLineChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: labels ? labels : ["customers", "requests", "page_printed", "print_operations", "planted_trees"],
                datasets: [{
                    label: name ? name : 'title',
                    data: data ? data : [120, 119, 53, 53, 25, 33],
                    // backgroundColor:
                    //     color ? color : '#472833'
                    // ,
                    borderColor:
                        color ? color : '#472833'
                    ,
                    borderWidth: 1,
                    fill: false
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }],
                    xAxes: [{
                        barPercentage: 0.4
                    }]

                },
                title: {
                    display: true
                    // text: 'Predicted Students (millions) in 2050'
                }

            }
        });
    }
}

function _lineChart(labels, data, color, name) {
    // var co=color;
    var datum = [], label = [];
    var labels = labels ? labels : ['x', '2016-03-01', '2016-04-01', '2016-05-01', '2016-06-01', '2016-07-01', '2016-08-01', '2016-09-01', '2016-10-01', '2016-11-01'];
    var data = data ? data : ['data1', 24600, 27900, 29200, 32200, 37300, 41500, 42950, 43100, 42900, 43000];
    if (data !== undefined || data !== '' || data !== null) {
        $.each(data, function (i, v) {
            datum.push(v.replace('"', ''));
        });
    }
    if (labels !== undefined || labels !== '' || labels !== null) {
        $.each(labels, function (i, v) {
            label.push(v.replace(/"/g, "'"));
        });
    }

    if (data === undefined || data === '' || data === null) {
        console.log('no d3');
    } else {
        var lineChart,
            lineChartObject = {
                bindto: '#line-chart',
                color: {
                    pattern: color ? [color] : '#a820d3'
                },
                point: {
                    show: false,
                    r: 4
                },
                padding: {
                    left: 50,
                    right: 30,
                    top: 0,
                    bottom: 0
                },
                data: {
                    x: 'x',
                    columns: [
                        label ? label : ['x', '2016-03-01', '2016-04-01', '2016-05-01', '2016-06-01', '2016-07-01', '2016-08-01', '2016-09-01', '2016-10-01', '2016-11-01'],
                        datum ? datum : ['data1', 24600, 27900, 29200, 32200, 37300, 41500, 42950, 43100, 42900, 43000]
                        // ['data2', 22900, 25600, 26800, 28200, 32700, 36200, 37500, 36700, 35100, 33700]
                    ],
                    axes: {
                        data1: 'y'
                    },
                    type: 'spline',
                    names: {
                        data1: name ? name : 'Statistics'
                        // data2: 'Expenses'
                    }
                },
                legend: {
                    show: true,
                    position: 'bottom'
                },
                grid: {
                    x: {
                        show: true
                    },
                    y: {
                        show: false
                    }
                },
                labels: true,
                axis: {
                    x: {
                        type: 'timeseries',
                        min: '2014-02-01',
                        tick: {
                            format: '%b %Y',
                            outer: false
                        },
                        padding: {
                            left: 0,
                            right: 10
                        }
                    },
                    y: {
                        min: 0,
                        max: 1400,
                        label: {
                            text: 'unit',
                            position: 'inner-top'
                        },
                        tick: {
                            format: function (x) {
                                return x;
                            },
                            outer: false
                        },
                        padding: {
                            top: 0,
                            bottom: 0
                        }
                    }
                },
                line: {
                    connectNull: true
                },
                oninit: wrapLabels(),
                onrendered: wrapLabels(),
                onresized: wrapLabels(),
                onmouseout: wrapLabels()
            };
        lineChart = c3.generate(lineChartObject);
        wrapLabels();
        d3.select('.d3-chart-wrap').insert('div', '.d3-chart + *').attr('class', 'd3-chart-legend').selectAll('span')
            .data(['data1'])
            .enter().append('span')
            .attr('data-id', function (id) {
                return id;
            })
            .html(function (id) {
                return lineChartObject.data.names[id] ? lineChartObject.data.names[id] : id;
            })
            .on('mouseover', function (id) {
                lineChart.focus(id);
            })
            .on('mouseout', function (id) {
                lineChart.revert();
            });
    }
}

function wrapLabels() {
    d3.select('#line-chart').selectAll(".c3-axis-x .tick text")
        .attr('dy', '0.5em')
        .call(wrap, 30);
}

function wrap(text, width) {
    text.each(function () {
        var text = d3.select(this);
        if (text.selectAll('tspan').size() > 1) return;

        var words = text.text().split(/\s+/).reverse(),
            word,
            line = [],
            lineNumber = 0,
            lineHeight = 1.2, // ems
            y = text.attr("y"),
            dy = parseFloat(text.attr("dy")),
            tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");

        while (word = words.pop()) {
            line.push(word);
            tspan.text(line.join(" "));
            if (tspan.node().getComputedTextLength() > width) {
                line.pop();
                tspan.text(line.join(" "));
                line = [word];
                tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
            }
        }
    });
}
