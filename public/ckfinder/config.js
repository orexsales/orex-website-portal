/*
 Copyright (c) 2007-2017, CKSource - Frederico Knabben. All rights reserved.
 For licensing, see LICENSE.html or http://cksource.com/ckfinder/license
 */

var config = {};

// Set your configuration options below.

// Examples:
// config.language = 'pl';
config.skin = 'jquery-mobile';
// config.enable = 'true';

CKFinder.define(config);
// CKFinder.define({
//     plugins: 'MyPlugin'
// });
// CKFinder.define({
//
//     init: function (finder) {
//         finder.on('app:ready', function (evt) {
//             finder.request('dialog:info', {
//                 msg: 'Welcome to CKFinder!'
//             });
//         });
//         console.log(finder)
//         finder.on('file:dblclick', function (evt) {
//             alert('yes db Clicked')
//         });
//     }
// });
// CKFinder.popup( {
//     onInit: function( finder ) {
//         finder.request( 'dialog:info', { msg: 'It works!' } );
//     }
// } );
