<?php
    extract($gridSystemResult);
?>

<div class='form-group <?php echo e($global); ?>' style="display:block;float: <?php if($attr): ?><?php echo e($left); ?><?php endif; ?>">
    <?php if(!preg_match('/\b(?<![\S])(noLabel)(?![\S])\b/',$class)): ?>
        <label class='<?php echo e($label); ?> control-label' for='<?php echo e($name); ?>'><?php echo e($title); ?> <?php if($star): ?> <span class='text-danger'>*</span> <?php endif; ?> </label>
    <?php endif; ?>
    <div class='<?php echo e($input); ?>'>
        <textarea id='<?php echo e($id); ?>'
                  data-datavalue='<?php echo e($data); ?>'
                  name='<?php echo e($name); ?>'
                  placeholder='<?php echo e($title); ?>'
                  class='form-control <?php echo e($class); ?>'
                  style='resize:vertical;'
                  data-editable = 'true'
                  <?php echo e($attr); ?>

        ></textarea>
        <div id='error_<?php echo e($id); ?>'></div>
    </div>
</div><?php /**PATH D:\UPWork Project\Orex\portal.orex\vendor\Aut\DataTable\src/Resources/Views/component/textArea.blade.php ENDPATH**/ ?>