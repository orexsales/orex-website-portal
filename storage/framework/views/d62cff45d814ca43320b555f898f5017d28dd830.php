<?php
    $id='product_id';
    $imageWidth=null;
    $imageHeight=null;
    $imageMainWidth=760;
    $imageMainHeight=500;

?>
<div class="modal fade" id="modal-image" tabindex="-1" aria-hidden="true" style="background-color: #464a4ca3">
    <div class="modal-dialog" style="min-width: 75%">
        <div class="modal-content">
            <?php echo Form::open(['class'=>'ajax-form','method'=>'put','id'=>'upload-image']); ?>

            <div class="modal-header">
                <h4 class="modal-title"><?php echo app('translator')->getFromJson('app.update_image'); ?></h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only"><?php echo app('translator')->getFromJson('app.close'); ?></span></button>
            </div>
            <div class=" modal-body clearfix margin-sm">

                <div class="image-upload-cont">
                    <input id="input-703" data-type="image" data-size="true" data-width="760" data-height="500" class="image-uploade" name="image[]" type="file" multiple>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-outline-success btn--icon-text waves-effect" data-dismiss="modal"><i
                            class="zmdi zmdi-thumb-up"></i> <?php echo e(trans('app.sweet.ok')); ?></button>
                
            </div>
            <?php echo Form::close(); ?>

        </div>
    </div>
</div>
<div class="modal fade" id="modal-video" tabindex="-1" aria-hidden="true" style="background-color: #464a4ca3">
    <div class="modal-dialog" style="min-width: 75%">
        <div class="modal-content">
            <?php echo Form::open(['class'=>'ajax-form','method'=>'put','id'=>'upload-image']); ?>

            <div class="modal-header">
                <h4 class="modal-title"><?php echo app('translator')->getFromJson('app.upload_video'); ?></h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only"><?php echo app('translator')->getFromJson('app.close'); ?></span></button>
            </div>
            <div class=" modal-body clearfix margin-sm">

                <div class="image-upload-cont">
                    <?php echo Form::bsImageUpload('col_full','video',false,'',true,null,null,null,null,null,'video'); ?>

                    
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-outline-success btn--icon-text waves-effect" data-dismiss="modal"><i
                            class="zmdi zmdi-close"></i> <?php echo e(trans('app.close')); ?></button>
                <button type="submit" class="btn btn-outline-danger btn--icon-text waves-effect"><i class="zmdi zmdi-plus"></i> <?php echo e(trans('app.save')); ?></button>
            </div>
            <?php echo Form::close(); ?>

        </div>
    </div>
</div>
<div class="modal fade" id="modal-main-image" tabindex="-1" aria-hidden="true" style="background-color: #464a4ca3">
    <div class="modal-dialog" style="min-width: 75%">
        <div class="modal-content">
            <?php echo Form::open(['class'=>'ajax-form','method'=>'put']); ?>

            <div class="modal-header">
                <h4 class="modal-title"><?php echo app('translator')->getFromJson('app.update_image'); ?></h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only"><?php echo app('translator')->getFromJson('app.close'); ?></span></button>
            </div>
            <div class=" modal-body clearfix margin-sm">
                <?php echo Form::thImageUpload('col_full','image',false,'',true,null,null,'','','','image',$imageMainWidth!==null?'data-width='.$imageMainWidth.' data-height='.$imageMainHeight.' data-size=true':''); ?>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-outline-success btn--icon-text waves-effect" data-dismiss="modal"><i
                            class="zmdi zmdi-close"></i> <?php echo e(trans('app.close')); ?></button>
                <button type="submit" class="btn btn-outline-danger btn--icon-text waves-effect"><i class="zmdi zmdi-plus"></i> <?php echo e(trans('app.save')); ?></button>
            </div>
            <?php echo Form::close(); ?>

        </div>
    </div>
</div>
<div class="modal fade" id="modal-upload-pdf" data-url="<?php echo e(localizeURL('admin/get-files')); ?>" style="z-index: 199999999">
    <div class="modal-dialog" style="min-width: 75%">
        <div class="modal-content">
            <?php echo Form::open(['class'=>'ajax-form','method'=>'put']); ?>

            <div class="modal-header">
                <h4 class="modal-title"><?php echo app('translator')->getFromJson('app.upload_doc'); ?></h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only"><?php echo app('translator')->getFromJson('app.close'); ?></span></button>
            </div>
            <div class=" modal-body clearfix ">
                <div class="form-group">
                    <input class="doc-upload" name="doc" type="file" data-type="input"
                           data-url="<?php echo e(localizeURL('admin/get-files')); ?>" multiple>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-success btn--icon-text waves-effect" data-dismiss="modal">
                    <i class="icon-close"></i><span> <?php echo app('translator')->getFromJson('app.close'); ?></span></button>
            </div>
            <?php echo Form::close(); ?>

        </div>
    </div>
</div>

<script>
    <?php if($imageMainWidth!==null): ?>
    $(function () {
        $('#modal-main-image').on('show.bs.modal', function () {
            _imageCropUpload($(this),$(this).attr('data-name'));
        });
    });
    <?php endif; ?>
    function admin_update_image($this) {
        var obj = _aut_datatable_getSelectedRow(_aut_datatable_getTableObjectApi('#<?php echo e($table); ?>'), $($($this).closest('tr')));
        var $imageUrl = $(obj.image).attr('src');
        if (typeof ($imageUrl) === typeof (undefined))
            $imageUrl = $(obj.image).find('img').attr('src');
        var $modal = $('#modal-image');
        var name=obj.name_en.toLowerCase().split(' ').join('_').split('/').join('-').split('(').join('').split(')').join('');
        $modal.find('form').attr('action', '<?php echo e(localizeURL('admin/'.$table)); ?>/' + obj['<?php echo e($id); ?>']);
        <?php if($imageWidth===null): ?>
        $modal.find('#image').attr('data-url', $imageUrl)/*.attr('data-title',obj.template_en)*/;
        $.ajax({
            type:'GET',
            url:'<?php echo e(localizeURL('admin/get-images')); ?>',
            data:{file:obj.name_en.toLowerCase().split(' ').join('_').split('(').join('').split(')').join(''),table:'<?php echo e($table); ?>'},
            success:function (data) {
             _imageUpload($modal,name,'<?php echo e($table); ?>', data.files, data.config);
            }
        });
        <?php else: ?>
        $modal.find('.image-crop-upload').attr('src', $imageUrl)/*.attr('data-title',obj.template_en)*/;
        <?php endif; ?>
        _validate($modal, 'admin_reloadGrid');
        // $modal.find('#image-name').val($imageUrl.split('/').pop());
        $modal.modal({keyboard: false}).modal('show');
        $('form').on('submit', function () {
            aut_datatable_refresh('.card', true);
            $modal.modal('hide');
        });
    }
    function admin_upload_video($this) {
        var obj=_aut_datatable_getSelectedRow(_aut_datatable_getTableObjectApi('#<?php echo e($table); ?>'),$($($this).closest('tr')));
        var $videoUrl=$(obj.video).attr('href');
        if(typeof ($videoUrl)=== typeof (undefined))
            $videoUrl=$(obj.image).attr('href');
        var $modal =$('#modal-video');
        var $name=obj.name_en.toLowerCase().split(' ').join('_').split('/').join('-').split('(').join('').split(')').join('');;

        $modal.find('form').attr('action','<?php echo e(localizeURL('admin/'.$table)); ?>/'+obj['<?php echo e($id); ?>']);
        $modal.find('#video').attr('data-url', $videoUrl);
        $modal.find('#video-name').val($videoUrl);

        $.ajax({
            type:'GET',
            url:'<?php echo e(localizeURL('admin/get-video-product')); ?>',
            data:{id:obj['<?php echo e($id); ?>']},
            success:function (data) {
                _videoUpload($modal,$name,'<?php echo e($table); ?>',data.url,data.configs);
            }
        });
        _validate($modal,'admin_reloadGrid');
        $modal.modal('show');
        $('form').on('submit', function () {
            aut_datatable_refresh('.card', true);
            $modal.modal('hide');
        });
    }
    function admin_update_main_image($this) {
        var obj = _aut_datatable_getSelectedRow(_aut_datatable_getTableObjectApi('#<?php echo e($table); ?>'), $($($this).closest('tr')));
        var $imageUrl = $(obj.image).attr('src');
        if (typeof ($imageUrl) === typeof (undefined))
            $imageUrl = $(obj.image).find('img').attr('src');
        var $modal = $('#modal-main-image');
        $modal.attr('data-name',obj.name_en.toLowerCase().split(' ').join('_').split('/').join('-').split('(').join('').split(')').join(''));
        $modal.find('form').attr('action', '<?php echo e(localizeURL('admin/'.$table)); ?>/' + obj['<?php echo e($id); ?>']);
        <?php if($imageMainWidth===null): ?>
            $modal.find('#image').attr('data-url', $imageUrl);
            _imageUpload($modal,name);
        <?php else: ?>
            $modal.find('.image-crop-upload').attr('src', $imageUrl);
        <?php endif; ?>
        _validate($modal, 'admin_reloadGrid');
        $modal.find('#image-name').val($imageUrl);
        $modal.modal({keyboard: false}).modal('show');
        $('form').on('submit', function () {
            aut_datatable_refresh('.card', true);
            $modal.modal('hide');
        });
    }
    function _open_file_upload($this) {
        var obj = _aut_datatable_getSelectedRow(_aut_datatable_getTableObjectApi('#<?php echo e($table); ?>'), $($($this).closest('tr')));
        var $modal = $('#modal-upload-pdf');
        $modal.modal('show');
        var file = obj.name_en.split(' ').join('_').toLowerCase().split('.').join('_').split('/').join('-').split('(').join('').split(')').join('');
        var url = $modal.data('url');
        $.ajax({
            type: 'GET',
            url: url,
            data: {file: file},
            success: function (files) {
                _docUpload($modal, file, files.files, files.config);
            }
        })
    }
    function admin_reloadGrid() {
        aut_datatable_reloadTable(_aut_datatable_getTableObjectApi('#<?php echo e($table); ?>'), null, false, false)
    }

</script>

<style>
    .image-crop-upload{
        width:<?php echo e($imageMainWidth); ?>px;
    }
</style>

<?php if($imageMainWidth!==null): ?>
    <?php echo $CSS['cropper']; ?>

    <?php echo $JS['cropper']; ?>

<?php endif; ?>

<?php /**PATH D:\UPWork Project\Orex\portal.orex\Modules\Admin\Providers/../Resources/views/table-modal/products-extra.blade.php ENDPATH**/ ?>