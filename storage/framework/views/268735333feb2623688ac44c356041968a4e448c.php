<?php
    extract($gridSystemResult);
?>

<div class='form-group <?php echo e($global); ?>'  style="display:block;float: <?php echo e($dir=='rtl'?'right':'left'); ?>">
    <label class='<?php echo e($label); ?> control-label' for='<?php echo e($name); ?>'><?php echo e($title); ?> <?php if($star): ?> <span class='text-danger'>*</span> <?php endif; ?> </label>
    <div class='<?php echo e($input); ?>'>
        <input id='<?php echo e($id); ?>'
               data-datavalue='<?php echo e($data); ?>'
               name='<?php echo e($name); ?>'
               type='<?php echo e($type); ?>'
               placeholder='<?php echo e($title); ?>'
               class='form-control <?php echo e($class); ?>'
               data-editable = 'true'
               <?php echo e($value ? "value=$value" : ''); ?>

               <?php echo e($attr); ?>

        >
        <div id='error_<?php echo e($id); ?>'></div>
    </div>
</div><?php /**PATH D:\UPWork Project\Orex\portal.orex\vendor\Aut\DataTable\src/Resources/Views/component/input.blade.php ENDPATH**/ ?>