<?php $__env->startSection('title',Auth::user()->name); ?>
<?php $__env->startSection('content'); ?>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    <div class="row">
        <div class="col-xl-4 offset-4 order-xl-2 mb-5 mb-xl-0">
            <div class="card card-profile shadow">
                <div class="row justify-content-center">
                    <div class="col-lg-3 order-lg-2">
                        <div class="card-profile-image">
                            <a href="#">
                                <img id="image-view"
                                     src="<?php echo e(isset(Auth::user()->image)?asset(\App\User::IMAGE_FILE.Auth::user()->image):asset('images/avatar.jpg')); ?>"
                                     data-image-name="<?php echo e(isset(Auth::user()->image)?asset(\App\User::IMAGE_FILE.Auth::user()->image):asset('images/avatar.jpg')); ?>"
                                     class="rounded-circle user-image">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                    <div class="d-flex justify-content-between">
                        <a href="#" class="btn btn-sm btn-info mr-4" data-toggle="modal"
                           data-backdrop="static" data-target="#pr-image-modal"><?php echo e(trans('app.upload_image')); ?></a>
                    </div>
                </div>
                <br>
                <br>
                <div class="card-body pt-0 pt-md-4">
                    <div class="text-center">
                        <form id="profile-form" action="<?php echo e(localizeURL('admin/profile')); ?>" enctype="multipart/form-data"
                              method="post">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="input-group input-group-alternative mb-4">
                                        <input class="form-control" value="<?php echo e(Auth::user()->name); ?>" name="name"
                                               placeholder="<?php echo e(trans('profile.username')); ?>" type="text">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="ni ni-circle-08"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="input-group input-group-alternative mb-4">
                                        <input class="form-control" value="<?php echo e(Auth::user()->email); ?>" name="email"
                                               placeholder="<?php echo e(trans('profile.email')); ?>" type="email">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="input-group input-group-alternative mb-4">
                                        <input class="form-control" name="password"
                                               placeholder="<?php echo e(trans('profile.password')); ?>" type="password">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="ni ni-key-25"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit"
                                        class="btn btn-outline-primary waves-effect"><?php echo e(trans('app.save')); ?></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo $__env->make('admin::modals', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('styles'); ?>
    <?php echo $CSS['bs-editable']; ?>

    <link rel="stylesheet" href="<?php echo e(asset('general/css/toastr.css')); ?>" type="text/css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <?php echo $JS['bs-editable']; ?>

    <?php echo $JS['cropper']; ?>

    <script src="<?php echo e(asset('general/js/toastr.js')); ?>"></script>
    <script>
        $(function () {
            $('#pr-image-modal').on('show.bs.modal', function (event) {
                pr_handleShowModal(this);
            });
            $('#profile-form').ajaxForm({
                data: $(this).serialize()/*+'&_method=put'*/,
                complete: function (data) {
                    toastr.info(data.responseJSON.msg);
                }
            });
        });

        function pr_handleShowModal(obj) {
            var $this = $(obj);
            $this.find('#image-name').val($('#image-view').attr('data-image-name'));
            $this.find('.image-crop-upload').attr('src', $('#image-view').attr('src'));
            _imageCropUpload($this, '<?php echo e(str_replace(' ','_',Auth::user()->name)); ?>');
            _validate(obj, 'pr_handleImageSelection');
        }

        function pr_handleImageSelection() {
            $('.user-image').attr('src', $('#pr-image-modal').find('img').attr('src'));
            $('.user__img').attr('src', $('#pr-image-modal').find('img').attr('src'));
        }
    
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin::layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\UPWork Project\Orex\portal.orex\Modules\Admin\Providers/../Resources/views/profile.blade.php ENDPATH**/ ?>