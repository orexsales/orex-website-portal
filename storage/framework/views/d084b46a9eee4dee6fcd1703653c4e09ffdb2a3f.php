<?php $__env->startSection('title',trans('app.'.$table)); ?>
<?php $__env->startSection('content'); ?>
        <div class="row">
            <div class="col-md-11 col-lg-offset-1">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <h2 class=""><?php echo e(trans('app.'.$table)); ?></h2>
                    </div>

                    <div class="table-responsive" style="padding: 0;font-size: 15px">
                        <?php $params=\Request::getQueryString() ? ('?' . \Request::getQueryString()) : ''; ?>
                        <?php echo datatable($table,$params); ?>

                    </div>
                </div>
            </div>

        </div>
        <div id="ckfinder-widget"></div>
    <?php if(view()->exists('admin::table-modal.'.$table.'-extra')): ?>
        <?php echo $__env->make('admin::table-modal.'.$table.'-extra', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('styles'); ?>
    <?php echo $CSS['select2']; ?>

    <?php echo $CSS['bs-editable']; ?>

    <?php echo $CSS['bs-filestyle']; ?>

    <?php echo $CSS['datatable']; ?>

    <?php echo $CSS['select-picker']; ?>

    <?php echo $CSS['timepicker']; ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <script src="<?php echo e(asset('ckfinder/ckfinder.js')); ?>"></script>
    <?php echo $JS['select2']; ?>

    <?php echo $JS['bs-editable']; ?>

    <?php echo $JS['bs-filestyle']; ?>

    <?php echo $JS['datatable']; ?>

    <?php echo $JS['select-picker']; ?>

    <?php echo $JS['timepicker']; ?>

    <?php echo $JS['ckeditor']; ?>

    <script src="<?php echo e(asset('admin/js/buttons.html5.js')); ?>"></script>
    <script src="<?php echo e(asset('admin/js/buttons.print.js')); ?>"></script>
<script>

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\UPWork Project\Orex\portal.orex\Modules\Admin\Providers/../Resources/views/table.blade.php ENDPATH**/ ?>