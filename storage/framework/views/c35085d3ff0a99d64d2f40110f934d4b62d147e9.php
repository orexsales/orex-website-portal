<div class="modal fade" id="pr-image-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <?php echo Form::open(['class'=>'form-horizontal ajax-form   nobottommargin','url'=>localizeURL('admin/profile'),'method'=>'put']); ?>

            <div class="modal-header">
                <h4 class="modal-title" id="edu-modal-label"><?php echo app('translator')->getFromJson('profile.change_profile_portriat'); ?></h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?php echo app('translator')->getFromJson('app.close'); ?></span></button>
            </div>
            <div class="modal-body">
                <div class="clearfix">
                    <?php echo e(Form::thImageUpload('col_full','image',false,'',true,'image',null,'','','','image','data-width="'.Auth::user()->IMAGE_WIDTH.'" data-height="'.Auth::user()->IMAGE_HEIGHT.'"')); ?>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger " data-dismiss="modal"><?php echo app('translator')->getFromJson('app.close'); ?></button>
                <button type="submit" class="btn btn-outline-success"><?php echo app('translator')->getFromJson('app.save'); ?></button>
            </div>
            <?php echo Form::close(); ?>

        </div>
    </div>
</div>
<?php /**PATH D:\UPWork Project\Orex\portal.orex\Modules\Admin\Providers/../Resources/views/modals.blade.php ENDPATH**/ ?>