<!-- This Component Support Upload Image or Video and Cropping Image-->
<?php $name=isset($name)?$name:$id?>
<div class="<?php echo e($contClass); ?> image-upload-cont">
    <?php if($label!==false): ?><?php echo Form::label($id,' '); ?><?php endif; ?>
    <span class=" show-error-msg " id="error-<?php echo e($id); ?>-name"></span>
    <?php echo Form::label($id.'-name',$errors->has($name)?$errors->first($name):' ',['id'=>$id.'-name-error','class'=>$class.' error f'.$right]); ?>

    <input data-msg="<?php echo app('translator')->getFromJson('validation.custom.select_image'); ?>" id="<?php echo e($id); ?>-name" name="<?php echo e($name); ?>" value="<?php echo e($fileName); ?>" type="text"  class="image-upload-name <?php echo e(($required==true)?' required':''); ?> hide">
    <div class="clear"></div>
    <input <?php echo $attr; ?> data-url="<?php echo e($fileUrl); ?>" data-type="<?php echo e($type); ?>" id="<?php echo e($id); ?>" name="image" data-title="<?php echo e($fileName); ?>" type="file"  class="<?php echo e(strpos($attr,'data-width')!==false?'hide':'image-upload'); ?>" <?php if($type=='image'): ?> accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff" <?php endif; ?>>
    <?php if(strpos($attr,'data-width')!==false): ?>
        <button type="button" class="choose-action button button-3d button-small button-green f<?php echo e($left); ?>" style=" z-index: 100;"><i class="icon-upload"></i> <?php echo app('translator')->getFromJson('app.pick_image'); ?></button>
        <button type="button" class="crop-action button button-3d button-small f<?php echo e($left); ?>" style=" z-index: 100;"><i class="icon-crop"></i> <?php echo app('translator')->getFromJson('app.crop'); ?></button>
        <img  <?php echo $attr; ?> class="image-crop-upload" src="<?php echo e($fileUrl); ?>" style="width: 100%;">
    <?php endif; ?>
</div>
<?php /**PATH D:\UPWork Project\Orex\portal.orex\resources\views/components/form/bootstrap/image-upload.blade.php ENDPATH**/ ?>