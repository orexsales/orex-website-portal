<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main"
                aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Brand -->
        <a class="navbar-brand pt-0" href="/">
            <img src="<?php echo e(asset('images/Logo1.png')); ?>" class="navbar-brand-img" alt="..." style="height: 100px;">
        </a>
        <!-- User -->
        <ul class="nav align-items-center d-md-none">
            <li class="nav-item dropdown">
                <a class="nav-link  nav-link -icon" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false">
                    <i class="ni ni-bell-55"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right"
                     aria-labelledby="navbar-default_dropdown_1">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link " href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false">
                    <div class="media align-items-center">
              <span class="avatar avatar-sm rounded-circle">
                <img alt="Image placeholder"
                     src="<?php echo e(isset(Auth::user()->image)?asset(\App\User::IMAGE_FILE.Auth::user()->image):asset('images/avatar.jpg')); ?>">
              </span>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div class=" dropdown-header noti-title">
                        <h6 class="text-overflow m-0">Welcome!</h6>
                    </div>
                    <a href="<?php echo e(localizeURL('admin/profile')); ?>" class="dropdown-item">
                        <i class="ni ni-single-02"></i>
                        <span><?php echo e(trans('profile.profile')); ?></span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="<?php echo e(localizeURL('logout')); ?>" class="dropdown-item">
                        <i class="ni ni-user-run"></i>
                        <span><?php echo e(trans('app.logout')); ?></span>
                    </a>
                </div>
            </li>
        </ul>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <!-- Collapse header -->
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="/">
                            <img src="<?php echo e(asset('images/Logo.png')); ?>">
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse"
                                data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false"
                                aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <!-- Form -->
            <form class="mt-4 mb-3 d-md-none">
                <div class="input-group input-group-rounded input-group-merge">
                    <input type="search" class="form-control form-control-rounded form-control-prepended"
                           placeholder="Search" aria-label="Search">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <span class="fa fa-search"></span>
                        </div>
                    </div>
                </div>
            </form>
            <!-- Navigation -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="" class="nav-link  dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false"><i
                            class="ni ni-settings-gear-65"></i> <?php echo e(trans('app.settings')); ?></a>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <li class="<?php if(URL::current()==route('admin-table',['table'=>'users'])): ?> active <?php endif; ?> nav-item">
                            <a href="<?php echo e(route('admin-table',['table'=>'users'])); ?>"
                               class="nav-link <?php if(URL::current()==route('admin-table',['table'=>'users'])): ?> active <?php endif; ?> "><?php echo e(trans('app.users')); ?></a>
                        </li>
                        <li class="<?php if(URL::current()==route('admin-table',['table'=>'generals'])): ?> active <?php endif; ?> nav-item">
                            <a href="<?php echo e(route('admin-table',['table'=>'generals'])); ?>"
                               class="nav-link <?php if(URL::current()==route('admin-table',['table'=>'generals'])): ?> active <?php endif; ?> "><?php echo e(trans('app.generals')); ?></a>
                        </li>
                        <li class="<?php if(URL::current()==route('admin-table',['table'=>'styles'])): ?> active <?php endif; ?> nav-item">
                            <a href="<?php echo e(route('admin-table',['table'=>'styles'])); ?>"
                               class="nav-link <?php if(URL::current()==route('admin-table',['table'=>'styles'])): ?> active <?php endif; ?> "><?php echo e(trans('app.styles')); ?></a>
                        </li>
                        <li class="<?php if(URL::current()==route('admin-table',['table'=>'counters'])): ?> active <?php endif; ?> nav-item">
                            <a href="<?php echo e(route('admin-table',['table'=>'counters'])); ?>"
                               class="nav-link <?php if(URL::current()==route('admin-table',['table'=>'counters'])): ?> active <?php endif; ?> "><?php echo e(trans('app.counters')); ?></a>
                        </li>
                        <li class="<?php if(URL::current()==route('admin-table',['table'=>'contact-mails'])): ?> active <?php endif; ?> nav-item">
                            <a href="<?php echo e(route('admin-table',['table'=>'contact-mails'])); ?>"
                               class="nav-link <?php if(URL::current()==route('admin-table',['table'=>'contact-mails'])): ?> active <?php endif; ?> "><?php echo e(trans('app.contact-mails')); ?></a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="javascript:void(0)" class="nav-link  dropdown-toggle" id="dropdownMenuButton1"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                            class="ni ni-settings"></i> <?php echo e(trans('app.settings_website')); ?></a>

                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                        <li class="<?php if(URL::current()==route('admin-table',['table'=>'products'])): ?> active <?php endif; ?> nav-item">
                            <a href="<?php echo e(route('admin-table',['table'=>'products'])); ?>"
                               class="nav-link <?php if(URL::current()==route('admin-table',['table'=>'products'])): ?> active <?php endif; ?> "><?php echo e(trans('app.products')); ?></a>
                        </li>
                        <li class="<?php if(URL::current()==route('admin-table',['table'=>'categories'])): ?> active <?php endif; ?> nav-item">
                            <a href="<?php echo e(route('admin-table',['table'=>'categories'])); ?>"
                               class="nav-link <?php if(URL::current()==route('admin-table',['table'=>'categories'])): ?> active <?php endif; ?> "><?php echo e(trans('app.categories')); ?></a>
                        </li>
                        <li class="<?php if(URL::current()==route('admin-table',['table'=>'questions'])): ?> active <?php endif; ?> nav-item">
                            <a href="<?php echo e(route('admin-table',['table'=>'questions'])); ?>"
                               class="nav-link <?php if(URL::current()==route('admin-table',['table'=>'questions'])): ?> active <?php endif; ?> "><?php echo e(trans('app.questions')); ?></a>
                        </li>
                        <li class="<?php if(URL::current()==route('admin-table',['table'=>'pages'])): ?> active <?php endif; ?> nav-item">
                            <a href="<?php echo e(route('admin-table',['table'=>'pages'])); ?>"
                               class="nav-link <?php if(URL::current()==route('admin-table',['table'=>'pages'])): ?> active <?php endif; ?> "><?php echo e(trans('app.pages')); ?></a>
                        </li>
                        <li class="<?php if(URL::current()==route('admin-table',['table'=>'attributes'])): ?> active <?php endif; ?> nav-item">
                            <a href="<?php echo e(route('admin-table',['table'=>'attributes'])); ?>"
                               class="nav-link <?php if(URL::current()==route('admin-table',['table'=>'attributes'])): ?> active <?php endif; ?> "><?php echo e(trans('app.attributes')); ?></a>
                        </li>
                        <li class="<?php if(URL::current()==route('admin-table',['table'=>'gallery'])): ?> active <?php endif; ?> nav-item">
                            <a href="<?php echo e(route('admin-table',['table'=>'gallery'])); ?>"
                               class="nav-link <?php if(URL::current()==route('admin-table',['table'=>'gallery'])): ?> active <?php endif; ?> "><?php echo e(trans('app.gallery')); ?></a>
                        </li>
                        <li class="<?php if(URL::current()==route('admin-table',['table'=>'achievments'])): ?> active <?php endif; ?> nav-item">
                            <a href="<?php echo e(route('admin-table',['table'=>'achievments'])); ?>"
                               class="nav-link <?php if(URL::current()==route('admin-table',['table'=>'achievments'])): ?> active <?php endif; ?> "><?php echo e(trans('app.achievments')); ?></a>
                        </li>
                        <li class="<?php if(URL::current()==route('admin-table',['table'=>'blogs'])): ?> active <?php endif; ?> nav-item">
                            <a href="<?php echo e(route('admin-table',['table'=>'blogs'])); ?>"
                               class="nav-link <?php if(URL::current()==route('admin-table',['table'=>'blogs'])): ?> active <?php endif; ?> "><?php echo e(trans('app.blogs')); ?></a>
                        </li>
                        <li class="<?php if(URL::current()==route('admin-table',['table'=>'rates'])): ?> active <?php endif; ?> nav-item">
                            <a href="<?php echo e(route('admin-table',['table'=>'rates'])); ?>"
                               class="nav-link <?php if(URL::current()==route('admin-table',['table'=>'rates'])): ?> active <?php endif; ?> "><?php echo e(trans('app.rates')); ?></a>
                        </li>
                        <li class="<?php if(URL::current()==route('admin-table',['table'=>'subscribes'])): ?> active <?php endif; ?> nav-item">
                            <a href="<?php echo e(route('admin-table',['table'=>'subscribes'])); ?>"
                               class="nav-link <?php if(URL::current()==route('admin-table',['table'=>'subscribes'])): ?> active <?php endif; ?> "><?php echo e(trans('app.subscribes')); ?></a>
                        </li>
                        <li class="<?php if(URL::current()==route('admin-table',['table'=>'opening-hours'])): ?> active <?php endif; ?> nav-item">
                            <a href="<?php echo e(route('admin-table',['table'=>'opening-hours'])); ?>"
                               class="nav-link <?php if(URL::current()==route('admin-table',['table'=>'opening-hours'])): ?> active <?php endif; ?> "><?php echo e(trans('app.opening_hours')); ?></a>
                        </li>
                        <li class="<?php if(URL::current()==route('admin-table',['table'=>'members'])): ?> active <?php endif; ?> nav-item">
                            <a href="<?php echo e(route('admin-table',['table'=>'members'])); ?>"
                               class="nav-link <?php if(URL::current()==route('admin-table',['table'=>'members'])): ?> active <?php endif; ?> "><?php echo e(trans('app.members')); ?></a>
                        </li>
                        <li class="<?php if(URL::current()==route('admin-table',['table'=>'calculation_users'])): ?> active <?php endif; ?> nav-item">
                            <a href="<?php echo e(route('admin-table',['table'=>'calculation_users'])); ?>"
                               class="nav-link <?php if(URL::current()==route('admin-table',['table'=>'calculation_users'])): ?> active <?php endif; ?> "><?php echo e(trans('app.calculation_users')); ?></a>
                        </li>
                        <li class="<?php if(URL::current()==route('admin-table',['table'=>'distributors'])): ?> active <?php endif; ?> nav-item">
                            <a href="<?php echo e(route('admin-table',['table'=>'distributors'])); ?>"
                               class="nav-link <?php if(URL::current()==route('admin-table',['table'=>'distributors'])): ?> active <?php endif; ?> "><?php echo e(trans('app.distributors')); ?></a>
                        </li>

                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<?php /**PATH D:\UPWork Project\Orex\portal.orex\Modules\Admin\Providers/../Resources/views/layouts/sidebar.blade.php ENDPATH**/ ?>