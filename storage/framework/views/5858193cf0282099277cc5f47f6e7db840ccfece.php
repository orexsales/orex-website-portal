<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>" dir="<?php echo e($dir); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?php echo e(asset('images/Logo.png')); ?>" rel="icon" sizes="24*16" type="image/x-icon">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <link rel="icon"
          href="<?php echo e(isset($favicon)?asset(\App\Models\CmpGeneral::IMAGE_File_PATH.$favicon->value):asset('images/favicon.png')); ?>"
          type="image/x-icon">

    <title><?php echo e(isset($project_name)?$project_name->value:'Orex'); ?> | <?php echo $__env->yieldContent('title'); ?></title>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">
    <link href="<?php echo e(asset('argon/vendor/nucleo/css/nucleo.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('argon/vendor/@fortawesome/fontawesome-free/css/all.min.css')); ?>" rel="stylesheet">
<?php if($dir=='ltr'): ?>
    <!-- Argon CSS -->
        <link type="text/css" href="<?php echo e(asset('argon/css/argon.css')); ?>" rel="stylesheet">
    <?php else: ?>
        <link type="text/css" href="<?php echo e(asset('argon/css/argon-rtl.css')); ?>" rel="stylesheet">
    <?php endif; ?>
    <?php echo $__env->yieldContent('styles'); ?>
<!-- <link rel="stylesheet" href="https://portal.orexpower.com/admin/css/components/select-boxes.css" type="text/css"/>
<link rel="stylesheet" href="https://portal.orexpower.com/admin/css/components/select2-bootstrap.css" type="text/css"/>
<link rel="stylesheet" href="https://portal.orexpower.com/admin/css/components/bs-editable.css" type="text/css" />
<link rel="stylesheet" href="https://portal.orexpower.com/admin/css/components/bs-filestyle.css" type="text/css" />
<link rel="stylesheet" href="https://portal.orexpower.com/datatable/datatable-ltr.css" type="text/css"/>
<link rel="stylesheet" href="https://portal.orexpower.com/admin/css/components/select-picker/bootstrap-select.css" type="text/css" />
<link rel="stylesheet" href="https://portal.orexpower.com/admin/css/components/timepicker.css" type="text/css"/> -->
    <link type="text/css" href="<?php echo e(asset('datatable/custom.css')); ?>" rel="stylesheet">
    <script type="text/javascript" src="<?php echo e(asset ('admin/js/jquery.js')); ?>"></script>
    <style>
        @font-face {
            font-family: "omar";
            src: url('../../../../../fonts/SairaCondensed-Medium.ttf');
        }

        @font-face {
            font-family: 'arabic';
            src: url('../../../../../fonts/JF-Flat-regular.ttf');
        }

        <?php if($dir=='rtl'): ?>
         * {
            font-family: 'arabic', sans-serif;
        }

        body {
            font-family: 'arabic', sans-serif;
            text-transform: none !important;
        }

        <?php else: ?>
        * {
            /*font-family: 'omar';*/
            font-family: 'Roboto Slab', serif;
        }

        body {
            /*font-family: 'omar';*/
            font-family: 'Roboto Slab', serif;
            text-transform: none !important;
        }
        <?php endif; ?>
        .fr:not(.table-button,input).none{
    display:none;
}
.none:not(.all):not(.nav-item){
    display:none;
}

    </style>
</head>
<body class="">
<?php echo $__env->make('admin::layouts.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="main-content">
    <?php echo $__env->make('admin::layouts.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('admin::layouts.top-bar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <div class="container-fluid mt--7">
        <?php echo $__env->yieldContent('content'); ?>
        <?php echo $__env->make('admin::layouts.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
</div>
<?php if(\Session::has('msg')): ?>
    <div class="alert alert-info alert-dismissible fade show custom-alert" role="alert">
        <span class="alert-inner--icon"><i class="ni ni-like-2"></i></span>
        <span class="alert-inner--text"> <?php echo e(\Session::get('msg')); ?></span>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
<?php endif; ?>


<script>
    var success = "<?php echo app('translator')->getFromJson('app.success'); ?>";
    var pickimage = "<?php echo app('translator')->getFromJson('app.pick_image'); ?>";
    var pickdoc = "<?php echo app('translator')->getFromJson('app.pick_doc'); ?>";
    var pick = "<?php echo app('translator')->getFromJson('app.pick'); ?>";
    var del = "<?php echo app('translator')->getFromJson('app.delete'); ?>";
    var dir = "<?php echo e($dir); ?>";
    var DIR = "<?php echo e($dir); ?>";
    var _csrf = '<?php echo e(csrf_token()); ?>';
    var lang = '<?php echo e($lang); ?>';
    var LANG = '<?php echo e($lang); ?>';
    var right = '<?php echo e($right); ?>';
    var left = '<?php echo e($left); ?>';
    var baseUrl = '<?php echo e(localizeURL('').'/'); ?>';
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': _csrf
        }
    });
    $(function () {
        var $items = $('.config-search li a');
        var $all = [];
        $items.each(function (i) {
            $all.push($(this).attr('data-config'))
        });
        $('.config-input').keyup(function () {
            $('.config-search').css('display', 'block');
            var $val = $(this).val();
            var $founded = $not = [];

            $items.each(function (i, v) {
                if ($(this).attr('data-config').indexOf($val) >= 0) {
                    // $founded.push({name:$(this).attr('data-name'),id:i})
                    $founded.push($(this).attr('data-config'))
                }
            });
            $not = collect($all).except($founded).all();
            $.each($not, function (i, v) {
                // if (lang === 'ar')
                //     $("[data-config='" + v + "']").css('display', 'none');
                // else
                    $('[data-config=' + v + ']').css('display', 'none');
            });
            if (collect($not).isEmpty()) {
                $('.config-search').css('display', 'none');
                $items.each(function () {
                    $(this).css('display', 'block')
                });
            }
        })
    })
</script>
<!-- Core -->

<script src="<?php echo e(asset('argon/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')); ?>"></script>
<!-- Argon JS -->
<script type="text/javascript" src="<?php echo e(asset('admin/js/bootstrap-notify.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('admin/js/jquery-request-types.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('admin/js/moment-with-locales.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('admin/js/jquery.validate.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('admin/js/index.js')); ?>"></script>
<script src="<?php echo e(asset('argon/js/argon.js')); ?>"></script>
<?php echo $__env->yieldContent('scripts'); ?>


</body>
</html>
<?php /**PATH D:\UPWork Project\Orex\portal.orex\Modules\Admin\Providers/../Resources/views/layouts/app.blade.php ENDPATH**/ ?>