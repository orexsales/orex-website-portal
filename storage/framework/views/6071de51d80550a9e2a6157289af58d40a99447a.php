<div id='<?php echo e($id); ?>'  data-table="<?php echo e($table); ?>" role='dialog' aria-labelledby='myModalLabel' aria-hidden='true' class='modal datatable-modal fade'>
    <div class='modal-dialog modal-lg' role='document' style='<?php echo e($dialogWidth); ?>'>
        <div class='modal-content'>
            <form id='form-dialog' action='#' method='post' class='<?php echo e($form_class); ?>' <?php echo e($form_attr); ?>>
                <div class='modal-header'>
                    <h4 id='myModalLabel' class='modal-title'>
                        <?php if($withTabs): ?>
                            <i class="datatable-sidebar-tab-toggle hidden-lg hidden-md zmdi zmdi-plus-square zmdi-hc-fw datatable-pr-10 hand"></i>
                        <?php endif; ?>
                        <span class='text-dialog'></span>
                    </h4>
                    <button type='button' data-dismiss='modal' aria-label='Close' class='close'>
                        <span aria-hidden='true'>&times;</span>
                    </button>
                </div>
                <div class='modal-body clearfix container-fluid <?php echo e($withTabs == true ? 'tabs' : ''); ?> '>
                    <div class='<?php if($enableGridSystem): ?> <?php echo e($gridSystem['formHorizontal'] ? 'form-horizontal' : ''); ?> <?php else: ?> form-horizontal <?php endif; ?>'>
                        <?php echo $continer; ?>

                    </div>
                </div>
                <div class='modal-footer'>
                    <button type='button' data-dismiss='modal' class='btn btn-outline-danger waves-effect'><?php echo e(trans('datatable::table.dialog.close')); ?></button>
                    <button type='submit' class='btn btn-outline-primary waves-effect' data-status='add' ><span class='text-dialog'></span></button>
                    <button type='submit' class='btn btn-outline-success waves-effect' data-status='save'><span><?php echo e(trans('datatable::table.dialog.save_add_new')); ?></span></button>
                    <?php echo $dialogFooter; ?>

                </div>
            </form>
        </div>
    </div>
</div><?php /**PATH D:\UPWork Project\Orex\portal.orex\vendor\Aut\DataTable\src/Resources/Views/_modal.blade.php ENDPATH**/ ?>