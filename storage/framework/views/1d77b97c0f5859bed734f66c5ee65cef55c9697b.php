<!-- Top navbar -->
<nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
    <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block"
           href="javascript:void(0)"><?php echo e(trans('app.admin_panel')); ?></a>
        <!-- Form -->
        <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
            <div class="form-group mb-0">
                <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-search"></i></span>
                    </div>
                    <input class="form-control config-input" placeholder="<?php echo e(trans('app.Search')); ?>" type="text">
                </div>
            </div>
            <div class="card config-search" style="display: none;position: absolute;top: 78%;width: 25%;">
                <ul class="" style="list-style: none">
                    <?php $__currentLoopData = array_keys(\Config::get('datatableModels')); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $config): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        
                        <li><a href="<?php echo e(route('admin-table',$config)); ?>"
                               data-config="<?php echo e($config); ?>"><?php echo e(trans('app.'.$config)); ?></a></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>
        </form>
        <!-- User -->
        <ul class="navbar-nav align-items-center d-none d-md-flex">
            <li class="nav-item dropdown">
                <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false">
                    <div class="media align-items-center">
                <span class="avatar avatar-sm rounded-circle">
                  <img alt="Image placeholder" src="<?php echo e(Auth::user()->image); ?>"
                       style="width: 36px !important;height: 36px;">
                </span>
                        <div class="media-body ml-2 d-none d-lg-block">
                            <span class="mb-0 text-sm  font-weight-bold"><?php echo e(Auth::user()->name); ?></span>
                        </div>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div class=" dropdown-header noti-title">
                        <h6 class="text-overflow m-0">Welcome!</h6>
                    </div>
                    <a href="<?php echo e(localizeURL('admin/profile')); ?>" class="dropdown-item">
                        <i class="ni ni-single-02"></i>
                        <span><?php echo e(trans('profile.profile')); ?></span>
                    </a>
                    <a href="<?php echo e(LaravelLocalization::getLocalizedURL($lang=='en'?'ar':'en')); ?>" class="dropdown-item">
                        <i class="ni ni-world"></i>
                        <span><?php echo e(trans('app.change_lang')); ?></span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="<?php echo e(localizeURL('logout')); ?>" class="dropdown-item">
                        <i class="ni ni-user-run"></i>
                        <span><?php echo e(trans('app.logout')); ?></span>
                    </a>
                </div>
            </li>
        </ul>
    </div>
</nav>
<!-- Header -->


<?php /**PATH D:\UPWork Project\Orex\portal.orex\Modules\Admin\Providers/../Resources/views/layouts/header.blade.php ENDPATH**/ ?>