<?php
     extract($gridSystemResult);
?>

<div class='form-group <?php echo e($global); ?>' style="display:block;float: <?php if($attr): ?><?php echo e($left); ?><?php endif; ?>">
    <label class='<?php echo e($label); ?> control-label' for='<?php echo e($name); ?>'><?php echo e($title); ?> <?php if($star): ?> <span class='text-danger'>*</span> <?php endif; ?></label>
    <div class="<?php echo e($input); ?>">
        <select id='<?php echo e($id); ?>'
                data-datavalue='<?php echo e($data); ?>'
                name='<?php echo e($name); ?>'
                class='form-control datatable-autocomplete <?php echo e($class); ?>'
                data-letter='0'
                data-target='<?php echo e($target); ?>'
                data-placeholder='<?php echo e($title); ?>'
                tabindex='1'
                data-remote='<?php echo e($url); ?>'
                style='width: 99.9%'
                data-collabel = '<?php echo e($colLabel); ?>'
                data-editable = 'true'
                <?php echo e($attr); ?>

        >
        </select>
        <div id='error_<?php echo e($id); ?>'></div>
    </div>
</div><?php /**PATH D:\UPWork Project\Orex\portal.orex\vendor\Aut\DataTable\src/Resources/Views/component/autocomplete.blade.php ENDPATH**/ ?>