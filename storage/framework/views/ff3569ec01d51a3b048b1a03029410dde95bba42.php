<footer class="footer">
    <div class="row align-items-center justify-content-xl-between">
        <div class="col-xl-6">
            <div class="copyright text-center text-xl-left text-muted">
                &copy; <?php echo e(date('Y')); ?> <a href="/" class="font-weight-bold ml-1" target="_blank"> Orex </a>
            </div>
        </div>
        <div class="col-xl-6">
            <div class="text-xl-right">
                <?php echo trans('app.footer_dev'); ?>

            </div>
        </div>
    </div>
</footer><?php /**PATH D:\UPWork Project\Orex\portal.orex\Modules\Admin\Providers/../Resources/views/layouts/footer.blade.php ENDPATH**/ ?>