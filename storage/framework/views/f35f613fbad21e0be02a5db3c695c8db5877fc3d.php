<!DOCTYPE html>
<html lang="<?php echo e($lang); ?>" dir="<?php echo e($dir); ?>">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->
    <link href="/favicon.png" rel="icon" type="image/png">
    <!-- Icons -->
    <title>Orex | <?php echo $__env->yieldContent('title'); ?></title>
    <link href="<?php echo e(asset('argon/vendor/nucleo/css/nucleo.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('argon/vendor/@fortawesome/fontawesome-free/css/all.min.css')); ?>" rel="stylesheet">
    <?php if($dir=='ltr'): ?>
    <!-- Argon CSS -->
        <link type="text/css" href="<?php echo e(asset('argon/css/argon.css')); ?>" rel="stylesheet">
    <?php else: ?>
        <link type="text/css" href="<?php echo e(asset('argon/css/argon-rtl.css')); ?>" rel="stylesheet">
    <?php endif; ?>
    <script type="text/javascript" src="<?php echo e(asset ('admin/js/jquery.js')); ?>"></script>
</head>

<body class="bg-default">
<div class="main-content">
    <div class="header bg-gradient-primary py-7 py-lg-8">
        <div class="container">
            <div class="header-body text-center mb-7">
                <div class="row justify-content-center">
                    <div class="col-lg-5 col-md-6">
                        <h1 class="text-white"><?php echo e(trans('app.welcome')); ?>!</h1>
                        <p class="text-lead text-light d-none">Use these awesome forms to login or create new account in your project for free.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="separator separator-bottom separator-skew zindex-100 d-none">
            <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
            </svg>
        </div>
    </div>
    <!-- Page content -->
    <?php echo $__env->yieldContent('content'); ?>

    <footer class="py-5">
        <div class="container">
            <div class="row align-items-center justify-content-xl-between">
                <div class="col-xl-6">
                    <div class="copyright text-center text-xl-left text-muted">
                        © <?php echo e(date('Y')); ?>

                        <a href="https://www.orex-ae.com" class="font-weight-bold ml-1" target="_blank">Orex</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<script src="<?php echo e(asset('argon/js/argon.js')); ?>"></script>
<?php echo $__env->yieldContent('scripts'); ?>
</body>

</html>
<?php /**PATH D:\UPWork Project\Orex\portal.orex\resources\views/auth/layouts/app.blade.php ENDPATH**/ ?>