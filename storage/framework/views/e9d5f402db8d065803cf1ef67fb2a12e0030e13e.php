<div class="form-group col-lg-12 col-md-12" style="display:block;float: <?php echo e($dir=='rtl'?'right':'left'); ?>">
    <div role="tabpanel">
        <!-- Nav tabs-->
        <ul role="tablist" class="nav nav-tabs">
            <?php $__currentLoopData = $htabs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tab): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php extract($tab) ?>
                <li role="presentation" class=" nav-item <?php echo e($active ? 'active' : ''); ?> <?php echo e($class); ?>"><a class="nav-link <?php echo e($active ? 'active show' : ''); ?>" data-tab="<?php echo e($id); ?>" href="#<?php echo e($id); ?>" aria-controls="<?php echo e($id); ?>" role="tab" data-toggle="tab" aria-expanded="<?php echo e($active); ?>"><?php echo e($title); ?> <?php if($star): ?><span class='text-danger'>*</span><?php endif; ?></a></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
        <!-- Tab panes-->
        <div class="tab-content clearfix">
            <?php $__currentLoopData = $htabs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tab): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php extract($tab) ?>
                <div id="<?php echo e($id); ?>" data-tab="<?php echo e($id); ?>" role="tabpanel" class="tab-pane <?php echo e($active ? 'active' : ''); ?>"><?php echo $content; ?></div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</div><?php /**PATH D:\UPWork Project\Orex\portal.orex\vendor\Aut\DataTable\src/Resources/Views/_htab.blade.php ENDPATH**/ ?>