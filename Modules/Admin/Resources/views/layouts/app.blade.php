<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" dir="{{$dir}}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{asset('images/Logo.png')}}" rel="icon" sizes="24*16" type="image/x-icon">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon"
          href="{{isset($favicon)?asset(\App\Models\CmpGeneral::IMAGE_File_PATH.$favicon->value):asset('images/favicon.png')}}"
          type="image/x-icon">

    <title>{{isset($project_name)?$project_name->value:'Orex'}} | @yield('title')</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">
    <link href="{{asset('argon/vendor/nucleo/css/nucleo.css')}}" rel="stylesheet">
    <link href="{{asset('argon/vendor/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
@if($dir=='ltr')
    <!-- Argon CSS -->
        <link type="text/css" href="{{asset('argon/css/argon.css')}}" rel="stylesheet">
    @else
        <link type="text/css" href="{{asset('argon/css/argon-rtl.css')}}" rel="stylesheet">
    @endif
    @yield('styles')
<!-- <link rel="stylesheet" href="https://portal.orexpower.com/admin/css/components/select-boxes.css" type="text/css"/>
<link rel="stylesheet" href="https://portal.orexpower.com/admin/css/components/select2-bootstrap.css" type="text/css"/>
<link rel="stylesheet" href="https://portal.orexpower.com/admin/css/components/bs-editable.css" type="text/css" />
<link rel="stylesheet" href="https://portal.orexpower.com/admin/css/components/bs-filestyle.css" type="text/css" />
<link rel="stylesheet" href="https://portal.orexpower.com/datatable/datatable-ltr.css" type="text/css"/>
<link rel="stylesheet" href="https://portal.orexpower.com/admin/css/components/select-picker/bootstrap-select.css" type="text/css" />
<link rel="stylesheet" href="https://portal.orexpower.com/admin/css/components/timepicker.css" type="text/css"/> -->
    <link type="text/css" href="{{asset('datatable/custom.css')}}" rel="stylesheet">
    <script type="text/javascript" src="{{asset ('admin/js/jquery.js')}}"></script>
    <style>
        @font-face {
            font-family: "omar";
            src: url('../../../../../fonts/SairaCondensed-Medium.ttf');
        }

        @font-face {
            font-family: 'arabic';
            src: url('../../../../../fonts/JF-Flat-regular.ttf');
        }

        @if($dir=='rtl')
         * {
            font-family: 'arabic', sans-serif;
        }

        body {
            font-family: 'arabic', sans-serif;
            text-transform: none !important;
        }

        @else
        * {
            /*font-family: 'omar';*/
            font-family: 'Roboto Slab', serif;
        }

        body {
            /*font-family: 'omar';*/
            font-family: 'Roboto Slab', serif;
            text-transform: none !important;
        }
        @endif
        .fr:not(.table-button,input).none{
    display:none;
}
.none:not(.all):not(.nav-item){
    display:none;
}

    </style>
</head>
<body class="">
@include('admin::layouts.sidebar')
<div class="main-content">
    @include('admin::layouts.header')
    @include('admin::layouts.top-bar')
    <div class="container-fluid mt--7">
        @yield('content')
        @include('admin::layouts.footer')
    </div>
</div>
@if(\Session::has('msg'))
    <div class="alert alert-info alert-dismissible fade show custom-alert" role="alert">
        <span class="alert-inner--icon"><i class="ni ni-like-2"></i></span>
        <span class="alert-inner--text"> {{\Session::get('msg')}}</span>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
@endif
{{-------Global variable --------}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/collect.js/4.0.19/collect.min.js" type="text/javascript"></script>--}}
<script>
    var success = "@lang('app.success')";
    var pickimage = "@lang('app.pick_image')";
    var pickdoc = "@lang('app.pick_doc')";
    var pick = "@lang('app.pick')";
    var del = "@lang('app.delete')";
    var dir = "{{$dir}}";
    var DIR = "{{$dir}}";
    var _csrf = '{{csrf_token()}}';
    var lang = '{{$lang}}';
    var LANG = '{{$lang}}';
    var right = '{{$right}}';
    var left = '{{$left}}';
    var baseUrl = '{{localizeURL('').'/'}}';
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': _csrf
        }
    });
    $(function () {
        var $items = $('.config-search li a');
        var $all = [];
        $items.each(function (i) {
            $all.push($(this).attr('data-config'))
        });
        $('.config-input').keyup(function () {
            $('.config-search').css('display', 'block');
            var $val = $(this).val();
            var $founded = $not = [];

            $items.each(function (i, v) {
                if ($(this).attr('data-config').indexOf($val) >= 0) {
                    // $founded.push({name:$(this).attr('data-name'),id:i})
                    $founded.push($(this).attr('data-config'))
                }
            });
            $not = collect($all).except($founded).all();
            $.each($not, function (i, v) {
                // if (lang === 'ar')
                //     $("[data-config='" + v + "']").css('display', 'none');
                // else
                    $('[data-config=' + v + ']').css('display', 'none');
            });
            if (collect($not).isEmpty()) {
                $('.config-search').css('display', 'none');
                $items.each(function () {
                    $(this).css('display', 'block')
                });
            }
        })
    })
</script>
<!-- Core -->
{{--<script src="{{asset('argon/vendor/jquery/dist/jquery.min.js')}}"></script>--}}
<script src="{{asset('argon/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
<!-- Argon JS -->
<script type="text/javascript" src="{{asset('admin/js/bootstrap-notify.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/jquery-request-types.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/moment-with-locales.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/jquery.validate.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/index.js')}}"></script>
<script src="{{asset('argon/js/argon.js')}}"></script>
@yield('scripts')


</body>
</html>
