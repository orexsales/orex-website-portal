<footer class="footer">
    <div class="row align-items-center justify-content-xl-between">
        <div class="col-xl-6">
            <div class="copyright text-center text-xl-left text-muted">
                &copy; {{date('Y')}} <a href="/" class="font-weight-bold ml-1" target="_blank"> Orex </a>
            </div>
        </div>
        <div class="col-xl-6">
            <div class="text-xl-right">
                {!! trans('app.footer_dev') !!}
            </div>
        </div>
    </div>
</footer>