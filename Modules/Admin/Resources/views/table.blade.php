@extends('admin::layouts.app')
@section('title',trans('app.'.$table))
@section('content')
        <div class="row">
            <div class="col-md-11 col-lg-offset-1">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <h2 class="">{{trans('app.'.$table)}}</h2>
                    </div>

                    <div class="table-responsive" style="padding: 0;font-size: 15px">
                        @php $params=\Request::getQueryString() ? ('?' . \Request::getQueryString()) : ''; @endphp
                        {!! datatable($table,$params) !!}
                    </div>
                </div>
            </div>

        </div>
        <div id="ckfinder-widget"></div>
    @if(view()->exists('admin::table-modal.'.$table.'-extra'))
        @include('admin::table-modal.'.$table.'-extra')
    @endif
@endsection
@section('styles')
    {!! $CSS['select2'] !!}
    {!! $CSS['bs-editable'] !!}
    {!! $CSS['bs-filestyle'] !!}
    {!! $CSS['datatable'] !!}
    {!! $CSS['select-picker'] !!}
    {!! $CSS['timepicker'] !!}
@endsection
@section('scripts')
    <script src="{{asset('ckfinder/ckfinder.js')}}"></script>
    {!! $JS['select2'] !!}
    {!! $JS['bs-editable'] !!}
    {!! $JS['bs-filestyle'] !!}
    {!! $JS['datatable'] !!}
    {!! $JS['select-picker'] !!}
    {!! $JS['timepicker'] !!}
    {!! $JS['ckeditor'] !!}
    <script src="{{asset('admin/js/buttons.html5.js')}}"></script>
    <script src="{{asset('admin/js/buttons.print.js')}}"></script>
<script>

</script>
@endsection
