@php
    $id='id';
    $imageWidth=1500;
    $imageHeight=650;
    $true=true;
@endphp
<div class="modal fade" id="modal-image">
    <div class="modal-dialog" style="min-width: 75%">
        <div class="modal-content">
            {!!Form::open(['class'=>'ajax-form','method'=>'put']) !!}
            <div class="modal-header">
                <h4 class="modal-title">@lang('app.update_image')</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">@lang('app.close')</span></button>
            </div>
            <div class=" modal-body clearfix margin-sm">
                {!! Form::thImageUpload('col_full','image',false,'',true,null,null,'','','','image',($imageWidth!==null?'data-width='.$imageWidth.' data-height='.$imageHeight.' data-size='.$true:'')) !!}
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-success btn--icon-text waves-effect" data-dismiss="modal"><i class="zmdi zmdi-close"></i> {{trans('app.close')}}</button>
                <button type="submit" class="btn btn-danger btn--icon-text waves-effect"><i class="zmdi zmdi-plus"></i> {{trans('app.save')}}</button>
            </div>
            {!!Form::close() !!}
        </div>
    </div>
</div>

{{--@section('scripts')--}}
<script>
    @if($imageWidth!==null)
    $(function () {
        $('#modal-image').on('shown.bs.modal', function () {
            var name=$(this).data('name');
            _imageCropUpload($(this),$(this).attr('data-name'));
        });
    });

    @endif
    function admin_update_image($this) {
        var obj = _aut_datatable_getSelectedRow(_aut_datatable_getTableObjectApi('#{{$table}}'), $($($this).closest('tr')));
        var $imageUrl = $(obj.image).attr('src');
        if (typeof ($imageUrl) === typeof (undefined))
            $imageUrl = $(obj.image).find('img').attr('src');
        var $modal = $('#modal-image');
        $modal.attr('data-name',obj['title_en'].split(' ').join('_'));
        $modal.find('form').attr('action', '{{localizeURL('admin/'.$table)}}/' + obj['{{$id}}']);
        @if($imageWidth===null)
        $modal.find('#image').attr('data-url', $imageUrl)/*.attr('data-title',obj.template_en)*/;
        _imageUpload($modal);
        @else
        $modal.find('.image-crop-upload').attr('src', $imageUrl)/*.attr('data-title',obj.template_en)*/;
        @endif
        _validate($modal, 'admin_reloadGrid');
        $modal.find('#image-name').val($imageUrl);
        $modal.modal({backdrop: 'static', keyboard: false}).modal('show');
        $('form').on('submit', function () {
            aut_datatable_refresh('.card', true);
            $modal.modal('hide');
        });
    }

</script>

@if($imageWidth!==null)
    {!! $CSS['cropper'] !!}
    {!! $JS['cropper'] !!}
@endif
{{--@endsection--}}