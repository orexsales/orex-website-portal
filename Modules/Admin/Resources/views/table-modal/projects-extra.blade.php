@php
    $id='id';
    $imageWidth=null;
    $imageHeight=null;
    $imageMainWidth=760;
    $imageMainHeight=500;

@endphp
<div class="modal fade" id="modal-image" tabindex="-1" aria-hidden="true" style="background-color: #464a4ca3">
    <div class="modal-dialog" style="min-width: 75%">
        <div class="modal-content">
            {!!Form::open(['class'=>'ajax-form','method'=>'put','id'=>'upload-image']) !!}
            <div class="modal-header">
                <h4 class="modal-title">@lang('app.update_image')</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">@lang('app.close')</span></button>
            </div>
            <div class=" modal-body clearfix margin-sm">
                <div class="image-upload-cont">
                    <input id="input-703" data-type="image" data-size="true" data-width="760" data-height="500" class="image-uploade" name="image[]" type="file" multiple>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-outline-success btn--icon-text waves-effect" data-dismiss="modal"><i
                        class="zmdi zmdi-thumb-up"></i> {{trans('app.sweet.ok')}}</button>
                {{--<button type="submit" class="btn btn-danger btn--icon-text waves-effect"><i class="zmdi zmdi-plus"></i> {{trans('app.save')}}</button>--}}
            </div>
            {!!Form::close() !!}
        </div>
    </div>
</div>
<div class="modal fade" id="modal-main-image" tabindex="-1" aria-hidden="true" style="background-color: #464a4ca3">
    <div class="modal-dialog" style="min-width: 75%">
        <div class="modal-content">
            {!!Form::open(['class'=>'ajax-form','method'=>'put']) !!}
            <div class="modal-header">
                <h4 class="modal-title">@lang('app.update_image')</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">@lang('app.close')</span></button>
            </div>
            <div class=" modal-body clearfix margin-sm">
                {!! Form::thImageUpload('col_full','image',false,'',true,null,null,'','','','image',$imageMainWidth!==null?'data-width='.$imageMainWidth.' data-height='.$imageMainHeight.' data-size=true':'') !!}
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-outline-success btn--icon-text waves-effect" data-dismiss="modal"><i
                        class="zmdi zmdi-close"></i> {{trans('app.close')}}</button>
                <button type="submit" class="btn btn-outline-danger btn--icon-text waves-effect"><i class="zmdi zmdi-plus"></i> {{trans('app.save')}}</button>
            </div>
            {!!Form::close() !!}
        </div>
    </div>
</div>
<script>
    @if($imageMainWidth!==null)
    $(function () {
        $('#modal-main-image').on('show.bs.modal', function () {
            _imageCropUpload($(this),$(this).attr('data-name'));
        });
    });
    @endif
    function admin_update_image($this) {
        var obj = _aut_datatable_getSelectedRow(_aut_datatable_getTableObjectApi('#{{$table}}'), $($($this).closest('tr')));
        var $imageUrl = $(obj.image).attr('src');
        if (typeof ($imageUrl) === typeof (undefined))
            $imageUrl = $(obj.image).find('img').attr('src');
        var $modal = $('#modal-image');
        var name=obj.name_en.toLowerCase().split(' ').join('_');
        $modal.find('form').attr('action', '{{localizeURL('admin/'.$table)}}/' + obj['{{$id}}']);
        @if($imageWidth===null)
        $modal.find('#image').attr('data-url', $imageUrl)/*.attr('data-title',obj.template_en)*/;
        $.ajax({
            type:'GET',
            url:'{{localizeURL('admin/get-images')}}',
            data:{file:obj.name_en.toLowerCase().split(' ').join('_'),table:'{{$table}}'},
            success:function (data) {
                _imageUpload($modal,name,'{{$table}}', data.files, data.config);
            }
        });
        @else
        $modal.find('.image-crop-upload').attr('src', $imageUrl)/*.attr('data-title',obj.template_en)*/;
        @endif
        _validate($modal, 'admin_reloadGrid');
        // $modal.find('#image-name').val($imageUrl.split('/').pop());
        $modal.modal({keyboard: false}).modal('show');
        $('form').on('submit', function () {
            aut_datatable_refresh('.card', true);
            $modal.modal('hide');
        });
    }

    function admin_update_main_image($this) {
        var obj = _aut_datatable_getSelectedRow(_aut_datatable_getTableObjectApi('#{{$table}}'), $($($this).closest('tr')));
        var $imageUrl = $(obj.image).attr('src');
        if (typeof ($imageUrl) === typeof (undefined))
            $imageUrl = $(obj.image).find('img').attr('src');
        var $modal = $('#modal-main-image');
        $modal.attr('data-name',obj.name_en.toLowerCase().split(' ').join('_'));
        $modal.find('form').attr('action', '{{localizeURL('admin/'.$table)}}/' + obj['{{$id}}']);
        @if($imageMainWidth===null)
        $modal.find('#image').attr('data-url', $imageUrl);
        _imageUpload($modal,name);
        @else
        $modal.find('.image-crop-upload').attr('src', $imageUrl);
        @endif
        _validate($modal, 'admin_reloadGrid');
        $modal.find('#image-name').val($imageUrl);
        $modal.modal({keyboard: false}).modal('show');
        $('form').on('submit', function () {
            aut_datatable_refresh('.card', true);
            $modal.modal('hide');
        });
    }

    function admin_reloadGrid() {
        aut_datatable_reloadTable(_aut_datatable_getTableObjectApi('#{{$table}}'), null, false, false)
    }

</script>

@if($imageMainWidth!==null)
    {!! $CSS['cropper'] !!}
    {!! $JS['cropper'] !!}
@endif
