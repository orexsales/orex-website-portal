<div class="modal fade" id="modal-answer" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            {!!Form::open(['class'=>'form-ajax','method'=>'post']) !!}
            <div class="modal-header">
                <h5 class="modal-title pull-left">{{trans('app.reply')}}</h5>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">@lang('app.close')</span></button>
            </div>
            <div class="modal-body">
               {!! Form::bsTextArea('cil_full','answer',trans('app.answer'),'',true,'answer','',3) !!}</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-success btn--icon-text waves-effect" data-dismiss="modal"><i
                            class="zmdi zmdi-close"></i> {{trans('app.close')}}</button>
                <button type="submit" class="btn btn-outline-danger btn--icon-text waves-effect"><i class="zmdi zmdi-plus"></i> {{trans('app.save')}}</button>
            </div>
            {!!Form::close() !!}
        </div>
    </div>
</div>
<link rel="stylesheet" href="{{asset('general/css/toastr.css')}}">
<script src="{{asset('general/js/toastr.js')}}"></script>
<script>
    function _open_answer($this) {
        var $modal=$('#modal-answer');
        $modal.find('form').attr('action','{{localizeURL('admin/answer')}}');
        $modal.find('form').append('<input type="hidden" name="id" value='+$($this).data('key')+'>');
        $modal.modal('show');
    }
    $(function () {
        $('.form-ajax').ajaxForm(function (data) {
            $('.form-control').val('');
                toastr.info(data.msg);
                $('#modal-answer').modal('hide');
        })
    })
</script>