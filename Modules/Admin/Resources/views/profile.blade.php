@extends('admin::layouts.app')
@section('title',Auth::user()->name)
@section('content')
    {{--<section class="content">--}}
    {{--<div class="content__inner content__inner--sm">--}}
    {{--<header class="content__title">--}}
    {{--<h1>{{Auth::user()->name}}</h1>--}}
    {{--</header>--}}
    
    {{--<div class="card profile col-lg-6 offset-2">--}}
    {{--<div class="profile__img">--}}
    {{--<img id="image-view"--}}
    {{--src="{{isset(Auth::user()->image)?asset(\App\User::IMAGE_FILE.Auth::user()->image):asset('images/avatar.jpg')}}"--}}
    {{--data-image-name="{{asset(\App\User::IMAGE_FILE.Auth::user()->image)}}" width="200" height="200"--}}
    {{--class="user-image"--}}
    {{--alt="">--}}
    
    {{--<a href="javascript:void(0)" class="zmdi zmdi-camera profile__img__edit" data-toggle="modal"--}}
    {{--data-backdrop="static" data-target="#pr-image-modal"></a>--}}
    {{--</div>--}}
    
    {{--<div class="profile__info">--}}
    {{--<form id="profile-form" action="{{localizeURL('admin/profile')}}" enctype="multipart/form-data"--}}
    {{--method="post">--}}
    {{--<div class="row">--}}
    {{--<div class="col-sm-12">--}}
    {{--<div class="input-group">--}}
    {{--<span class="input-group-addon">@</span>--}}
    {{--<div class="form-group">--}}
    {{--<input type="text" class="form-control"--}}
    {{--placeholder="{{trans('profile.username')}}" name="name"--}}
    {{--value="{{Auth::user()->name}}">--}}
    {{--<i class="form-group__bar"></i>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<br>--}}
    {{--<div class="row">--}}
    {{--<div class="col-sm-12">--}}
    {{--<div class="input-group">--}}
    {{--<span class="input-group-addon"><i class="icon-email2"></i></span>--}}
    {{--<div class="form-group">--}}
    {{--<input type="email" class="form-control"--}}
    {{--placeholder="{{trans('profile.email')}}" name="email"--}}
    {{--value="{{Auth::user()->email}}">--}}
    {{--<i class="form-group__bar"></i>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<br>--}}
    {{--<div class="row">--}}
    {{--<div class="col-sm-12">--}}
    {{--<div class="input-group">--}}
    {{--<span class="input-group-addon"><i class="icon-lock"></i></span>--}}
    {{--<div class="form-group">--}}
    {{--<input type="password" class="form-control"--}}
    {{--placeholder="{{trans('profile.password')}}" name="password" value="">--}}
    {{--<i class="form-group__bar"></i>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<br>--}}
    {{--<div class="row">--}}
    {{--<div class="col-sm-9">--}}
    {{--<button type="submit" class="btn btn-outline-primary waves-effect">{{trans('app.save')}}</button>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</form>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</section>--}}
    <div class="row">
        <div class="col-xl-4 offset-4 order-xl-2 mb-5 mb-xl-0">
            <div class="card card-profile shadow">
                <div class="row justify-content-center">
                    <div class="col-lg-3 order-lg-2">
                        <div class="card-profile-image">
                            <a href="#">
                                <img id="image-view"
                                     src="{{isset(Auth::user()->image)?asset(\App\User::IMAGE_FILE.Auth::user()->image):asset('images/avatar.jpg')}}"
                                     data-image-name="{{isset(Auth::user()->image)?asset(\App\User::IMAGE_FILE.Auth::user()->image):asset('images/avatar.jpg')}}"
                                     class="rounded-circle user-image">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                    <div class="d-flex justify-content-between">
                        <a href="#" class="btn btn-sm btn-info mr-4" data-toggle="modal"
                           data-backdrop="static" data-target="#pr-image-modal">{{trans('app.upload_image')}}</a>
                    </div>
                </div>
                <br>
                <br>
                <div class="card-body pt-0 pt-md-4">
                    <div class="text-center">
                        <form id="profile-form" action="{{localizeURL('admin/profile')}}" enctype="multipart/form-data"
                              method="post">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="input-group input-group-alternative mb-4">
                                        <input class="form-control" value="{{Auth::user()->name}}" name="name"
                                               placeholder="{{trans('profile.username')}}" type="text">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="ni ni-circle-08"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="input-group input-group-alternative mb-4">
                                        <input class="form-control" value="{{Auth::user()->email}}" name="email"
                                               placeholder="{{trans('profile.email')}}" type="email">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="input-group input-group-alternative mb-4">
                                        <input class="form-control" name="password"
                                               placeholder="{{trans('profile.password')}}" type="password">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="ni ni-key-25"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit"
                                        class="btn btn-outline-primary waves-effect">{{trans('app.save')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin::modals')
@endsection
@section('styles')
    {!! $CSS['bs-editable'] !!}
    <link rel="stylesheet" href="{{asset('general/css/toastr.css')}}" type="text/css">
@endsection
@section('scripts')
    {!! $JS['bs-editable'] !!}
    {!! $JS['cropper'] !!}
    <script src="{{asset('general/js/toastr.js')}}"></script>
    <script>
        $(function () {
            $('#pr-image-modal').on('show.bs.modal', function (event) {
                pr_handleShowModal(this);
            });
            $('#profile-form').ajaxForm({
                data: $(this).serialize()/*+'&_method=put'*/,
                complete: function (data) {
                    toastr.info(data.responseJSON.msg);
                }
            });
        });

        function pr_handleShowModal(obj) {
            var $this = $(obj);
            $this.find('#image-name').val($('#image-view').attr('data-image-name'));
            $this.find('.image-crop-upload').attr('src', $('#image-view').attr('src'));
            _imageCropUpload($this, '{{str_replace(' ','_',Auth::user()->name)}}');
            _validate(obj, 'pr_handleImageSelection');
        }

        function pr_handleImageSelection() {
            $('.user-image').attr('src', $('#pr-image-modal').find('img').attr('src'));
            $('.user__img').attr('src', $('#pr-image-modal').find('img').attr('src'));
        }
    
    </script>
@endsection