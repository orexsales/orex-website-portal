<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web','auth','auth.admin','localeSessionRedirect', 'localizationRedirect', 'localeViewPath'], 'prefix' => LaravelLocalization::setLocale().'/admin'], function()
{
    Route::get('/','AdminController@index')->name('admin-home');
    Route::get('table/{table}','AdminController@table')->name('admin-table');
    Route::post('style','AdminController@saveStyle');
    //uploads
    Route::post('upload-image/{resize?}', 'AdminController@uploadImage');
    Route::post('multi-upload-image/{resize?}', 'AdminController@multiUploadImage');
    Route::post('upload-doc/{file}', 'AdminController@uploadDocument');
    Route::put('gallery/{id}','AdminController@updateImageGallery');
    Route::put('generals/{id}','AdminController@updateImageGeneral');
    Route::put('products/{id}','AdminController@updateImageProduct');
    Route::put('projects/{id}','AdminController@updateImageProjects');
    Route::put('members/{id}','AdminController@updateImageMember');
    Route::put('categories/{id}','AdminController@updateImageCategory');
    Route::put('achievments/{id}','AdminController@updateImageAchievement');
    Route::put('blogs/{id}','AdminController@updateImageBlog');
    //Autocomplete
    Route::get('icons-autocomplete','AutoCompleteController@iconAutocomplete');
    Route::get('categories-autocomplete','AutoCompleteController@categoryAutocomplete');
    Route::get('sizes-autocomplete','AutoCompleteController@sizesAutocomplete');
    Route::get('roles-autocomplete','AutoCompleteController@roleAutocomplete');
    Route::get('tags-autocomplete','AutoCompleteController@tagAutocomplete');
    Route::get('products-autocomplete','AutoCompleteController@productsAutocomplete');
    Route::get('attributes-autocomplete','AutoCompleteController@attributesAutocomplete');
    //files
    Route::get('get-images','AdminController@getImages');
    Route::get('get-files','AdminController@getFilesUploaded');
    Route::get('get-video-product','AdminController@getVideoProduct');
    Route::post('delete-image', 'AdminController@deleteImage')->name('delete-image');
    Route::post('delete-file/{type}/{file}', 'AdminController@deletePdfFile')->name('delete-files');
    //profile
    Route::get('profile','AdminController@profile');
    Route::post('profile','AdminController@saveProfile');
    Route::put('profile','AdminController@saveProfile');
    Route::post('answer','AdminController@answer');
//    Route::get('/', 'UserController@index');
});
