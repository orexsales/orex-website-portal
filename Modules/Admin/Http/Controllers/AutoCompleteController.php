<?php

namespace Modules\Admin\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Attribute;
use App\Models\Category;
use App\Models\City;
use App\Models\FontAwesome;
use App\Models\MenuItem;
use App\Models\Page;
use App\Models\Product;
use App\Models\Role;
use App\Models\Tag;
use App\Models\Team;
use App\User;
use Illuminate\Http\Request;

class AutoCompleteController extends Controller
{
    public function categoryAutocomplete(Request $request)
    {
        $q = str_replace(' ', '%', $request->get('q', ''));
        $data = Category::whereRaw('upper(name_' . $this->lang . ') like upper(\'%' . $q . '%\')')
            ->get(['name_' . $this->lang . ' as name', 'category_id as id']);
        return ['items' => $data];
    }

    public function roleAutocomplete(Request $request)
    {
        $q = str_replace(' ', '%', $request->get('q', ''));
        $data = Role::whereRaw('upper(name_' . $this->lang . ') like upper(\'%' . $q . '%\')')
            ->get(['name_' . $this->lang . ' as name', 'id as id']);
        return ['items' => $data];
    }

    public function tagAutocomplete(Request $request)
    {
        $q = str_replace(' ', '%', $request->get('q', ''));
        $data = Tag::whereRaw('upper(name_' . $this->lang . ') like upper(\'%' . $q . '%\')')
            ->get(['name_' . $this->lang . ' as name', 'tag_id as id']);
        return ['items' => $data];
    }

    public function productsAutocomplete(Request $request)
    {
        $q = str_replace(' ', '%', $request->get('q', ''));
        $data = Product::whereRaw('upper(name_' . $this->lang . ') like upper(\'%' . $q . '%\')')
            ->get(['name_' . $this->lang . ' as name', 'product_id as id']);
        return ['items' => $data];
    }

    public function attributesAutocomplete(Request $request)
    {
        $q = str_replace(' ', '%', $request->get('q', ''));
        $data = Attribute::whereRaw('upper(text) like upper(\'%' . $q . '%\')')
            ->get(['text as name', 'attribute_id as id']);
        return ['items' => $data];
    }

    public function iconAutocomplete(Request $request)
    {
        $q = str_replace(' ', '%', $request->get('q', ''));
        $data = FontAwesome::whereRaw('upper(content) like upper(\'%' . $q . '%\')')
            ->get();
        foreach ($data as $datum) {
            $data[] = ['name' => '<em class="' . $datum->icon . '"></em>' . ' ' . $datum->content, 'id' => $datum->icon];
        }
        return ['items' => $data];
    }

    public function usersAutocomplete(Request $request)
    {
        $q = str_replace(' ', '%', $request->get('q', ''));
        $data = User::whereRaw('upper(name) like upper(\'%' . $q . '%\')');
        return ['items' => $data->get(['name as name', 'id as id'])];
    }

    public function sizesAutocomplete(Request $request)
    {
        $sizes = ['S' => trans('app.S'), 'M' => trans('app.M'), 'L' => trans('app.L')];
        $data = [];
        foreach ($sizes as $key => $value) {
            $data[] = ['id' => $key, 'name' => $value];
        }
        return ['items' => $data];
    }

}
