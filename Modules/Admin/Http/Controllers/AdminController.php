<?php

namespace Modules\Admin\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Jobs\CopyFileToOrigin;
use App\Models\Achievment;
use App\Models\Blog;
use App\Models\Category;
use App\Models\CmpGeneral;
use App\Models\ContactMail;
use App\Models\Gallery;
use App\Models\Member;
use App\Models\Product;
use App\Models\Project;
use App\Models\Style;
use App\User;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Route;
use Intervention\Image\ImageManager;
use Storage;
use File;

class AdminController extends Controller
{
    public function __construct(Route $route)
    {
        parent::__construct($route);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('admin::index');
    }

    /**
     * @param  Request  $request
     * @param  bool  $resize
     * @return array
     */
    public function multiUploadImage(Request $request, $resize = false)
    {
        if ($request->hasFile('image')) {
            foreach ($request->files->get('image') as $file) {
                $image = $file;
                $filename = $image->getClientOriginalName();
                $ext = pathinfo($filename, PATHINFO_EXTENSION);
                $name = $request->get('name').(int) rand(10, 1000).'.'.$ext;
                $path = 'images/'.$request->get('table').'/_'.$name;
                Storage::disk('public')->put($path, file_get_contents($image->getRealPath()));

                // $this->compress(storage_path('app/public/images/'.$request->get('table').'/_'.$name),
                //     storage_path('app/public/images/'.$request->get('table').'/_'.$name), 75);
                rename(config('path.SYSTEM_BASE_STORAGE') . 'storage/temp/'.$request->get('image'),
                    config('path.SYSTEM_BASE_STORAGE') . 'storage/images/'.$request->get('image'));

                $source = storage_path('app/public/images/'.$request->get('table').'/_'.$name);
                
                $target2 = config('path.SYSTEM_BASE_STORAGE') . 'storage/images/'.$request->get('table').'/_'.$name;
                $target = config('path.SYSTEM_BASE_STORAGE') . 'storage/app/public/images/'.$request->get('table').'/_'.$name;

                CopyFileToOrigin::dispatch($source, $target);
                CopyFileToOrigin::dispatch($source, $target2);

                return ['success' => true, 'filename' => $filename, 'fileurl' => url('storage/'.$path)];
            }
        } else {
            return ['false'];
        }
    }

    /**
     * @param $source
     * @param $destination
     * @param $quality
     * @return mixed
     */
    function compress($source, $destination, $quality)
    {
        $info = getimagesize($source);
        $image = '';

        if ($info['mime'] == 'image/jpeg') {
            $image = imagecreatefromjpeg($source);
        } elseif ($info['mime'] == 'image/gif') {
            $image = imagecreatefromgif($source);
        } elseif ($info['mime'] == 'image/png') {
            $image = imagecreatefrompng($source);
        }

        imagejpeg($image, $destination, $quality);

        return $destination;
    }

    /**
     * @param  Request  $request
     * @return array
     */
    public function getImages(Request $request)
    {
        if ($request->get('file')) {
            $files = Storage::disk('public')->files('images/'.$request->get('table'));
            $urlFile = [];
            $config = [];
            $filesystem = new Filesystem;
            $file = str_replace('/', '-', $request->get('file'));
            $foundFiles = collect($files)->filter(function ($v) use ($file) {
                return preg_match("/_$file/", $v);
            });
            foreach ($foundFiles as $index => $file) {
                $key = $file;
                $deleteUrl = substr($key, 16);
                $urlFile[] = '<img src="'.config('path.WEB_BASE_URL') . 'storage/'.$file.'" class="kv-preview-data file-preview-image">';
                $config[] = [
                    'type' => "image", 'size' => Storage::size('public/'.$file), 'key' => $file,
                    'caption' => $filesystem->name("storage/$file"), 'url' => route('delete-image'),
                    'downloadUrl' => url(Storage::url('public/'.$file)),
                ];
            }
            return ['success' => true, 'files' => $urlFile, 'config' => $config];
        }
        return ['success' => false];
    }

    public function getFilesUploaded(Request $request)
    {
        if ($request->get('file')) {
            $files = Storage::disk('public')->files('files');
            $urlFile = [];
            $config = [];
            $fileSystem = new Filesystem;
            $file = str_replace('/', '-', $request->get('file'));
            $foundFiles = collect($files)->filter(function ($v) use ($file) {
                return preg_match("/$file/", $v);
            });
            foreach ($foundFiles as $index => $file) {
                $key = $file;
                $deleteUrl = substr($key, 6);
                $urlFile[] = config('path.WEB_BASE_URL') . "storage/{$file}";
                $config[] = [
                    'type' => substr($file, strpos($file, ".") + 1), 'size' => Storage::size('public/'.$file),
                    'key' => $key, 'caption' => $fileSystem->name("storage/$file"),
                    'url' => route('delete-files', ['files', $deleteUrl]),
                    'downloadUrl' => config('path.WEB_BASE_URL') . 'storage/'.$file,
                ];
            }
            return ['success' => true, 'files' => $urlFile, 'config' => $config];
        }
        return ['success' => false];
    }


    public function getVideoProduct(Request $request)
    {
        if ($request->filled('id')) {
            $product = Product::findOrFail($request->get('id'));
            $urlFile = null;
            $config = null;
            if ($product->video) {
                $urlFile = config('path.WEB_BASE_URL') . 'storage/videos/products/'.$product->video;
                $config = [
                    'type' => "video",
                    'size' => '0.3M',
                    'key' => 'videos/products/'.$product->video,
                    'caption' => $product->name,
                    'url' => route('delete-image'),
                    'downloadUrl' => config('path.WEB_BASE_URL') . 'storage/videos/products/'.$product->video,
                ];
            }

            return ['success' => true, 'url' => $urlFile, 'configs' => $config];
        } else {
            return ['success' => false];
        }
    }

    public function deleteImage(Request $request)
    {
        $url = explode('/', $request->get('key'));
        $type = $url[0];
        $folder = $url[1];
        $file = $url[2];
        try {
            $model = 'App\Models\\'.explode('s', ucfirst($folder))[0];
            Storage::delete('public/'.$type.'/'.$folder.'/'.$file);

            $deleteFilePath = config('path.SYSTEM_BASE_STORAGE') . 'storage/app/public/'.$type.'/'.$folder.'/'.$file;
            unlink($deleteFilePath);

            $product = $model::where('video', $file)->first();

            if ($product) {
                if ($folder == 'videos') {
                    $product->video = '';
                }
                $product->save();
            }
            return ['success' => true];
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function deletePdfFile($type, $file)
    {
        try {
            if ($type == 'files') {
                Storage::delete('public/'.$type.'/'.$file);
                $deleteFilePath = config('path.SYSTEM_BASE_STORAGE') . 'storage/app/public/'.$type.'/'.$file;
                unlink($deleteFilePath);
            } else {
                $folder = 'products';
                Storage::delete('public/'.$type.'/'.$folder.'/'.$file);
                $deleteFilePath = config('path.SYSTEM_BASE_STORAGE') . 'storage/app/public/'.$type.'/'.$folder.'/'.$file;
                unlink($deleteFilePath);
            }
            return ['success' => true];
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function uploadDocument($fileName, Request $request)
    {
        ///home/u216259457/domains/orexpower.com/portal
        $this->validate($request, [
            'doc' => 'mimes:pdf|max:8000',
        ]);
        $file = $request->files->get('doc');
        $extention = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
        $fileTempName = ((int) rand(100000, 1000000)).'-'.strtolower(str_replace(' ', '_',
                str_replace('.', '_', $fileName))).'.'.$extention;
        Storage::disk('public')->put('files/'.$fileTempName, file_get_contents($file->getRealPath()));
        $source = storage_path('app/public/files/'.$fileTempName);
       //$target = '/home/u216259457/domains/orexpower.com/orex/storage/app/public/files/'.$fileTempName;
        $target = config('path.SYSTEM_BASE_STORAGE') . 'storage/files/'.$fileTempName;
        $target2 = config('path.SYSTEM_BASE_STORAGE') . 'storage/app/public/files/'.$fileTempName;

        CopyFileToOrigin::dispatch($source, $target);
        CopyFileToOrigin::dispatch($source, $target2);
        return ['success' => true, 'filename' => $fileTempName, 'OriginalFilename' => $file->getClientOriginalName()];
    }

    public function uploadImage(Request $request, $resize = false)
    {

        $this->validate($request, [
            'image' => 'mimes:jpeg,jpg,bmp,png,mp4,avi,flv|max:100000',
            'upload' => 'image|max:100000',//For CKEditor
            'image_url' => 'max:10000000',//For Crop
        ]);
        $fileTempName = '';


        if ($request->files->has('image', '') || $request->files->has('upload', '')) {
            $file = $request->files->get('image', $request->files->get('upload', ''));
            $extention = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            $fileTempName = sha1(time().(int) rand(100000, 1000000)).'.'.$extention;//public_path() . '/upload/temp/';
            Storage::disk('public')->put('temp/'.$fileTempName, file_get_contents($file->getRealPath()));
        } else {
            $manager = new ImageManager(['driver' => 'gd']);
            $image = $manager->make($request->get('image_url', ''));
            if ($resize == 'image') {
                //$resize = $image->resize(334, 419);
                $fileTempName = $request->get('name') ? str_replace('/', '-',
                        $request->get('name')).sha1(time().(int) rand(10, 100)).'.'.explode('/',
                        $image->mime())[1] : $request->get('image').sha1(time().(int) rand(10, 100)).'.'.explode('/',
                        $image->mime())[1];
            } else {
                $fileTempName = $request->get('name') ? str_replace('/', '-',
                        $request->get('name')).sha1(time().(int) rand(10, 100)).'.'.explode('/',
                        $image->mime())[1] : $request->get('image').sha1(time().(int) rand(10, 100)).'.'.explode('/',
                        $image->mime())[1];
            }
            $image->save(storage_path('app/public/temp/'.$fileTempName));

            $source = storage_path('app/public/temp/'.$fileTempName);
            $target = config('path.SYSTEM_BASE_STORAGE') . 'storage/temp/'.$fileTempName;
            $target2 = config('path.SYSTEM_BASE_STORAGE') . 'storage/images/products/'.$fileTempName;
            CopyFileToOrigin::dispatch($source, $target);
            CopyFileToOrigin::dispatch($source, $target2);

        }
        return [
            'success' => true, 'filename' => $fileTempName,
            'fileurl' => config('path.WEB_BASE_URL') . 'storage/temp/'.$fileTempName
        ];
    }

    public function table($table, $hasExtra = false)
    {
        // $this->compressFiles();

        $tableName = $table.'-table';
        return view('admin::table', compact('tableName', 'hasExtra', 'table'));
    }


    public function saveStyle(Request $request)
    {
        $style = Style::first();
        $style->value = $request->get('value');
        $style->save();
    }

    public function updateImageGallery($id, Request $request)
    {
        $gallery = Gallery::find($id);
        if ($request->get('image') != $gallery->image) {
            if (isset($gallery->image)) {
                Storage::delete('public/images/gallery/'.$gallery->image);
            }
            $this->moveFileFromTemp(Gallery::IMAGE_URL_PATH, $request->get('image'));
        }
        $gallery->update($request->input());
        // $this->compress(storage_path('app/public/images/gallery/'.$request->get('image')),
        //     storage_path('app/public/images/gallery/'.$request->get('image')), 70);
        rename(config('path.SYSTEM_BASE_STORAGE') . 'storage/temp/'.$request->get('image'),
            config('path.SYSTEM_BASE_STORAGE') . 'storage/images/gallery/'.$request->get('image'));
    }

    /**
     * @param $newPath
     * @param $name
     * @param  string  $newName
     */
    function moveFileFromTemp($newPath, $name, $newName = '')
    {
        if ($newName == '') {
            $newName = $name;
        }
        \File::move(storage_path('app/public/temp/'.$name), storage_path('app/public/'.$newPath).$newName);
    }

    public function updateImageCategory($id, Request $request)
    {
        $category = Category::find($id);
        if ($request->get('image') != $category->image) {
            if (isset($category->image)) {
                Storage::delete('public/images/categories/'.$category->image);
            }
            $this->moveFileFromTemp(Category::IMAGE_URL_PATH, $request->get('image'));
        }
        $category->update($request->input());


        rename(config('path.SYSTEM_BASE_STORAGE') . 'storage/temp/'.$request->get('image'),
            config('path.SYSTEM_BASE_STORAGE') . 'storage/images/categories/'.$request->get('image'));

        // $this->compress(storage_path('app/public/temp/'.$request->get('image')),
        //     storage_path('app/public/images/categories/'.$request->get('image')), 75);
    }

    public function updateImageAchievement($id, Request $request)
    {
        $achievement = Achievment::find($id);
        if ($request->get('image') != $achievement->image) {
            if (isset($achievement->image)) {
                Storage::delete('public/images/achievments/'.$achievement->image);
            }
            $this->moveFileFromTemp(Achievment::IMAGE_URL_PATH, $request->get('image'));
        }
        $achievement->update($request->input());
        // $this->compress(storage_path('app/public/images/achievments/'.$request->get('image')),
            // storage_path('app/public/images/achievments/'.$request->get('image')), 50);

        rename(config('path.SYSTEM_BASE_STORAGE') . 'storage/temp/'.$request->get('image'),
            config('path.SYSTEM_BASE_STORAGE') . 'storage/images/achievments/'.$request->get('image'));
    }

    public function updateImageGeneral($id, Request $request)
    {
        $general = CmpGeneral::find($id);
        if ($request->get('image') != $general->value) {
            if (isset($general->value)) {
                Storage::delete('public/images/generals/'.$general->value);
            }
            $this->moveFileFromTemp(CmpGeneral::IMAGE_URL_PATH, $request->get('image'));
        }
        $general->value = $request->get('image');
        $general->save();
    }

    public function updateImageBlog($id, Request $request)
    {
        $blog = Blog::find($id);
        if ($request->get('image') != $blog->image) {
            if (isset($blog->image)) {
                Storage::delete('public/images/blogs/'.$blog->image);
            }
            $this->moveFileFromTemp(Blog::IMAGE_URL_PATH, $request->get('image'));
        }
        $blog->image = $request->get('image');
        $blog->save();
        // $this->compress(storage_path('app/public/images/blogs/'.$request->get('image')),
        //     storage_path('app/public/images/blogs/'.$request->get('image')), 50);
        rename(config('path.SYSTEM_BASE_STORAGE') . 'storage/temp/'.$request->get('image'),
            config('path.SYSTEM_BASE_STORAGE') . 'storage/images/blogs/'.$request->get('image'));
    }

    public function updateImageMember($id, Request $request)
    {
        $member = Member::find($id);
        if ($request->get('image') != $member->image) {
            if (isset($member->image)) {
                Storage::delete('public/images/members/'.$member->image);
            }
            $this->moveFileFromTemp(Member::IMAGE_URL_PATH, $request->get('image'));
        }
        $member->image = $request->get('image');
        $member->save();
        // $this->compress(storage_path('app/public/images/members/'.$request->get('image')),
        //     storage_path('app/public/images/members/'.$request->get('image')), 50);
        rename(config('path.SYSTEM_BASE_STORAGE') . 'storage/temp/'.$request->get('image'),
            config('path.SYSTEM_BASE_STORAGE') . 'storage/images/members/'.$request->get('image'));
    }

    public function updateImageProduct($id, Request $request)
    {
        $product = Product::find($id);
        if ($request->get('image')) {
            if ($request->get('image') != $product->image) {
                if (isset($product->image)) {
                    Storage::delete('public/images/products/'.$product->image);
                }
                $this->moveFileFromTemp(Product::IMAGE_URL_PATH, $request->get('image'));
                // $this->compress(storage_path('app/public/images/products/'.$request->get('image')),
                //     storage_path('app/public/images/products/'.$request->get('image')), 50);
                rename(config('path.SYSTEM_BASE_STORAGE') . 'storage/temp/'.$request->get('image'),
                    config('path.SYSTEM_BASE_STORAGE') . 'storage/images/products/'.$request->get('image'));
            }
        } else {
            if ($request->get('video') != $product->video) {
                if (isset($product->video)) {
                    Storage::delete('public/videos/products/'.$product->video);
                }
                $this->moveFileFromTemp(Product::VIDEO_URL_PATH, $request->get('video'));
            }
        }
        $product->update($request->input());
    }

    public function updateImageProjects($id, Request $request)
    {
        $product = Project::find($id);
        if ($request->get('image') != $product->image) {
            if (isset($product->image)) {
                Storage::delete('public/images/projects/'.$product->image);
            }
            $this->moveFileFromTemp(Project::IMAGE_URL_PATH, $request->get('image'));
            // $this->compress(storage_path('app/public/images/projects/'.$request->get('image')),
            //     storage_path('app/public/images/projects/'.$request->get('image')), 50);
            rename(config('path.SYSTEM_BASE_STORAGE') . 'storage/temp/'.$request->get('image'),
                config('path.SYSTEM_BASE_STORAGE') . 'storage/images/projects/'.$request->get('image'));
        }
        $product->image = $request->get('image');
        $product->save();
    }

    public function profile()
    {
        return view('admin::profile');
    }

    public function saveProfile(Request $request)
    {
        if ($request->get('image')) {
            Storage::delete('public/images/users/'.\Auth::user()->image);
            $this->moveFileFromTemp(User::IMAGE_PATH, $request->get('image'));
        }
        \Auth::user()->update($request->input());

        return ['msg' => trans('app.save_success')];
    }

    public function answer(Request $request)
    {
        $id = $request->get('id');
        $contact = ContactMail::find($id);
        $contact->answer = $request->get('answer');
        $contact->is_answered = 'Y';
        $contact->save();
        return ['msg' => trans('app.save_success')];
    }

    function getLocationInfoByIp()
    {
        $client = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote = @$_SERVER['REMOTE_ADDR'];
        $result = ['country' => '', 'city' => ''];
        if (filter_var($client, FILTER_VALIDATE_IP)) {
            $ip = $client;
        } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
            $ip = $forward;
        } else {
            $ip = $remote;
        }
        $ip = '82.100.191.255';
        $ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));
        if ($ip_data && $ip_data->geoplugin_countryName != null) {
            $result['country'] = $ip_data->geoplugin_countryCode;
            $result['city'] = $ip_data->geoplugin_city;
        }
        return $result;
    }

    function resize_image($file, $w, $h, $crop = false)
    {
        [$width, $height] = getimagesize($file);
        $r = $width / $height;
        if ($crop) {
            if ($width > $height) {
                $width = ceil($width - ($width * abs($r - $w / $h)));
            } else {
                $height = ceil($height - ($height * abs($r - $w / $h)));
            }
            $newWidth = $w;
            $newHeight = $h;
        } else {
            if ($w / $h > $r) {
                $newWidth = $h * $r;
                $newHeight = $h;
            } else {
                $newHeight = $w / $r;
                $newWidth = $w;
            }
        }

        //Get file extension
        $exploding = explode(".", $file);
        $ext = end($exploding);

        switch ($ext) {
            case "png":
                $src = imagecreatefrompng($file);
                break;
            case "jpeg":
            case "jpg":
                $src = imagecreatefromjpeg($file);
                break;
            case "gif":
                $src = imagecreatefromgif($file);
                break;
            default:
                $src = imagecreatefromjpeg($file);
                break;
        }

        $dst = imagecreatetruecolor($newWidth, $newHeight);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

        return $dst;
    }


}
