<?php

/**
 *  you must add here route and model and view foreach
 *
 *  model             : you must add model for datatable
 *  dataTableFunc     : default funcName is get_datatable // add name for maker method ,this name will be used in maker class or in factory class as get_func / build_func / store_func / update_fund / destroy_func
 *  middlewares       : you can make auth by pass string or array to this property
 *  middlewaresOption : this option allow you to only make auth on on action or except action from auth by pass array
 *  request           : this is must be Form Request Class
 *  stopOperation     : for stop normal oper and add custom oper ['store','update','destroy']
 *  factory           : the default factory is  modelFactory unless you add you own factory property
 *
 */

use App\User;
use App\Models\{Seo,
    Tag,
    Blog,
    Page,
    Role,
    Style,
    Member,
    Rating,
    Gallery,
    Product,
    Tracker,
    Project,
    Category,
    Question,
    Attribute,
    ClientSay,
    Subscribe,
    Achievment,
    CmpCounter,
    CmpGeneral,
    ContactMail,
    WorkSchedule,
    Attribute_page};
use App\Factories\{SeoFactory,
    TagFactory,
    BlogFactory,
    PageFactory,
    RateFactory,
    RoleFactory,
    StyleFactory,
    UsersFactory,
    MemberFactory,
    CounterFactory,
    GalleryFactory,
    GeneralFactory,
    ProductFactory,
    TrackerFactory,
    ProjectFactory,
    CategoryFactory,
    QuestionFactory,
    AttributeFactory,
    ClientSayFactory,
    SubscribeFactory,
    AchievmentFactory,
    ContactMailFactory,
    WorkScheduleFactory,
    AttributePageFactory};

return [

    'users' => [
        'model' => User::class,
        'factory' => UsersFactory::class,
        'stopOperation' => ['update'],
    ],
    'generals' => [
        'model' => CmpGeneral::class,
        'factory' => GeneralFactory::class,
    ],
    'categories' => [
        'model' => Category::class,
        'factory' => CategoryFactory::class,
        'stopOperation' => ['store', 'update'],
    ],
    'client-say' => [
        'model' => ClientSay::class,
        'factory' => ClientSayFactory::class,
    ],
    'counters' => [
        'model' => CmpCounter::class,
        'factory' => CounterFactory::class,
    ],
    'contact-mails' => [
        'model' => ContactMail::class,
        'factory' => ContactMailFactory::class,
    ],
    'products' => [
        'model' => Product::class,
        'factory' => ProductFactory::class,
        'stopOperation' => ['store', 'update'],
    ],
    'questions' => [
        'model' => Question::class,
        'factory' => QuestionFactory::class,
    ],
    'seos' => [
        'model' => Seo::class,
        'factory' => SeoFactory::class,
    ],
    'styles' => [
        'model' => Style::class,
        'factory' => StyleFactory::class,
    ],
    'achievments' => [
        'model' => Achievment::class,
        'factory' => AchievmentFactory::class,
    ],
    'pages' => [
        'model' => Page::class,
        'factory' => PageFactory::class,
        'stopOperation' => ['store', 'update', 'destroy'],
    ],
    'gallery' => [
        'model' => Gallery::class,
        'factory' => GalleryFactory::class,
    ],
    'blogs' => [
        'model' => Blog::class,
        'factory' => BlogFactory::class,
        'stopOperation' => ['store', 'update'],
    ],
    'rates' => [
        'model' => Rating::class,
        'factory' => RateFactory::class,
    ],
    'subscribes' => [
        'model' => Subscribe::class,
        'factory' => SubscribeFactory::class,
    ],
    'attribute-page' => [
        'model' => Attribute_page::class,
        'factory' => AttributePageFactory::class,
    ],
    'attributes' => [
        'model' => Attribute::class,
        'factory' => AttributeFactory::class,
    ],
    'tags' => [
        'model' => Tag::class,
        'factory' => TagFactory::class,
        'stopOperation' => ['store', 'update'],
    ],
    'roles' => [
        'model' => Role::class,
        'factory' => RoleFactory::class,
        'stopOperation' => ['store'],
    ],
    'opening-hours' => [
        'model' => WorkSchedule::class,
        'factory' => WorkScheduleFactory::class,
    ],
    'visitors' => [
        'model' => Tracker::class,
        'factory' => TrackerFactory::class,
    ],
    'members' => [
        'model' => Member::class,
        'factory' => MemberFactory::class,
    ],
    'projects' => [
        'model' => Project::class,
        'factory' => ProjectFactory::class,
    ],

    'calculation_users' => [
        'model' => \App\Models\CalculationUser::class,
        'factory' => \App\Factories\CalculationUsresFactory::class,
    ],
    'distributors' => [
        'model' => \App\Models\Distributor::class,
        'factory' => \App\Factories\DistributorsFactory::class,
    ],
];
