<?php

namespace App\Models;


class Tracker extends \Eloquent
{
    protected $fillable = ['ip', 'date', 'country'];

    public function hit()
    {
//        $visit=self::where('ip',$_SERVER['REMOTE_ADDR'])->first();
//        if ($visit)
//            return;
//        else
            self::create([
                'ip' => $_SERVER['REMOTE_ADDR'],
                'date' => date('Y-m-d H:i:s'),
                'country'=>$this->getLocationInfoByIp()
            ]);
    }

    /**
     * @return string
     */
    function getLocationInfoByIp()
    {
        $client = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote = @$_SERVER['REMOTE_ADDR'];
        $result ='';
        if (filter_var($client, FILTER_VALIDATE_IP)) {
            $ip = $client;
        } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
            $ip = $forward;
        } else {
            $ip = $remote;
        }
        if ($ip=='127.0.0.1')
            return 'Syria';
        $ip_data =  @unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
        if ($ip_data && $ip_data['country'] != null) {
            $result = $ip_data['country'];
            $result .= '|'.$ip_data['city'];
        }

        return $result;
    }
}
