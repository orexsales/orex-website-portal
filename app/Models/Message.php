<?php

namespace App\Models;

class Message extends \Eloquent
{
    protected $fillable = ['text', 'user', 'room_id', 'read_at'];
    protected $appends = ['author', 'ago', 'unread_messages'];

    public function getAuthorAttribute()
    {
        return $this->user;
    }

    public function getAgoAttribute()
    {
        return $this->created_at->shortAbsoluteDiffForHumans();
    }

    public function getUnreadMessagesAttribute()
    {
        return self::where([
            'room_id' => $this->room_id,
            'user'=>'me'
        ])->whereNull('read_at')->count();
    }
}
