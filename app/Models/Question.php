<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends \Eloquent
{
    protected $fillable = ['text_en','text_ar', 'answer_en','answer_ar'];
    protected $primaryKey="question_id";

}
