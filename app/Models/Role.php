<?php

namespace App\Models;


class Role extends \Eloquent
{
    protected $fillable=['name_en','name_ar'];
}
