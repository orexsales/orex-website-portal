<?php

namespace App\Models;


class Provider extends \Eloquent
{
    protected $fillable=['provider_id','user_id','provider'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
