<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FontAwesome extends \Eloquent
{
    protected $fillable = ['icon', 'content'];
    protected $primaryKey="font_id";
}
