<?php

namespace App\Models;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class Project extends \Eloquent
{

    use Cachable;
    const IMAGE_URL_PATH = 'images/projects/';
    const IMAGE_File_PATH = 'storage/images/projects/';

    protected $fillable = [
        'image', 'name_en', 'name_ar', 'description_ar', 'description_en',
    ];
    protected $appends = [
        'image_url',
    ];

    public function getImageUrlAttribute()
    {
        return isset($this->image) ? asset(self::IMAGE_File_PATH.$this->image) : asset('images/logo-default.jpg');
    }

    public function getImageFileSystem()
    {
        return storage_path('app/public/'.self::IMAGE_File_PATH.$this->image);
    }

}
