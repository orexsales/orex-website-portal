<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Rating extends \Eloquent
{
    protected $fillable = ['text', 'rating', 'name','email','product_id'];

    public function user()
    {
        return self::belongsTo(User::class);
    }

    public function product()
    {
     return self::belongsTo(Product::class,'product_id','product_id');
    }

}
