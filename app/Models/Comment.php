<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use SoftDeletes;
    protected $fillable = ['name', 'content', 'blog_id'];
    protected $table = "comments";
    protected $appends = ['ago'];

    public function blog()
    {
        return self::belongsTo(Blog::class, 'blog_id', 'blog_id');
    }

    public function getAgoAttribute()
    {
        return $this->created_at->diffForHumans();
    }

}
