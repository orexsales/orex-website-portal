<?php

namespace App\Models;

class Member extends \Eloquent
{
    public $timestamps=false;
    const IMAGE_URL_PATH = 'images/members/';
    const VIDEO_URL_PATH = 'videos/members/';

    protected $fillable = ['image',  'name_en', 'name_ar', 'position_ar', 'position_en', 'work_en', 'work_ar'];

    public function getImageUrlAttribute()
    {
        return isset($this->image)?asset(self::IMAGE_File_PATH . $this->image):asset('images/logo-default.jpg');
    }

    public function getImageFileSystem()
    {
        return storage_path('app/public/' . self::IMAGE_File_PATH . $this->image);
    }
}
