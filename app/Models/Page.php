<?php

namespace App\Models;


class Page extends \Eloquent
{
    protected $with=['seo'];
    protected $fillable = ['page_name','seo_id','page_template'];
    protected $primaryKey="page_id";
    const ADMIN_PAGE='admin';
    const PROG_PAGE='prog';

    public function attributePage()
    {
        return self::hasMany(Attribute_page::class,'page_id','page_id');
    }

    public function attribute()
    {
        return self::belongsToMany(Attribute::class,'attribute_page','page_id','attribute_id')->withPivot('attribute_value');
    }

    public function seo()
    {
        return self::belongsTo(Seo::class);
    }

    public function getPageByCurrentRoute($pagename)
    {
       return self::where('page_name','like',$pagename)->first();
    }
}
