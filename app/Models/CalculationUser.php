<?php


namespace App\Models;


class CalculationUser extends \Eloquent
{
    protected $fillable = ['email'];
}
