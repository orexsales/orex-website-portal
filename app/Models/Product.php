<?php

namespace App\Models;

use Storage;

class Product extends \Eloquent
{

//    use Cachable;
    const IMAGE_URL_PATH = 'images/products/';
    const VIDEO_URL_PATH = 'videos/products/';
    const IMAGE_File_PATH = 'storage/images/products/';
    const VIDEO_File_PATH = 'storage/videos/products/';

    protected $fillable = [
        'video', 'image', 'category_id', 'name_en', 'name_ar','name_fr', 'description_ar','description_fr', 'description_en', 'price', 'sale',
        'available_aleppo', 'available_china', 'available_dubai', 'slug_en', 'slug_ar', 'sizes', 'is_new','views',
    ];
    protected $appends = [
        'image_url', 'all_rating', 'images', 'description_html_en', 'description_html_ar', 'name', 'description',
    ];
    protected $primaryKey = "product_id";

    protected $casts = ['sizes' => 'array'];

    public function category()
    {
        return self::belongsTo(Category::class, 'category_id', 'category_id');
    }

    public function rating()
    {
        return self::hasMany(Rating::class, 'product_id', 'product_id');
    }

    public function getImageUrlAttribute()
    {
        return isset($this->image) ? config('path.WEB_BASE_URL') . self::IMAGE_File_PATH . $this->image : asset('images/logo-default.jpg');
    }

    public function getImageFileSystem()
    {
        return storage_path('app/public/' . self::IMAGE_File_PATH . $this->image);
    }

    public function getVideoFileSystem()
    {
        return storage_path('app/public/' . self::VIDEO_File_PATH . $this->video);
    }


    public function getAvailableAttribute()
    {
        $available = [];
        if ($this->available_aleppo != 'N') {
            $available[] = trans('app.aleppo');
        }

        if ($this->available_dubai != 'N') {
            $available[] = trans('app.dubai');
        }

        if ($this->available_china != 'N') {
            $available[] = trans('app.china');
        }

        return $available;
    }

    public function getSalePriceAttribute()
    {
        if ($this->sale) {
            $percent = ($this->price * $this->sale) / 100;
            $newPrice = $this->price - $percent;
            return $newPrice;
        }
    }

    public function getAllRatingAttribute()
    {
        $ratings = $this->rating;
        $avg = 0;
        if (count($ratings)) {
            foreach ($ratings as $rating) {
                $avg += $rating->rating;
            }
            $avg = $avg / count($ratings);
            return ceil($avg);
        } else {
            return $avg;
        }
    }


    public function getImagesAttribute()
    {
        return implode(',', $this->getFiles($this->name_en));
    }

    public function getFiles($product)
    {
        if ($product) {
            $files = Storage::disk('public')->files('images/products');
            $urlFile = [];
            $file = str_replace(' ', '_', str_replace('/', '-', strtolower($product)));

            $foundFiles = collect($files)->filter(function ($v) use ($file) {
                return preg_match("/_$file/", $v);
            });
            if ($foundFiles->count() > 0) {
                foreach ($foundFiles as $index => $file) {
                    $urlFile[] = asset("storage/$file");
                }
            }
            return $urlFile;
        }
        return ['success' => false];

    }

    public function getDescriptionHtmlEnAttribute()
    {
        return strip_tags($this->description_en, 'p,div,a,h1,h2,h3,h4,h5,h6,span,table,th,td,tr,tfoot,tbody,thead');
    }

    public function getDescriptionHtmlArAttribute()
    {
        return strip_tags($this->description_ar, 'p,div,a,h1,h2,h3,h4,h5,h6,span,table,th,td,tr,tfoot,tbody,thead');
    }

    public function getNameAttribute()
    {
        return $this['name_' . \App::getLocale()];
    }

    public function getDescriptionAttribute()
    {
        return $this['description_' . \App::getLocale()];
    }


}
