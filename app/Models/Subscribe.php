<?php

namespace App\Models;

class Subscribe extends \Eloquent
{
    protected $fillable=['email'];
    protected $table="subscribes";
}
