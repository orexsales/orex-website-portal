<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkSchedule extends \Eloquent
{
    use SoftDeletes;
    protected $primaryKey = "work_schedule_id";
    protected $fillable = ['day', 'schedule_from', 'schedule_to'];
    protected $dates = ['deleted_at', 'update_at', 'created_at'];

    public function getScheduleFromAttribute($value)
    {
        return $value ? date('H:i a', strtotime($value)) : null;
    }

    public function getScheduleToAttribute($value)
    {
        return $value ? date('H:i a', strtotime($value)) : null;
    }

    public function getFromTo()
    {
        if ($this->schedule_from) {
            $date = $this->schedule_from.'-'.$this->schedule_to;
            $date = str_replace('am', trans('app.am'), $date);
            $date = str_replace('pm', trans('app.pm'), $date);
            return $date;
        } else return '<span class="color t600">' . trans('app.closed') . '</span>';
    }

    public function update(array $attributes = [], array $options = [])
    {
        if (!$attributes['schedule_from']) $attributes['schedule_from'] = null;
        if (!$attributes['schedule_to']) $attributes['schedule_to'] = null;
        return parent::update($attributes, $options);
    }
}
