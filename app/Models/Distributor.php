<?php

namespace App\Models;

class Distributor extends \Eloquent
{
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'company_name',
        'company_description',
        'website_url',
        'since',
        'address',
        'country',
        'state',
        'city',
        'type_of_business',
        'interested_in'
    ];
    protected $casts = [
        'type_of_business' => 'array',
        'interested_in' => 'array'
    ];
}
