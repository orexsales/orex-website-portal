<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Style extends \Eloquent
{
    protected $fillable = [ 'value'];
    protected $primaryKey="style_id";
}
