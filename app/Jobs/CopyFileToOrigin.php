<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CopyFileToOrigin implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $source;
    protected $target;

    /**
     * CopyFileToOrigin constructor.
     * @param $source
     * @param $target
     */
    public function __construct($source, $target)
    {
        $this->source = $source;
        $this->target = $target;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \File::copy($this->source, $this->target);
    }
}
