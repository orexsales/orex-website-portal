<?php

namespace App\Factories;

use App\Models\Product;
use Aut\DataTable\Factories\GlobalFactory;

class WorkScheduleFactory extends GlobalFactory
{

    /**
     *  get datatable query
     */
    public function getDatatable($model, $request)
    {
        $query = $model::all();
        return \Datatable::queryConfig('work-schedules')
            ->queryDatatable($query)
            ->queryUpdateButton('work_schedule_id')
            ->queryDeleteButton('work_schedule_id')
            ->queryRender();
    }

    /**
     *  build datatable modal and table
     */
    public function buildDatatable($model, $request)
    {
        try {
            return \Datatable::config('work-schedules')
                ->addHiddenInput('work_schedule_id', 'work_schedule_id', '', true)
                ->addInputText(trans('app.day'), 'day', 'day', 'req required')
                ->addInputText(trans('app.schedule_from'), 'schedule_from', 'schedule_from', 'req required d:time')
                ->addInputText(trans('app.schedule_to'), 'schedule_to', 'schedule_to', 'req required d:time')
                ->addActionButton($this->update, 'update', 'update')
                ->addActionButton($this->delete, 'delete', 'delete')
                ->addNavButton([],['code'])
                ->onModalOpen('_timePicker($(modal))')
                ->render();
        } catch (\Exception $e) {
        }
    }

    /**
     *  store action for save relation
     */
    public function storeDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  store action for update relation
     */
    public function updateDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  store action for destroy relation
     */
    public function destroyDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  inline validate dialog form
     */
    public function validateDatatable()
    {
        return [];
    }
}
