<?php

namespace App\Factories;

use App\Models\Category;
use Aut\DataTable\Factories\GlobalFactory;

class CategoryFactory extends GlobalFactory
{

    /**
     *  get datatable query
     */
    public function getDatatable($model, $request)
    {

        $query = $model::orderBy('category_order')->with(['parent']);
        return \Datatable::queryConfig('categories')
            ->queryDatatable($query)
            ->queryUpdateButton('category_id')
            ->queryDeleteButton('category_id')
            ->queryAddColumn('icon_name', function ($item) {
                return "<em class='$item->icon'></em>";
            })
            ->queryAddColumn('icon_names', function ($item) {
                return "<em class='$item->icon'> $item->icon</em>";
            })
            ->queryAddColumn('image', function ($item) use ($model) {
                if (isset($item->image))
                    return '<img width="350" height="200" src="' . ($item->image ? $item->image_url : '') . '" />';
                else
                    return '';
            })
            ->queryCustomButton('update_image', 'id', 'fa fa-image', '', "href='javascript:void(0)' onclick='admin_update_image(this)'")
            ->queryRender();
    }

    /**
     *  build datatable modal and table
     */
    public function buildDatatable($model, $request)
    {
        try {
            return \Datatable::config('categories', '', ['gridSystem' => true, 'dialogWidth' => '40%'])
                ->addHiddenInput('category_id', 'category_id', '', true)
                ->addInputText(trans('app.name_en'), 'name_en', 'name_en', 'en d:en req required ')
                 ->addInputText(trans('app.name_fr'), 'name_fr', 'name_fr', 'en d:en req required ')
                ->addInputText(trans('app.name_ar'), 'name_ar', 'name_ar', 'ar d:ar req required ',"data-float=left")
                ->addAutocomplete('admin/icons-autocomplete', trans('app.icon'), 'icon', 'icon', 'icon_names', '', "data-float=left", '')
                ->addAutocomplete('admin/categories-autocomplete', trans('app.parent'), 'parent_id', 'parent_id', 'parent.name_' . \App::getLocale(), '', "data-float=left",'')
                ->addInputText(trans('app.code'), 'code', 'code', 'req required ',"data-float=left")
                ->addInputNumber(trans('app.order'), 'category_order', 'category_order', 'req required none')
                ->setGridNormalCol(12)
                ->startHorizontalTab()
                ->openHorizontalTab('content1', trans('app.content_en'), '', true)
                ->addTextArea('', 'content_en', 'content_en', 'en d:en d:text-editor none')
                ->closeHorizontalTab()
                  ->openHorizontalTab('content3', trans('app.content_fr'), 'none')
                ->addTextArea('', 'content_fr', 'content_fr', 'en d:en d:text-editor none')
                ->closeHorizontalTab()
                ->openHorizontalTab('content2', trans('app.content_ar'), 'none')
                ->addTextArea('', 'content_ar', 'content_ar', 'ar d:ar d:text-editor none')
                ->closeHorizontalTab()
                ->endHorizontalTab()
                ->addActionButton(trans('app.update_image'), 'update_image', 'update_image')
                ->addActionButton($this->update, 'update', 'update')
                ->addActionButton($this->delete, 'delete', 'delete')
                ->addNavButton([], ['code'])
                ->onModalOpen("_textEditor($(modal ));")
                ->render();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     *  store action for save relation
     */
    public function storeDatatable($model = null, $request = null, $result = null)
    {
        if (isset($request['name_en']))
            $request['slug'] = str_slug($request['name_en']);

        Category::create($request->input());
    }

    /**
     *  store action for update relation
     */
    public function updateDatatable($model = null, $request = null, $result = null)
    {
        $tag = Category::findOrFail($request->category_id);
        if (isset($request['name_en']))
            $request['slug'] = str_slug($request['name_en']);
        $tag->update($request->input());
    }

    /**
     *  store action for destroy relation
     */
    public function destroyDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  inline validate dialog form
     */
    public function validateDatatable()
    {
        return [];
    }
}
