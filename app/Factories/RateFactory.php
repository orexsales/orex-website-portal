<?php

namespace App\Factories;

use App\Models\Product;
use Aut\DataTable\Factories\GlobalFactory;

class RateFactory extends GlobalFactory
{

    /**
     *  get datatable query
     */
    public function getDatatable($model, $request)
    {
        $query = $model::with(['product'])->get();
        return \Datatable::queryConfig('ratess')
            ->queryDatatable($query)
            ->queryUpdateButton('id')
            ->queryDeleteButton('id')
//            ->queryMultiDeleteButton('id')
            ->queryRender();
    }

    /**
     *  build datatable modal and table
     */
    public function buildDatatable($model, $request)
    {
        try {
            return \Datatable::config('ratess','',['gridSystem'=>true,'dialogWidth'=>'40%'])
                ->addHiddenInput('id', 'id', '', true)
                ->addInputText(trans('app.name'), 'name', 'name', 'req required ')
                ->addInputText(trans('app.email'), 'email', 'email', 'req required')
                ->addInputText(trans('app.rating'), 'rating', 'rating', 'req required')
                ->addInputText(trans('app.text'), 'text', 'text', 'req required none')
                ->addAutocomplete('admin/products-autocomplete',trans('app.product'), 'product_id', 'product_id', 'product.name_'.\App::getLocale(),'req required','data-float=left')
                ->addActionButton($this->update, 'update', 'update')
                ->addActionButton($this->delete, 'delete', 'delete')
//                ->addActionButton($this->delete, 'multi-delete', 'multi-delete')
                ->addNavButton([],['code'])
                ->render();
        } catch (\Exception $e) {
        }
    }

    /**
     *  store action for save relation
     */
    public function storeDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  store action for update relation
     */
    public function updateDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  store action for destroy relation
     */
    public function destroyDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  inline validate dialog form
     */
    public function validateDatatable()
    {
        return [];
    }
}
