<?php

namespace App\Factories;

use App\Models\Product;
use Aut\DataTable\DataTableBuilder;
use Aut\DataTable\Factories\GlobalFactory;

class ContactMailFactory extends GlobalFactory
{
        protected $yesNo;
    public function __construct(DataTableBuilder $table)
    {
        $this->yesNo = [
            'Y' => trans('app.yes'),
            'N' => trans('app.no')
        ];
        parent::__construct($table);
    }

    /**
     *  get datatable query
     */
    public function getDatatable($model, $request)
    {
        $query = $model::orderBy('is_answered');
        return \Datatable::queryConfig('contact')
            ->queryDatatable($query)
            ->queryAddColumn('answered',function ($item){
                return '<span data-answered='.$item->is_answered.'>'.$this->yesNo[$item->is_answered].'</span>';
            })
            ->queryCustomButton('answer_btn','id','zmdi zmdi-comment-alt-text zmdi-hc-fw','',"href='javascript:void(0)' onclick='_open_answer($(this))'")
            ->queryRender();
    }

    /**
     *  build datatable modal and table
     */
    public function buildDatatable($model, $request)
    {
        try {
            return \Datatable::config('contact','',['gridSystem'=>true,'dialogWidth'=>'40%'])
                ->addHiddenInput('id', 'id', '', true)
                ->addInputText(trans('app.f_name'), 'f_name', 'f_name', 'req required ')
                ->addInputText(trans('app.l_name'), 'l_name', 'l_name', 'req required')
                ->addInputText(trans('app.phone'), 'phone', 'phone', 'req required d:input-mask',['data-type=phone','maxlength=19'])
                ->addInputText(trans('app.email'), 'email', 'email', 'req required')
                ->addInputText(trans('app.message'), 'message', 'message', 'req required')
                ->addViewField(trans('app.answered'), 'answerer', 'answerer', 'answered','req required ')
                ->addActionButton(trans('app.answer'),'answer_btn','answer_btn')
                ->onLoad("
                $('[data-answered=N]').parent().parent().css('color','red')")
                ->addNavButton([],['code'])
                ->render();
        } catch (\Exception $e) {
        }
    }

    /**
     *  store action for save relation
     */
    public function storeDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  store action for update relation
     */
    public function updateDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  store action for destroy relation
     */
    public function destroyDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  inline validate dialog form
     */
    public function validateDatatable()
    {
        return [];
    }
}
