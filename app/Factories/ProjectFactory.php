<?php

namespace App\Factories;

use Aut\DataTable\DataTableBuilder;
use Aut\DataTable\Factories\GlobalFactory;

class ProjectFactory extends GlobalFactory
{
    protected $yesNo = [];
    protected $sizes = [];

    public function __construct(DataTableBuilder $table)
    {
        parent::__construct($table);
    }

    /**
     *  get datatable query
     * @param  \Eloquent  $model
     * @param $request
     * @return mixed
     * @throws \Exception
     */
    public function getDatatable($model, $request)
    {
        $query = $model::get();
        return \Datatable::queryConfig('projects')
            ->queryDatatable($query)
            ->queryUpdateButton('id')
            ->queryDeleteButton('id')
            ->queryCustomButton('main_image', 'id', 'fa fa-file-photo-o (', '',
                "href='javascript:void(0)' onclick='admin_update_main_image(this)'")
            ->queryCustomButton('update_image', 'product_id', 'fa fa-image', '',
                "href='javascript:void(0)' onclick='admin_update_image(this)'")
            ->queryRender();
    }

    /**
     *  build datatable modal and table
     */
    public function buildDatatable($model, $request)
    {
        try {
            return \Datatable::config('projects', '', ['gridSystem' => true])
                ->addHiddenInput('id', 'id', '', true)
                ->addInputText(trans('app.name_en'), 'name_en', 'name_en', ' en d:en req required ')
                ->addInputText(trans('app.name_ar'), 'name_ar', 'name_ar', ' ar d:ar req required ')
                ->setGridNormalCol(12)
                ->startHorizontalTab()
                ->openHorizontalTab('description1', trans('app.description_en'), '', true)
                ->addTextArea('', 'description_en', 'description_en', 'en d:en d:text-editor none')
                ->closeHorizontalTab()
                ->openHorizontalTab('description2', trans('app.description_ar'), 'none')
                ->addTextArea('', 'description_ar', 'description_ar', 'ar d:ar d:text-editor none')
                ->closeHorizontalTab()
                ->endHorizontalTab()
                ->addActionButton(trans('app.upload_images'), 'update_image', 'update_image')
                ->addActionButton(trans('app.main_image'), 'main_image', 'main_image')
                ->addActionButton($this->update, 'update', 'update')
                ->addActionButton($this->delete, 'delete', 'delete')
                ->addNavButton([], ['code'])
                ->onModalOpen("_textEditor($(modal ));
                ")
                ->render();
        } catch (\Exception $e) {
        }
    }

    /**
     *  store action for save relation
     */
    public function storeDatatable($model = null, $request = null, $result = null)
    {

    }

    /**
     *  store action for update relation
     */
    public function updateDatatable($model = null, $request = null, $result = null)
    {
    }

    /**
     *  store action for destroy relation
     */
    public function destroyDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  inline validate dialog form
     */
    public function validateDatatable()
    {
        return [];
    }
}
