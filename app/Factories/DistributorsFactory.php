<?php

namespace App\Factories;

use Aut\DataTable\Factories\GlobalFactory;

class DistributorsFactory extends GlobalFactory
{

    /**
     *  get datatable query
     */
    public function getDatatable($model, $request)
    {
        $query = $model::all();
        return \Datatable::queryConfig('distributors')
            ->queryDatatable($query)
            ->queryAddcolumn('type_of_business_view', function ($item) {
                $type_of_business = '';
                foreach ($item->type_of_business as $i) {
                    $type_of_business .= '<span class="badge badge-primary mx-2" style="font-size: 15px">'.$i.'</span>';
                }
                return $type_of_business;
            })
            ->queryAddcolumn('interested_in_view', function ($item) {
                $interested_in = '';
                foreach ($item->interested_in as $i) {
                    $interested_in .= '<span class="badge badge-primary mx-2" style="font-size: 15px">'.$i.'</span>';
                }
                return $interested_in;
            })
            ->queryRender();
    }

    /**
     *  build datatable modal and table
     */
    public function buildDatatable($model, $request)
    {
        try {
            return \Datatable::config('distributors', '', ['gridSystem' => true, 'dialogWidth' => '40%'])
                ->addHiddenInput('client_id', 'client_id', '', true)
                ->addInputText(trans('app.first_name'), 'first_name', 'first_name', 'req required')
                ->addInputText(trans('app.last_name'), 'last_name', 'last_name', 'req required')
                ->addInputText(trans('app.email'), 'email', 'email', 'req required')
                ->addInputText(trans('app.phone'), 'phone', 'phone', 'req required')
                ->addInputText(trans('app.company_name'), 'company_name', 'company_name', 'req required')
                ->addInputText(trans('app.company_description'), 'company_description', 'company_description',
                    'req required')
                ->addInputText(trans('app.website_url'), 'website_url', 'website_url', 'req required')
                ->addInputText(trans('app.since'), 'since', 'since', 'req required')
                ->addInputText(trans('app.address'), 'address', 'address', 'req required')
                ->addInputText(trans('app.country'), 'country', 'country', 'req required')
                ->addInputText(trans('app.state'), 'state', 'state', 'req required')
                ->addInputText(trans('app.city'), 'city', 'city', 'req required')
                ->addViewField(trans('app.type_of_business'), 'type_of_business_view', 'type_of_business_view', '', 'none')
                ->addViewField(trans('app.interested_in'), 'interested_in_view', 'interested_in_view', '', 'none')
                ->addNavButton([], ['code', 'add'])
                ->render();
        } catch (\Exception $e) {
        }
    }

    /**
     *  store action for save relation
     */
    public function storeDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  store action for update relation
     */
    public function updateDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  store action for destroy relation
     */
    public function destroyDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  inline validate dialog form
     */
    public function validateDatatable()
    {
        return [];
    }
}
