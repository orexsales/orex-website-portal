<?php

namespace App\Factories;

use App\User;
use Aut\DataTable\DataTableBuilder;
use Aut\DataTable\Factories\GlobalFactory;

class UsersFactory extends GlobalFactory
{
    protected $types;
    public function __construct(DataTableBuilder $table)
    {
        parent::__construct($table);
        $this->types=[
            '1'=>'Admin',
            '2'=>'User',
            '3'=>'Support'
        ];
    }

    /**
     *  get datatable query
     */
    public function getDatatable($model, $request)
    {
        $query = $model::all();

        return $this->table
            ->queryConfig('users')
            ->queryDatatable($query)
       
            ->queryDeleteButton('id')
            ->queryAddColumn('type_view',function ($item){
                return $this->types[$item->user_type_id];
            })
         
            ->queryRender(true);
    }

    /**
     *  build datatable modal and table
     */
    public function buildDatatable($model, $request)
    {
        try {
            return $this->table
                ->config('users','',['gridSystem'=>true,'dialogWidth'=>'40%'])
                ->addHiddenInput('id', 'id', '', true)
                ->addInputText(trans('app.name'), 'name', 'name', ' required req')
                ->addInputText(trans('app.email'), 'email', 'email', ' required req')
                 ->addInputText(trans('app.phone'), 'phone', 'phone', ' required req')
                  ->addInputText(trans('app.created_at'), 'created_at', 'created_at', ' required req')
                ->addSelect($this->types,trans('app.user_type'),'user_type_id','user_type_id','type_view','d:select2 d:show-tick','data-style=btn-info')
                //->addMultiAutocomplete('admin/roles-autocomplete','roles[]',trans('app.roles'),'roles','roles','roles_view','none')
              
                ->addActionButton($this->delete, 'delete', 'delete')
                ->addNavButton([],['add','code'])
                ->onModalOpen('
                _select()')
                ->render();
        } catch (\Exception $e) {
        }

    }//
    public function storeDatatable($model = null, $request = null, $result = null)
    {
    }

    /**
     *  store action for update relation
     */
    public function updateDatatable($model = null, $request = null, $result = null)
    {
        $uesr=User::find($request->id);
        $uesr->update($request->input());
        $uesr->roles()->sync($request->get('roles'));
    }
}
