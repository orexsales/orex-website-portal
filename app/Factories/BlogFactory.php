<?php

namespace App\Factories;

use App\Models\Blog;
use App\Models\Product;
use Aut\DataTable\Factories\GlobalFactory;

class BlogFactory extends GlobalFactory
{

    /**
     *  get datatable query
     */
    public function getDatatable($model, $request)
    {
        $query = $model::with(['tag'])->get();
        return  \Datatable::queryConfig('blogs')
            ->queryDatatable($query)
            ->queryUpdateButton('blog_id')
            ->queryDeleteButton('blog_id')
            ->queryCustomButton('upload_file', 'blog_id', 'fa fa-file', '', "href='javascript:void(0)' onclick='_open_file_upload(this)'")
            ->queryCustomButton('update_image', 'blog_id', 'fa fa-image', '', "href='javascript:void(0)' onclick='admin_update_image(this)'")
            ->queryAddColumn('image',function ($item) use($model){
                return '<img width="300" height="200" src="' . ($item->image ? asset(Blog::IMAGE_File_PATH . $item->image) : '') . '" />';
            })
            ->queryRender();
    }

    /**
     *  build datatable modal and table
     */
    public function buildDatatable($model, $request)
    {
        try {
            return \Datatable::config('blogs','', ['gridSystem' => true])
                ->addHiddenInput('blog_id', 'blog_id', '', true)
                ->addInputText(trans('app.title').' '.trans('app._en'), 'title_en', 'title_en', 'en d:en req required')
                ->addInputText(trans('app.title').' '.trans('app._ar'), 'title_ar', 'title_ar', 'ar d:ar req required')
                 ->addInputText(trans('app.title_fr'), 'title_fr', 'title_fr', 'en d:en req required')
//                ->addInputText(trans('app.username'), 'username', 'username', 'req required')
                ->setGridNormalCol(12)
                ->startHorizontalTab()
                ->openHorizontalTab('content1', trans('app.content_en'), '', true)
                ->addTextArea('', 'content_en', 'content_en', ' none en d:en d:text-editor')
                ->closeHorizontalTab()
                  ->openHorizontalTab('content3', trans('app.content_fr'), '')
                ->addTextArea('', 'content_fr', 'content_fr', ' none en d:en d:text-editor')
                ->closeHorizontalTab()
                ->openHorizontalTab('content2', trans('app.content_ar'), '')
                ->addTextArea('', 'content_ar', 'content_ar', ' none ar d:ar d:text-editor')
                ->closeHorizontalTab()
                ->endHorizontalTab()
                ->addAutocomplete('admin/tags-autocomplete', trans('app.tags'), 'tag_id', 'tag_id', 'tag.name_' . \App::getLocale(),'req required','data-float=left')
                ->addViewField(trans('app.image'),'image', 'image', '', 'none')
                ->addActionButton(trans('app.update_image'), 'update_image', 'update_image')
                ->addActionButton(trans('app.upload_doc'), 'upload_file', 'upload_file')
                ->addActionButton($this->update, 'update', 'update')
                ->addActionButton($this->delete, 'delete', 'delete')
                ->addNavButton([],['code'])
                ->onModalOpen('_textEditor($(modal))')
                ->render();
        } catch (\Exception $e) {
        }
    }

    /**
     *  store action for save relation
     */
    public function storeDatatable($model = null, $request = null, $result = null)
    {
        if (isset($request['title_en']))
            $request['slug_en']=str_slug($request['title_en']);

        if (isset($request['title_ar']))
            $request['slug_ar']=str_replace(' ','-',$request['title_ar']);
        Blog::create($request->input()+['user_id'=>\Auth::id()]);
    }

    /**
     *  store action for update relation
     */
    public function updateDatatable($model = null, $request = null, $result = null)
    {
        $blog=Blog::findOrFail($request->blog_id);
        if (isset($request['title_en']))
            $request['slug_en']=str_slug($request['title_en']);

        if (isset($request['title_ar']))
            $request['slug_ar']=str_replace(' ','-',$request['title_ar']);
        $blog->update($request->input());
    }

    /**
     *  store action for destroy relation
     */
    public function destroyDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  inline validate dialog form
     */
    public function validateDatatable()
    {
        return [];
    }
}
