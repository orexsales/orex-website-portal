<?php

namespace App\Factories;

use App\Events\ProductEvent;
use App\Models\Product;
use Aut\DataTable\DataTableBuilder;
use Aut\DataTable\Factories\GlobalFactory;

class ProductFactory extends GlobalFactory
{
    protected $yesNo = [];
    protected $sizes = [];

    public function __construct(DataTableBuilder $table)
    {
        $this->yesNo = [
            'Y' => trans('app.yes'),
            'N' => trans('app.no')
        ];
        $this->sizes = ['S' => trans('app.S'), 'M' => trans('app.M'), 'L' => trans('app.L')];
        parent::__construct($table);
    }

    /**
     *  get datatable query
     */
    public function getDatatable($model, $request)
    {
        $query = $model::with(['category']);
        return \Datatable::queryConfig('products')
            ->queryDatatable($query)
            ->queryUpdateButton('product_id')
            ->queryDeleteButton('product_id')
            ->queryCustomButton('update_image', 'product_id', 'fa fa-image', '',
                "href='javascript:void(0)' onclick='admin_update_image(this)'")
            ->queryCustomButton('main_image', 'product_id', 'fa fa-file-photo-o (', '',
                "href='javascript:void(0)' onclick='admin_update_main_image(this)'")
            ->queryCustomButton('upload_file', 'product_id', 'fa fa-file', '',
                "href='javascript:void(0)' onclick='_open_file_upload(this)'")
            ->queryCustomButton('upload_video', 'product_id', 'fa fa-video-camera', '',
                "href='javascript:void(0)' onclick='admin_upload_video(this)'")
            ->queryAddColumn('image', function ($item) use ($model) {
                if (isset($item->image)) {
                    return '<img width="350" height="200" src="'.($item->image ? asset(Product::IMAGE_File_PATH.$item->image) : '').'" />';
                } else {
                    return '';
                }
            })
            ->queryAddColumn('aleppo', function ($item) use ($model) {
                if ($item->available_aleppo) {
                    return $this->yesNo[$item->available_aleppo];
                } else {
                    return $this->yesNo['Y'];
                }
            })
            ->queryAddColumn('dubai', function ($item) use ($model) {
                if ($item->available_dubai) {
                    return $this->yesNo[$item->available_dubai];
                } else {
                    return $this->yesNo['Y'];
                }
            })
            ->queryAddColumn('china', function ($item) use ($model) {
                if ($item->available_china) {
                    return $this->yesNo[$item->available_china];
                } else {
                    return $this->yesNo['Y'];
                }
            })
            ->queryAddColumn('is_new_view', function ($item) use ($model) {
                if ($item->is_new) {
                    return $this->yesNo[$item->is_new];
                } else {
                    return $this->yesNo['Y'];
                }
            })
//            ->queryAddColumn('sizes_view', function ($item) {
//                $data = [];
//                $sizes = explode(',', $item->sizes);
//                foreach ($sizes as $key => $value) {
//                    $data[] = ['id' => $value, 'name' => trans('app.' . $value)];
//                }
//                return ['items' => $data];
//            })
            ->queryRender();
    }

    /**
     *  build datatable modal and table
     */
    public function buildDatatable($model, $request)
    {
        try {
            return \Datatable::config('products', '', ['gridSystem' => true])
                ->addHiddenInput('product_id', 'product_id', '', true)
                ->addInputText(trans('app.name_en'), 'name_en', 'name_en', ' en d:en req required ')
                ->addInputText(trans('app.name_fr'), 'name_fr', 'name_fr', ' fr d:fr req required none ')
                ->addInputText(trans('app.name_ar'), 'name_ar', 'name_ar', ' ar d:ar req required ')
//                ->addInputText(trans('app.price'), 'price', 'price', 'none')
//                ->addInputText(trans('app.sale'), 'sale', 'sale', 'none')
                ->addAutocomplete('admin/categories-autocomplete', trans('app.category'), 'category_id', 'category_id',
                    'category.name_'.\App::getLocale(), 'req required', "data-float=true")
//                ->addMultiAutocomplete('admin/sizes-autocomplete', 'size[]', trans('app.sizes'), 'size', 'size', 'sizes_view', 'req required', "data-float=true")
                ->setGridNormalCol(12)
                ->startHorizontalTab()
                ->openHorizontalTab('description1', trans('app.description_en'), 'none en', true)
                ->addTextArea('', 'description_en', 'description_en', 'en d:en d:text-editor none')
                ->closeHorizontalTab()
                ->openHorizontalTab('description3', trans('app.description_fr'), 'none fr')
                ->addTextArea('', 'description_fr', 'description_fr', 'fr d:fr d:text-editor none')
                ->closeHorizontalTab()
                ->openHorizontalTab('description2', trans('app.description_ar'), 'none ar')
                ->addTextArea('', 'description_ar', 'description_ar', 'ar d:ar d:text-editor none')
                ->closeHorizontalTab()
                ->endHorizontalTab()
//                ->addViewField(trans('apa.main_image'), 'image', 'image', '', '')
                ->addActionButton(trans('app.upload_images'), 'update_image', 'update_image')
                ->addActionButton(trans('app.main_image'), 'main_image', 'main_image')
                ->addActionButton(trans('app.upload_video'), 'upload_video', 'upload_video')
                ->addActionButton(trans('app.upload_doc'), 'upload_file', 'upload_file')
                ->addActionButton($this->update, 'update', 'update')
                ->addActionButton($this->delete, 'delete', 'delete')
                ->addNavButton([], ['code'])
                ->onModalOpen("_textEditor($(modal ));_select()
//                $('button.dropdown-toggle').each(function () {
//                    $(this).on('click', function () {
//                        console.log($(this).attr('title'));
//                        $(this).parent().find('div.dropdown-menu.open').toggleClass('show')
//                    })
//                })
                ")
                ->render();
        } catch (\Exception $e) {
        }
    }

    /**
     *  store action for save relation
     */
    public function storeDatatable($model = null, $request = null, $result = null)
    {
        if (isset($request['name_en'])) {
            $request['slug_en'] = str_slug($request['name_en']);
        }

        if (isset($request['name_ar'])) {
            $request['slug_ar'] = str_replace(' ', '-', $request['name_ar']);
        }

        $product = Product::create($request->input());
        //   event(new ProductEvent($product));
    }

    /**
     *  store action for update relation
     */
    public function updateDatatable($model = null, $request = null, $result = null)
    {
        $product = Product::findOrFail($request->product_id);
        if (isset($request['name_en'])) {
            $request['slug_en'] = str_slug($request['name_en']);
        }

        if (isset($request['name_ar'])) {
            $request['slug_ar'] = str_replace(' ', '-', $request['name_ar']);
        }
        if (isset($request['size'])) {
            $sizes = $request['size'];
            foreach ($sizes as $key => $value) {
                if ($key == count($sizes) - 1) {
                    $request['sizes'] .= $value;
                } else {
                    $request['sizes'] .= $value.',';
                }
            }
        }
        $product->update($request->input());
    }

    /**
     *  store action for destroy relation
     */
    public function destroyDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  inline validate dialog form
     */
    public function validateDatatable()
    {
        return [];
    }
}
