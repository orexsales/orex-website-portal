<?php

namespace App\Factories;

use Aut\DataTable\DataTableBuilder;
use Aut\DataTable\Factories\GlobalFactory;

class MemberFactory extends GlobalFactory
{
    protected $yesNo = [];
    protected $sizes = [];

    public function __construct(DataTableBuilder $table)
    {
        $this->yesNo = [
            'Y' => trans('app.yes'),
            'N' => trans('app.no')
        ];
        $this->sizes = ['S' => trans('app.S'), 'M' => trans('app.M'), 'L' => trans('app.L')];
        parent::__construct($table);
    }

    /**
     *  get datatable query
     */
    public function getDatatable($model, $request)
    {
        $query = $model::all();
        return \Datatable::queryConfig('members')
            ->queryDatatable($query)
            ->queryUpdateButton('id')
            ->queryDeleteButton('id')
            ->queryCustomButton('update_image', 'id', 'fa fa-image', '', "href='javascript:void(0)' onclick='admin_update_image(this)'")
            ->queryRender();
    }

    /**
     *  build datatable modal and table
     */
    public function buildDatatable($model, $request)
    {
        try {
            return \Datatable::config('members', '', ['gridSystem' => true])
                ->addHiddenInput('id', 'id', '', true)
                ->addInputText(trans('app.name_en'), 'name_en', 'name_en', ' en d:en')
                ->addInputText(trans('app.name_ar'), 'name_ar', 'name_ar', ' ar d:ar')
                ->addInputText(trans('app.work').' '.trans('app._en'), 'work_en', 'work_en', ' en d:en')
                ->addInputText(trans('app.work').' '.trans('app._en'), 'work_ar', 'work_ar', ' ar d:ar')
                ->addActionButton(trans('app.update_image'), 'update_image', 'update_image')
                ->addActionButton($this->update, 'update', 'update')
                ->addActionButton($this->delete, 'delete', 'delete')
                ->addNavButton([], ['code'])
                ->onModalOpen("_textEditor($(modal ));_select()
//                $('button.dropdown-toggle').each(function () {
//                    $(this).on('click', function () {
//                        console.log($(this).attr('title'));
//                        $(this).parent().find('div.dropdown-menu.open').toggleClass('show')
//                    })
//                })
                ")
                ->render();
        } catch (\Exception $e) {
        }
    }

    /**
     *  store action for save relation
     */
    public function storeDatatable($model = null, $request = null, $result = null)
    {

    }

    /**
     *  store action for update relation
     */
    public function updateDatatable($model = null, $request = null, $result = null)
    {

    }

    /**
     *  store action for destroy relation
     */
    public function destroyDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  inline validate dialog form
     */
    public function validateDatatable()
    {
        return [];
    }
}
