<?php

namespace App\Factories;

use App\Models\Style;
use Aut\DataTable\DataTableBuilder;
use Aut\DataTable\Factories\GlobalFactory;

class StyleFactory extends GlobalFactory
{

    protected $styles=[];
    public function __construct(DataTableBuilder $table)
    {
        parent::__construct($table);
        $this->styles=[
            'green'=>'Green',
            'blue'=>'Blue',
            'red'=>'Red',
            'orange'=>'Orange',
            'teal'=>'Teal',
            'cyan'=>'Cyan',
            'blue-grey'=>'Blue Grey',
            'purple'=>'Purple',
            'indigo'=>'Indigo',
            'lime'=>'Lime',
        ];
    }

    /**
     *  get datatable query
     */
    public function getDatatable($model, $request)
    {
        $query = $model::all();
        return \Datatable::queryConfig('styles')
            ->queryDatatable($query)
            ->queryUpdateButton('style_id')
            ->queryAddColumn('style',function ($item){
                return $this->styles[$item->value];
            })
            ->queryAddColumn('color',function ($item){
                return "<div class='color-demo' style=' background-color: $item->value;border-radius: 50%;width: 17px;height: 17px'></div>";
            })
            ->queryRender();
    }

    /**
     *  build datatable modal and table
     */
    public function buildDatatable($model, $request)
    {
        try {
            return \Datatable::config('styles','',['gridSystem'=>true,'dialogWidth'=>'40%'])
                ->addHiddenInput('style_id', 'style_id', '', true)
                ->addSelect($this->styles,trans('app.value'), 'value', 'value', 'style','req required col-lg-3 d:selectpicker d:show-tick','data-style=btn-success')
                ->addViewField(trans('app.color'),'color','color','color')
                ->addActionButton($this->update, 'update', 'update')
                ->addNavButton([],['add','code'])
                ->onModalOpen("_selectPicker()
                $('button.dropdown-toggle').each(function () {
                    $(this).on('click', function () {
                        console.log($(this).attr('title'));
                        $(this).parent().find('div.dropdown-menu.open').toggleClass('show')
                    })
                })")
                ->onUpdate("
                $('body').attr('data-ma-theme',$('#value').find('option:selected').val())")
                ->render();
        } catch (\Exception $e) {
        }
    }

    /**
     *  store action for save relation
     */
    public function storeDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  store action for update relation
     */
    public function updateDatatable($model = null, $request = null, $result = null)
    {
//        dd($request);
    }

    /**
     *  store action for destroy relation
     */
    public function destroyDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  inline validate dialog form
     */
    public function validateDatatable()
    {
        return [];
    }
}
