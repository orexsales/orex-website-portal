<?php

namespace App\Factories;

use App\Models\Product;
use App\Models\Tag;
use Aut\DataTable\Factories\GlobalFactory;
use Illuminate\Http\Request;

class TagFactory extends GlobalFactory
{

    /**
     *  get datatable query
     */
    public function getDatatable($model, $request)
    {
        $query = $model::all();
        return \Datatable::queryConfig('tags')
            ->queryDatatable($query)
            ->queryUpdateButton('tag_id')
            ->queryDeleteButton('tag_id')
            ->queryRender();
    }

    /**
     *  build datatable modal and table
     */
    public function buildDatatable($model, $request)
    {
        try {
            return \Datatable::config('tags',trans('app.tag'),['gridSystem'=>true])
                ->addHiddenInput('tag_id', 'tag_id', '', true)
                ->addInputText(trans('app.name_en'), 'name_en', 'name_en', 'en d:en req required col-lg-3')
                ->addInputText(trans('app.name_ar'), 'name_ar', 'name_ar', 'ar d:ar req required col-lg-3')
                ->addInputText(trans('app.code'), 'code', 'code', '')
                ->addActionButton($this->update, 'update', 'update')
                ->addActionButton($this->delete, 'delete', 'delete')
                ->addNavButton([],['code'])
                ->render();
        } catch (\Exception $e) {
        }
    }

    /**
     *  store action for save relation
     */
    public function storeDatatable($model = null, $request = null, $result = null)
    {
        if (isset($request['name_en']))
            $request['slug'] = str_slug($request['name_en']);

        Tag::create($request->input());
    }

    /**
     *  store action for update relation
     * @param null $model
     * @param Request $request
     * @param null $result
     */
    public function updateDatatable($model = null, $request = null, $result = null)
    {
        $tag = Tag::findOrFail($request->tag_id);
        if (isset($request['name_en']))
            $request['slug'] = str_slug($request['name_en']);
        $tag->update($request->input());

    }

    /**
     *  store action for destroy relation
     */
    public function destroyDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  inline validate dialog form
     */
    public function validateDatatable()
    {
        return [];
    }
}
