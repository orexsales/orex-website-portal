<?php

namespace App\Factories;

use App\Models\Course;
use App\Models\Product;
use Aut\DataTable\Factories\GlobalFactory;

class QuestionFactory extends GlobalFactory
{

    /**
     *  get datatable query
     */
    public function getDatatable($model, $request)
    {
        $query = $model::all();
//        dd($query);
        return \Datatable::queryConfig('questions')
            ->queryDatatable($query)
            ->queryUpdateButton('question_id')
            ->queryDeleteButton('question_id')
            ->queryRender();
    }

    /**
     *  build datatable modal and table
     */
    public function buildDatatable($model, $request)
    {
        try {
            return \Datatable::config('questions','',['gridSystem'=>true,'dialogWidth'=>'40%'])
                ->addHiddenInput('question_id', 'question_id', '', true)
                ->addInputText(trans('app.question').' '.trans('app._en'), 'text_en', 'text_en', 'en d:en req required col-lg-3')
                ->addInputText(trans('app.question').' '.trans('app._ar'), 'text_ar', 'text_ar', 'ar d:ar req required col-lg-3')
                ->addInputText(trans('app.answer').' '.trans('app._en'), 'answer_en', 'answer_en', 'en d:en req required col-lg-3')
                ->addInputText(trans('app.answer').' '.trans('app._ar'), 'answer_ar', 'answer_ar', 'ar d:ar req required col-lg-3')
                ->addActionButton($this->update, 'update', 'update')
                ->addActionButton($this->delete, 'delete', 'delete')
                ->addNavButton([],['code'])
                ->render();
        } catch (\Exception $e) {
        }
    }

    /**
     *  store action for save relation
     */
    public function storeDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  store action for update relation
     */
    public function updateDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  store action for destroy relation
     */
    public function destroyDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  inline validate dialog form
     */
    public function validateDatatable()
    {
        return [];
    }
}
