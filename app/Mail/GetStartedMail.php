<?php

namespace App\Mail;

use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class GetStartedMail extends Mailable
{
    use Queueable, SerializesModels;
    protected $request;

    /**
     * Create a new message instance.
     *
     * @param  Request  $request
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no_reply@orex-ae.com', 'No Reply')
            ->to($this->request->get('email'), $this->request->get('name',''))
            ->markdown('emails.get-started', [
                'name' => $this->request->get('name'),
                'email' => $this->request->get('email'),
                'message' => $this->request->get('message'),
            ]);
    }
}
