<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->product_id,
            'name'=>$this->name,
            'description'=>$this->description,
            'slug'=>$this->slug_en,
            'image_url'=>$this->image_url,
            'images'=>$this->images,
        ];
    }
}
