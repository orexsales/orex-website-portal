<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BlogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title_en'=>$this->title_en,
            'title_ar'=>$this->title_ar,
            'content_en'=>$this->content_en,
            'content_ar'=>$this->content_ar,
            'image_url'=>$this->image_url,
        ];
    }
}
