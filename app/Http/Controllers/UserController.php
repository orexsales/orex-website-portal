<?php

namespace App\Http\Controllers;

use App\Models\Page;
use App\Models\Rating;
use App\Models\Product;
use App\Models\Category;
use App\Models\Attribute;
use App\Models\Subscribe;
use App\Models\ContactMail;
use Illuminate\Http\Request;
use App\Mail\GetStartedMail;
use Illuminate\Routing\Route;

class UserController extends Controller
{
    public function __construct(Route $route)
    {
        parent::__construct($route);
    }

    public function index()
    {
        return view('pages.home', compact('blogs'));
    }

    public function faq()
    {
        return view('pages.faq');
    }

    public function getContact()
    {
        return view('pages.contact-us');
    }

    public function aboutUs()
    {
        return view('pages.about-us');
    }

    public function saveContact(Request $request)
    {
        ContactMail::create($request->input());
        return ['msg' => trans('app.message_sent_success'), 'type' => 'info'];
    }

    public function getStarted(Request $request)
    {
        \Mail::to($request->get('email'))->send(new GetStartedMail($request));
        return response()->json(['msg' => trans('app.message_sent_success'), 'type' => 'info']);
    }

    public function getProducts(Request $request)
    {
        $data = [];
        if ($request->get('param')) {
            $q = str_replace(' ', '%', $request->get('q', ''));
            $products = Product::whereRaw('upper(name_'.$this->lang.') like upper(\'%'.$q.'%\')')->where('category_id',
                $request->get('param'))->get();
            return ['items' => $data];
        } else {
            $q = str_replace(' ', '%', $request->get('q', ''));
            $products = Product::whereRaw('upper(name_'.$this->lang.') like upper(\'%'.$q.'%\')')->get();
        }
        foreach ($products as $product) {
            $data[] = [
                'id' => $product->product_id,
                'name' => $product['name_'.\App::getLocale()],
                'data-slug' => $product['slug_'.$this->lang],
            ];
            return ['items' => $data];
        }

    }


    public function getPage()
    {
        $info = [];
        $pageContent = Page::where('page_name', 'like', \Request::route()->getName())->where('page_template',
            'admin')->with(['attributePage.attribute'])->first();
        if ($pageContent) {
            $attributes = Attribute::all();
            if (isset($pageContent->attributePage)) {
                foreach ($pageContent->attributePage as $item) {
                    foreach ($attributes as $attr) {
                        if ($item->attribute->text == $attr->text) {
                            $info[$item->attribute->text] = [$item->attribute->text => $item->attribute_value];
                        }
                    }
                }
                foreach ($attributes as $attr) {
                    if (isset($info[$attr->text])) {
                        $obj = $info[$attr->text][$attr->text];
                        $info[$attr->text] = $obj;
                    }
                }
                return view('pages.'.$pageContent->page_template, compact('pageContent', 'info'));
            }
        }
    }

    public function rate(Request $request)
    {
        $rate = Rating::create($request->input());
        return ['msg' => trans('app.rating_success'), 'review' => $rate];
    }

    public function subscribe(Request $request)
    {
        Subscribe::updateOrCreate(['email' => $request->get('email')], $request->all());
        return ['msg' => trans('app.subscribe_success'), 'type' => 'success'];
    }


    public function getCategoryPage(Category $category)
    {
        if ($category->children->count() > 0) {
            $categories = $category->children()->get();
            return view('user::pages.category-product', compact('categories', 'category'));
        } else {
            $products = $category->product()->get();
            return view('user::pages.category-product', compact('products', 'category'));
        }
    }

    public function solutions()
    {
        return view('pages.solutions');
    }

    public function calculation()
    {

        return view('pages.calculation');
    }

    public function printCalculation(Request $request)
    {
        return view('partials.print', compact('request'))->render();
    }

}
