<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param Category $category
     * @return Response
     */
    public function index()
    {
        $productes = Product::with(['rating', 'category'])->paginate(9);
        $products = Product::paginate(9);
        $products = $products->sortBy(function ($product) {
            if ($product->all_rating != null) {
                return $product->all_rating;
            }
            return;
        }, SORT_DESC, true);
        return view('products.products', compact('productes', 'products'));
    }

    /**
     * Show the specified resource.
     * @param Product $product
     * @return Response
     */
    public function show(Product $product)
    {
        $images = $this->getFiles($product->name_en);
        $previous = Product::where('product_id', '<', $product->product_id)->first();
        $next = Product::where('product_id', '>', $product->product_id)->first();
        $products = Product::paginate(3);
        $products = $products->sortBy(function ($product) {
            return $product->all_rating;
        }, SORT_DESC, true);
        $files = $this->getFilesUploaded($product->name_en);
        return view('products.single-product', compact('product', 'images', 'products', 'files', 'next', 'previous'));
    }

    public function getFiles($product)
    {
        if ($product) {
            $files = Storage::disk('public')->files('images/products');
            $urlFile = [];
            $file = str_replace(' ', '_', str_replace('/', '-', strtolower($product)));

            $foundFiles = collect($files)->filter(function ($v) use ($file) {
                return preg_match("/_$file/", $v);
            });
            foreach ($foundFiles as $index => $file) {
                $urlFile[] = asset("storage/$file");
            }
            return $urlFile;
        }
        return ['success' => false];

    }

    public function getFilesUploaded($product)
    {
        if ($product) {
            $files = \Storage::disk('public')->files('files');
            $urlFile = [];
            $file = strtolower(str_replace(' ', '_', str_replace('/', '-', $product)));
            $foundFiles = collect($files)->filter(function ($v) use ($file) {
                return preg_match("/$file/", $v);
            });
            foreach ($foundFiles as $index => $file) {
                $urlFile[] = asset("storage/$file");

            }
            return $urlFile;
        }
        return ['success' => false];

    }

    public function getCategories()
    {
        $categories = Category::with(['parent', 'children'])->get();
        $data = [];
        foreach ($categories as $item) {
            if (is_null($item->parent) && $item->children()->get()->Count() == 0) {
                $data[] = $item;
            } elseif (is_null($item->parent)) {
                if ($item->children()->get()->Count() == 0) {
                    $data[] = $item;
                } else {
                    foreach ($item->children()->get() as $value) {
                        $data[] = $value;
                    }
                }
            }
        }
        return $data;
    }

    public function getProductsByType(Category $category)
    {
        $productes = Product::where('category_id', $category->category_id)->with(['rating', 'category'])->paginate(6);
        $products = Product::where('category_id', $category->category_id)->paginate(6);
        $products = $products->sortBy(function ($product) {
            if ($product->all_rating != null) {
                return $product->all_rating;
            }
            return;
        }, SORT_DESC, true);
        $cat = Category::where('category_id', $category->category_id)->first();
        return view('products.products', compact('productes', 'products', 'category', 'cat'));
    }

    public function getProducts(Request $request)
    {
        $data = [];
        if ($request->has('query')) {
            $q = str_replace(' ', '%', $request->get('query', ''));
            $products = Product::whereRaw('upper(name_' . $this->lang . ') like upper(\'%' . $q . '%\')')->with('category')->get();
        } else {
            $products = Product::with('category')->get();
        }
        foreach ($products as $product) {
            $data[] = [
                'id' => $product->product_id,
                'name' => $product['name_' . \App::getLocale()],
                'data-slug' => $product['slug_' . $this->lang],
                'image_url' => $product->image_url,
                'slug' => $product->slug_en,
                'content' => $product['description_html_' . $this->lang],
                'category' => $product->category,
            ];
        }
        return ['items' => $data];
    }


}
