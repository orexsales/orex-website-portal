<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    /**
     * @var string $lang
     */
    protected $lang;

    public function __construct()
    {
        $this->lang = \LaravelLocalization::getCurrentLocale();
        $this->init();
    }

    public function init()
    {
        $categories1 = Category::orderBy('category_order','asc')->with('products')->whereNotNull('parent_id')->get();
        $categories_temp = Category::orderBy('category_order')->with('products')->whereNull('parent_id')->get();
        $data = [];
        foreach ($categories_temp as $item) {
            if ($item->children()->get()->count() == 0) {
                $data[] = $item->toArray();
            }
        }
        $categories = array_merge($categories1->toArray(), $data);
        $categories=array_sort($categories,function ($cat){
            return $cat['category_order'];
        });
        view()->share('categories', $categories);
    }
}
