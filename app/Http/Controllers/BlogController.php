<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Comment;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BlogController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $blogs = Blog::with(['tag', 'comment'])->paginate(6);
        $tags = Tag::all();
        $months = [
            'كانون الثاني' => 'January',
            'شباط' => 'February',
            'آذار' => 'March',
            'نيسان' => 'April',
            'أيار' => 'May',
            'حزيران' => 'June',
            'تموز' => 'July',
            'آب' => 'August',
            'أيلول' => 'September',
            'تشرين الأول' => 'October',
            'تشرين الثاني' => 'November',
            'كانون الأول' => 'December',
        ];

        return view('blogs.blogs', compact('blogs', 'tags', 'months'));
    }


    /**
     * Show the specified resource.
     * @param Blog $blog
     * @return Response
     */
    public function show(Blog $blog)
    {
        $blogs = Blog::paginate(5);
        $blogs = $blogs->sortBy(function ($blog) {
            return $blog->created_at;
        }, SORT_DESC, true);
        $files = $this->getFilesUploaded($blog->title_en);
        $previous = Blog::where('blog_id', '<', $blog->blog_id)->first();
        $next = Blog::where('blog_id', '>', $blog->blog_id)->first();
        $months = [
            'كانون الثاني' => 'January',
            'شباط' => 'February',
            'آذار' => 'March',
            'نيسان' => 'April',
            'أيار' => 'May',
            'حزيران' => 'June',
            'تموز' => 'July',
            'آب' => 'August',
            'أيلول' => 'September',
            'تشرين الأول' => 'October',
            'تشرين الثاني' => 'November',
            'كانون الأول' => 'December',
        ];
        return view('blogs.single-blog', compact('blog', 'blogs', 'months', 'files', 'previous', 'next'));
    }

    public function getFilesUploaded($blog)
    {
        if ($blog) {
            $files = \Storage::disk('public')->files('files');
            $urlFile = [];
            $file = strtolower(str_replace(' ', '_', str_replace('/', '-', $blog)));

            $foundFiles = collect($files)->filter(function ($v) use ($file) {
                return preg_match("/$file/", $v);
            });

            foreach ($foundFiles as $index => $file) {
                $urlFile[] = asset("storage/$file");
            }
            return $urlFile;
        }
        return ['success' => false];

    }

    public function getBlogByTag(Tag $tag)
    {
        $blogs = Blog::where('tag_id', $tag->tag_id)->with(['tag', 'comment'])->paginate(8);
        $tags = Tag::all();
        $months = [
            'كانون الثاني' => 'January',
            'شباط' => 'February',
            'آذار' => 'March',
            'نيسان' => 'April',
            'أيار' => 'May',
            'حزيران' => 'June',
            'تموز' => 'July',
            'آب' => 'August',
            'أيلول' => 'September',
            'تشرين الأول' => 'October',
            'تشرين الثاني' => 'November',
            'كانون الأول' => 'December',
        ];

        return view('blogs.blogs', compact('blogs', 'tags', 'months'));
    }

    public function getBlogByMonth($month)
    {
        $blogs = Blog::filter(['month' => $month])->paginate(6);
        $tags = Tag::all();
        $months = [
            'كانون الثاني' => 'January',
            'شباط' => 'February',
            'آذار' => 'March',
            'نيسان' => 'April',
            'أيار' => 'May',
            'حزيران' => 'June',
            'تموز' => 'July',
            'آب' => 'August',
            'أيلول' => 'September',
            'تشرين الأول' => 'October',
            'تشرين الثاني' => 'November',
            'كانون الأول' => 'December',
        ];

        return view('blogs.blogs', compact('blogs', 'tags', 'months'));
    }

    public function saveComment(Request $request)
    {
        $comment = Comment::create($request->input());
        $count = Comment::where('blog_id', $request->get('blog_id'))->count();
        return ['msg' => trans('app.comment_success'), 'comment' => $comment, 'count' => $count];
    }

}
