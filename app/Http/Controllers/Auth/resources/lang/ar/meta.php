<?php

return [
    'home_title' =>'تجارة ألواح طاقة شمسية | Solar Power Products online in UAE - OREX',
    'home_description' =>'دع منتجات الطاقة الشمسية تضيئ حياتك. محطة OREX لتجارة منتجات ألواح الطاقة الشمسية بما في ذلك UPS الخاص بالطاقة الشمسية، و الانفيرتر، والبطاريات الخاصة بالطاقة الشمسية، والمنظمات اللازمة، والى اخره من مستلزمات واكسسوارات',

    'calculation_tittle'=>'حاسبة متطلبات الطاقة الشمسية وحساب إجمالي التوفير',
    'calculation_description'=>'قم بتقدير إجمالي التوفير باستخدام حاسبة الألواح الشمسية هذه ، يمكنك هنا تحليل عدد الألواح والعاكسات والبطاريات المطلوبة بناءً على حجم السقف وفاتورة الكهرباء وعوامل أخرى.',

    'about_title'=>'حول Orex - مورد بالجملة لمنتجات الطاقة الشمسية',
    'about_description'=>'نحن في OREX نوفر منتجات وخدمات طاقة شمسية فريدة وموثوقة وعالية الجودة والتي تشمل جميع المراحل التي ينطوي عليها التصنيع وضمان الجودة والتوريد. تأسست OREX في عام 2011 لتحديد احتياجاتك وتزويدك بأنظمة فعالة للطاقة الشمسية منخفضة التكلفة. نحن على ثقة من أنه باستخدام أنظمة الطاقة الشمسية الخاصة بنا ، سوف تبدأ في توفير المال والوقت والطاقة من البداية.نحن في OREX نوفر منتجات وخدمات طاقة شمسية فريدة وموثوقة وعالية الجودة والتي تشمل جميع المراحل التي ينطوي عليها التصنيع وضمان الجودة والتوريد. تأسست OREX في عام 2011 لتحديد احتياجاتك وتزويدك بأنظمة فعالة للطاقة الشمسية منخفضة التكلفة. نحن على ثقة من أنه باستخدام أنظمة الطاقة الشمسية الخاصة بنا ، سوف تبدأ في توفير المال والوقت والطاقة من البداية.',

    'blog_title'=>'ابق على اطلاع على المدونات - Orex ، مورد جملة',
    'blog_description'=>'ستكشف مدونات منتجات الطاقة الشمسية وابق على علم بآخر منتجات الألواح الشمسية.',

    'solar-panels-title'=>'مورد جملة منتجات الألواح الشمسية | طاقة شمسية',
    'solar-panels-descp'=>'تابع منتجات الألواح الشمسية ، فلدينا مجموعة متنوعة من منتجات الطاقة الشمسية من Monocrystalline إلى Polycrysralline.  خصومات للموزعين.',

    'inverters-title'=>'مجموعة محولات الطاقة الشمسية | PV Powered Inverter',
    'inverters-descp'=>'تابع منتجات الألواح الشمسية ، فلدينا مجموعة متنوعة من منتجات الطاقة الشمسية من Monocrystalline إلى Polycrysralline.  خصومات للموزعين',

    'batteries-title'=>'بطاريات شمسية مورد جملة | أفضل صفقة متوفرة على البطارية الشمسية',
    'batteries-descp'=>'كوننا مورد جملة لبطاريات الطاقة الشمسية ، فإن الموزعين يحصلون دائمًا على أفضل العروض في Orex. اتصل بنا لإجراءللحصول على عرض سعر للبطارية الشمسية اليوم.',

    'charge-controllers-title'=>'بيع بالجملة Solar Controller | Solar Controller',
    'charge-controllers-descp'=>'كمورد بالجملة ، سيحصل الموزعون دائمًا على أفضل صفقة على أجهزة التحكم بالشحن بالطاقة الشمسية في Orex.',

    'led-bulb-title'=>'المصابيح الشمسية بالجملة | لمبة اضاءة',
    'led-bulb-descp'=>'أفضل مجموعة من لمبة LED الشمسية متوفرة للموزعين بأفضل سعر من Orex ، مورد الجملة لدولة الإمارات العربية المتحدة. رؤية المصابيح الشمسية الصمام اليوم!',


    'solar-panel-poly-title'=>'تجارة ألواح طاقة شمسية (بولي) | Polycrystalline Solar Panels',
    'solar-panel-poly-descp'=>'توصل الى أفضل عروض ألواح الطاقة الشمسية (بولي) من OREX بأفضل الأسعار. هنا ألواح بولي متوفرة من 5 واط الى 300 واط. إتصل بنا اليوم!',


    'solar-panel-mono-title'=>'تجارة ألواح طاقة شمسية (مونو) | Monocrystalline Solar Panels',
    'solar-panel-mono-descp'=>'توصل الى أفضل عروض ألواح الطاقة الشمسية (مونو) من OREX بأفضل الأسعار. هنا ألواح مونو متوفرة من 100 واط الى 300 واط. إتصل بنا اليوم!',

    'red-inverter-title'=>'Buy Red Inverter Online at Best Prices | Red Inverter Battery',
    'red-inverter-descp'=>'Orex offers best red inverters which is undoubtedly a leading player in power backup. Here red inverter batteries are available from 300w to 3000W.',

    'solar-water-pump-inverter-title'=>'انفيرتر طاقة شمسية خاص بغاطسات ومضخات المياه',
    'solar-water-pump-inverter-descp'=>'عروض ممتازة على انفيرترات الطاقة الشمسية الخاصة بمضخات المياه والغاطسات متوفرة لدى OREX بقدرات متنوعة ابتداءا من 5.5 كيلو واط الى 22 كيلو واط',

    'home-ups-title'=>'Home UPS | Power Inverters Online - OREX',
    'home-ups-descp'=>'تنظيم تقطع الكهرباء مع ال UPS المنزلي. العديد من الفوائد في inverter ال UPS المنزلي. لللمزيد من المعلومات اضغط هنا !',

    'solar-home-ups-title'=>'Buy Solar Home UPS online at Best Price in UAE | Solar UPS',
    'solar-home-ups-descp'=>'Find the best collection of solar home UPS at Orex & get great deals on every purchase. Click here to check the collection of solar power UPS for home today.',


    'hybrid-inverter-with-mppt-title'=>'مورد جملة للعاكس الهجين مع MPPT | وحدة الطاقة الشمسي',
    'hybrid-inverter-with-mppt-descp'=>'مجموعة رائعة من العاكس الهجين مع Mppt. يمكن للموزعين زيادة أرباحهم التجارية مع OREX. احصل على عرض أسعار مجاني اليوم!',

    'high-capacity-inverter-title'=>'أفضل محولات عالية السعة ومجموعة البطاريات في Orex',
    'high-capacity-inverter-descp'=>'تقدم Orex محولات عالية السعة للتجار بأفضل الأسعار. إذا كنت موزعًا ولديك أي استفسار بخصوص المحولات عالية السعة ، فانقر هنا.',




    'agm-batteries-title'=>'اختيار بطاريات AGM للأداء العالي | بيع بالجملة',
    'agm-batteries-descp'=>'بطارية AGM والمعروفة باسم البطارية التي لا تحتاج الى صيانة المصممة لنظام الطاقة الشمسية من مورد الجملة في الإمارات العربية المتحدة بأفضل سعر. اقرأ المزيد عن بطاريات AGM اليوم!',

    'solar-kit-title'=>'Solar Kit for Home Needs | Wholesale Supplier',
    'solar-kit-descp'=>'Being a wholesale supplier of Solar kit, distributors are always getting best deals at Orex. Contact us to make a trade enquiry of solar kits.',

    'led-bulb-12v-title'=>' لمبة LED 12V بأفضل الأسعار | لمبة الطاقة الشمسية',
    'led-bulb-12v-descp'=>'وفر أكثر مع مصباح LED في Orex. يمكن للموزعين دائما الحصول على أفضل العروض. اكتب لنا اليوم للاستفسار عن لمبة LED الشمسية التي تتوفر في قدرات 3 وات ، 5 وات ، 7 وات.',


    'led-bulb-220v-title'=>'مورد جملة لمصابيح 220 فولت | شراء المصابيح الشمسية ',
    'led-bulb-220v-descp'=>'احصل عن مجموعة رائعة من لمبات LED 220v من مورد بالجملة في دولة الإمارات العربية المتحدة. احصل على عروض حصرية للموزعين ، اتصل للاستعلام التجاري عن لمبة',


    'tall-tubular-batteries-title'=>'بطاريات Tall Tabular Batteries - Orex ، متجر بيع بالجملة OREX',
    'tall-tubular-batteries-descp'=>'تساعد Orex Tubular Batteries المستخدمين على الاستعمال لفترة طويلة. كوننا مورد بالجملة ، يمكن للموزعين الحصول على صفقات مذهلة للتجارة. اطلب عرض سعر!',



    'project-accessories-spare-parts-title'=>'مورد جملة لمشاريع الطاقة الشمسية اكسسوارات وقطع غيار ',
    'project-accessories-spare-parts-descp'=>' مورد الجملة المعروف في الإمارات العربية المتحدة يقدم أفضل العروض للموزعين لجميع مشاريع الطاقة الشمسية بما في ذلك الملحقات وقطع الغيار.',


    'pwm-inverter-title'=>'PWM مورد عاكس بالجملة | أفضل صفقة متاحة على العاكس',
    'pwm-inverter-descp'=>'كمورد بالجملة ، سيحصل الموزعون دائمًا على أفضل صفقة على محولات PWM في Orex. احصل على عرض لPulse Width Modulation inverter بمجرد نقرة واحدة!',

    'dc-fan-title'=>'أفضل مراوح السقف DC | مروحة ستاند DC - Orex',
    'dc-fan-descp'=>'تقدم Orex مروحة DC مُعدة للعمل مع لوحة شمسية. كوننا مورد بالجملة ، يمكن للموزعين الحصول على صفقات مذهلة للتجارة. اطلب اقتباس!',

    'solar-kit-title'=>'مجموعة الطاقة الشمسية لاحتياجات المنزل | مورد جملة',
    'solar-kit-descp'=>'كونه مورد جملة لمجموعة Solar ، فإن الموزعين يحصلون دائمًا على أفضل العروض في Orex. اتصل بنا لإجراء تحقيق تجاري لمجموعات الطاقة الشمسية.',

    'solar-water-heater-title'=>'مورد جملة سخان مياه بالطاقة الشمسية | سخان مياه بالطاقة الشمسية',
    'solar-water-heater-descp'=>'مورد جملة لموزعين منتجات الطاقة الشمسية، يمكن للموزعين الحصول على أفضل عروض تسخين المياه بالطاقة الشمسية من OREX. اضغط هنا للحصول على كامل تفاصل تسخين المياه باستعمال الطاقة الشمسية اليوم!',

];
