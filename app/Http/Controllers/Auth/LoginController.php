<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/table/products';

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $categories1 = Category::orderBy('category_order','asc')->with('products')->whereNotNull('parent_id')->get();
        $categories_temp = Category::orderBy('category_order')->with('products')->whereNull('parent_id')->get();
        $data = [];
        foreach ($categories_temp as $item) {
            if ($item->children()->get()->count() == 0) {
                $data[] = $item->toArray();
            }
        }
        $categories = array_merge($categories1->toArray(), $data);
        $categories=array_sort($categories,function ($cat){
            return $cat['category_order'];
        });
        view()->share('categories', $categories);
        $this->middleware('guest')->except('logout');
    }
}
