<?php

namespace App\Http\Controllers\Auth;

//use Socialite;

use App\Http\Controllers\Controller;
use App\models\Provider;
use App\User;
use Config;
use Illuminate\Support\Facades\Input;
use Laravel\Socialite\Facades\Socialite;

class OAuthController extends Controller
{
    /**
     * Redirect the user to the Provider authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {
        $providerKey = Config::get('services.'.$provider);

        if (empty($providerKey)) {

            return back()
                ->with('error', 'No such provider');

        }
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from the Provider.
     *
     * @return array
     */
    public function handleProviderCallback($provider)
    {
        if (Input::get('denied') != '') {
            return redirect()->to('/login')
                ->with('status', 'danger')
                ->with('message', 'You did not share your profile data with our social app.');
        }
        $user = $this->createOrGetUser(Socialite::driver($provider)->stateless()->user(), $provider);
        auth()->login($user);

        return redirect()->to('/home');
    }

    /**
     * Create or get a user based on provider id.
     *
     * @param $providerUser
     * @param $provider
     * @return Object $user
     */
    private function createOrGetUser($providerUser, $provider)
    {
        $account = Provider::where('provider', $provider)
            ->where('user_id', $providerUser->getId())
            ->first();

        if ($account) {
            //Return account if found
            return $account->user;
        } else {

            //Check if user with same email address exist
            $user = User::where('email', $providerUser->getEmail())->first();

            //Create user if doses not exist
            if (!$user) {
                $user = User::create([
                    'email' => $providerUser->getEmail(),
                    'image' => $providerUser->avatar_original,
                    'name' => !is_array($providerUser->user['name']) ? $providerUser->user['name'] : $providerUser->user['name']['familyName'],
                ]);

//                $user->getId();
//                $user->getNickname();
//                $user->getName();
//                $user->getEmail();
//                $user->getAvatar();

            }
            //Create social account
            $user->social_accounts()->create([
                'user_id' => $providerUser->getId(),
                'provider' => $provider,
            ]);

            return $user;
        }
    }
}
