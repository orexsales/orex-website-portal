<?php

namespace App\Http\ViewComposers;

/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 12/31/2016
 * Time: 10:33 PM
 */
use App\Models\Tracker;
use Illuminate\Contracts\View\View;

class VisitComposer
{
    /**
     * @param View $view
     */
    public function compose(View $view)
    {
            (new Tracker)->hit();
    }
}
