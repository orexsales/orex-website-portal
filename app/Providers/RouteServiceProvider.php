<?php

namespace App\Providers;

use App\Models\Product;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use LaravelLocalization;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        Route::bind('product', function ($new) {
            return Product::where('slug_en',$new)->orWhere('slug_ar',$new)->orWhere(function ($query) use ($new){
                if (is_numeric($new))
                    $query->where('product_id',$new);
            })->first();
        });
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @param Router $router
     * @return void
     */
    public function map(Router $router)
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

//        $this->mapCustomPagesRoutes($router);

    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }

    protected function mapCustomPagesRoutes($router)
    {
        $articleRoutes=\DB::table('pages')->where('page_template','admin')->where('page_name','<>','home')->select('page_name')->get();
        if(count($articleRoutes))
            $router->group(
                [
                    'prefix' => LaravelLocalization::setLocale(),
                    'namespace' => $this->namespace,
                    'middleware' => ['web', 'localizationRedirect'],
                ], function ($router) use($articleRoutes){
                foreach ($articleRoutes as $value){
                    $router->get(str_slug($value->page_name), 'UserController@getPage')->name($value->page_name);
                }
            });
    }
}
